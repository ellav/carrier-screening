import split_by_disorder as split
import seaborn as sns
import matplotlib.pyplot as plt
import pandas as pd
from class_def import Parameter
from variant_filter import damaging_variant
from scipy.stats import (linregress, spearmanr, normaltest)
import numpy as np
from damaging_filter import (Polyphen2_HVAR, LRT, MutationTaster, MutationAssessor, SIFT,
                             FATHMM, Provean, M_CAP)
from sklearn.linear_model import LinearRegression
from scipy.stats import linregress



def right_function(dataframe, score):

    def over_22(element):
        if element >= 22:
            return 1
        return 0

    if score == 'SIFT_score':
        dataframe = SIFT(dataframe)
    elif score == 'Polyphen2_HVAR_score':
        dataframe = Polyphen2_HVAR(dataframe)
    elif score == 'LRT_score':
        dataframe = LRT(dataframe)
    elif score == 'MutationTaster_score':
        dataframe = MutationTaster(dataframe)
    elif score == 'MutationAssessor_score':
        dataframe = MutationAssessor(dataframe)
    elif score == 'FATHMM_score':
        dataframe = FATHMM(dataframe)
    elif score == 'PROVEAN_score':
        dataframe = Provean(dataframe)
    elif score == 'M-CAP_score':
        dataframe = M_CAP(dataframe)
    elif score == 'CADD_phred':
        dataframe['CADD_phred'] = dataframe['CADD_phred'].apply(pd.to_numeric)
        dataframe['CADD_over_22'] = dataframe['CADD_phred'].apply(over_22)
    return dataframe


def string_to_float(dataframe, software_score):

    # def invert(element):
    #     return element*(-1)

    AF_name = software_score
    dataframe.loc[:, AF_name] = dataframe[AF_name].fillna(value=0)
    float_col = pd.to_numeric(dataframe[AF_name], errors='coerce')
    res = dataframe[float_col.isna()]
    rest = dataframe[float_col.notna()]

    if res.shape[0] == 0:
        return rest

    res.loc[:, AF_name] = res.loc[:, AF_name].str.split(',')
    if AF_name == 'PROVEAN_score' or AF_name == 'FATHMM_score' or AF_name =='LRT_score' or AF_name == 'SIFT_score':
        res.loc[:, AF_name] = res[AF_name].apply(min)
    res.loc[:, AF_name] = res[AF_name].apply(max)

    dataframe_no_string = res.append(rest)
    # if AF_name == 'PROVEAN_score' or AF_name == 'FATHMM_score' or AF_name =='LRT_score' or AF_name == 'SIFT_score':
    #     print(dataframe_no_string[AF_name])
    #     dataframe_no_string[AF_name] = dataframe_no_string[AF_name].apply(invert)
    #     print(dataframe_no_string[AF_name])
    #     exit(0)
    return dataframe_no_string


def pairwise_software(dataframe):
    list_softwares = ['SIFT_score', 'Polyphen2_HVAR_score', 'LRT_score', 'MutationTaster_score', 'MutationAssessor_score',
                      'FATHMM_score', 'PROVEAN_score', 'M-CAP_score', 'CADD_phred']
    software = dataframe[list_softwares]

    sns.pairplot(software)
    plt.show()
    plt.clf()
    plt.close()
    return


def correlatio_softwares(dataframe, ch_eng):
    # dataframe is all true Calls! :)
    # list_softwares = ['SIFT_score', 'Polyphen2_HVAR_score', 'LRT_score', 'MutationTaster_score', 'MutationAssessor_score',
    #                   'FATHMM_score', 'PROVEAN_score', 'M-CAP_score', 'CADD_phred']
    list_softwares = ['SIFT_score', 'Polyphen2_HVAR_score', 'LRT_score', 'MutationAssessor_score',
                      'FATHMM_score', 'PROVEAN_score', 'M-CAP_score', 'CADD_phred']
    # list_softwares = ['SIFT_score', 'Polyphen2_HVAR_score', 'LRT_score', 'MutationTaster_score']

    list_neg_scores = ['PROVEAN_score', 'FATHMM_score', 'LRT_score', 'SIFT_score']

    result = pd.DataFrame(0, index=list_softwares, columns=list_softwares,  dtype='float')

    # print(dataframe[list_softwares])
    for score in list_softwares:
        # x = software_df[score].fillna(0)
        for score2 in list_softwares:
            # y = software_df[score2].fillna(0)
            if score == score2:
                two_scores = dataframe[[score]].dropna(axis=0)
                two_scores = string_to_float(two_scores, score)
                x = two_scores.apply(pd.to_numeric).values[:,0]
                y = two_scores.apply(pd.to_numeric).values[:,0]
                if score in list_neg_scores:
                    x = np.negative(x)
                    y = np.negative(y)
            else:
                two_scores = dataframe[[score, score2]].dropna(axis=0)
                two_scores = string_to_float(two_scores, score)
                two_scores = string_to_float(two_scores, score2)
                x = two_scores[score].apply(pd.to_numeric).values
                y = two_scores[score2].apply(pd.to_numeric).values
                if score in list_neg_scores:
                    x = np.negative(x)
                if score2 in list_neg_scores:
                    y = np.negative(y)
            k2, p1 = normaltest(x)
            k2,  p2 = normaltest(y)
            if p1 > 0.05 and p2 > 0.05:
                print(score)
                print(score2)
                print('we have normal distribution')
            corr, _ = spearmanr(x, y)
            r_value = corr
            # slope, intercept, r_value, p_value, std_err = linregress(x, y)

            print(r_value)
            result.at[score, score2] = r_value
            if score != score2:
                result.at[score2, score] = r_value
    print(result)

    print(result.max)
    print(result.min)
    mask = np.full_like(result, 0)
    mask[np.triu_indices_from(mask, k=1)] = True
    with sns.axes_style("white"):
        ax = sns.heatmap(result, mask=mask, square=True, yticklabels=True, xticklabels=True, linewidths=.5, vmin=-1, vmax=1, cmap='seismic')

    ax.tick_params(labelsize=14)
    if ch_eng == 0:
        nat = 'Swiss'
    elif ch_eng == 1:
        nat = 'Eng'
    title = nat + ' - Spearman Correlation Coefficient'
    ax.set_title(title)

    path = '/home/selene/Programm/Diss/' + nat+ '_correlation_prediction_software.png'
    plt.savefig(path, bbox_inches='tight')

    plt.clf()
    plt.close()
    return


def software_distribution(dataframe, score, ch_eng):
    print(list(dataframe))
    print(dataframe['SIFT_pred'].unique())
    dataframe = right_function(dataframe, score)
    col_name = list(dataframe)[-1]
    print(col_name)

    # score_col = dataframe[score]
    dataframe = dataframe.dropna(subset=[score])
    dataframe = string_to_float(dataframe, score)
    dataframe[score] = dataframe[score].apply(pd.to_numeric)

    patho = dataframe[dataframe[col_name] == 1][score]
    benign = dataframe[dataframe[col_name] == 0][score]

    ax = sns.distplot(patho, label='Damaging prediction')
    ax = sns.distplot(benign, label='Benign prediction')
    if ch_eng == 0:
        nat = 'Swiss'
    elif ch_eng == 1:
        nat = 'English'
    title = nat + ' - ' + score
    ax.set_title(title)
    ax.set_ylabel('Occurence')
    ax.set_xlabel('Score')
    ax.legend()
    path = '/home/selene/Programm/Diss/prediction_software/'
    path = path + nat + '_' + score + '_distribution.png'
    plt.savefig(path, bbox_inches='tight')
    # plt.show()
    plt.clf()
    plt.close()
    return

def CADD_MCAP_corr(dataframe):
    CADD_name = 'CADD_phred'
    MCAP_name = 'M-CAP_score'

    x = dataframe[CADD_name].fillna(0)
    y = dataframe[MCAP_name].fillna(0)
    slope, intercept, r_value, p_value, std_err = linregress(x, y)

    x = x.values.reshape(-1, 1)
    y = y.values.reshape(-1, 1)

    linear_regressor = LinearRegression()  # create object for the class
    linear_regressor.fit(x, y)  # perform linear regression
    y_pred = linear_regressor.predict(x)  # make predictions

    fig, ax = plt.subplots()
    ax.plot(x, y_pred, color='red')
    ax.scatter(x, y)

    text = ('Correlation Coefficient = %.2f \n'
            'p-value = %.4E' % (r_value, p_value))
    props = dict(boxstyle='round', facecolor='white', alpha=0.5)
    ax.text(0.05, 0.95, text, transform=ax.transAxes, fontsize=14,
            verticalalignment='top', bbox=props)

    ax.tick_params(labelsize=14)
    ax.set_xlabel('CADD', fontsize=14)
    ax.set_ylabel('M-CAP score', fontsize=14)

    path = '/home/selene/Programm/Diss/' + 'correlation_CADD_MCAP.png'
    plt.savefig(path, bbox_inches='tight')

    plt.clf()
    plt.close()
    return


if __name__=="__main__":

    ch_eng = 0
    if ch_eng == 0:
        pickel_filename = 'true_calls.pkl'
    elif ch_eng == 1:
        pickel_filename = 'true_calls_eng.pkl'
    folder_path = '/home/selene/Programm/Diss'
    true_calls = pd.read_pickle(folder_path + '/' + pickel_filename)

    CADD_MCAP_corr(true_calls)
    correlatio_softwares(true_calls, ch_eng)
    # pairwise_software(true_calls)

    list_softwares = ['FATHMM_score', 'SIFT_score', 'Polyphen2_HVAR_score', 'LRT_score', 'MutationTaster_score', 'MutationAssessor_score',
                      'FATHMM_score', 'PROVEAN_score', 'M-CAP_score', 'CADD_phred']


    # for scr in list_softwares:
    #     print(scr)
    #     software_distribution(true_calls, scr, ch_eng)
