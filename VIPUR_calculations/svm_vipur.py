import pandas as pd
import matplotlib.pyplot as plt

def plot_ms_VIPUR_classification_all_variants():
    df = pd.read_excel('VIPUR_calculations/for_AI_Ella.xlsx', sheet_name='all variants')
    df.columns = (df.columns.str.strip()
                                .str.replace(' ', '_')
                                .str.replace(':', ''))

    color = df['ClinicalSignificance']
    clasifciations = ['Benign', 'Benign/Likely benign', 'Likely benign', 'Likely pathogenic', 'Pathogenic', 'Pathogenic/Likely pathogenic', 'presumed pathogenic']

    colorcode = ['darkblue', 'cornflowerblue', 'lightsteelblue', 'salmon', 'darkred', 'indianred', 'darkred']
    i = 0
    for classi in clasifciations:
        df_sub = df.loc[df['ClinicalSignificance'] == classi]
        vipur_class = df_sub['VIPUR_classification']
        ms = df_sub['%ms']
        plt.scatter(ms, vipur_class, c=colorcode[i], label=classi, alpha=0.3)
        i = i +1

    plt.legend()
    plt.xlabel('MS percentage')
    plt.ylabel('VIPUR classification')
    plt.title('all variabls')
    plt.savefig('VIPUR_calculations/all_variants.png', bbox_inches='tight')
    plt.clf()
    plt.close()
    # plt.show()
    return


def plot_ms_VIPUR_classification_test1():
    df = pd.read_excel('VIPUR_calculations/for_AI_Ella.xlsx', sheet_name='plot test 1')
    df.columns = (df.columns.str.strip()
                                .str.replace(' ', '_')
                                .str.replace(':', ''))

    color = df['ClinicalSignificance']
    clasifciations = ['Benign', 'Benign/Likely benign', 'Likely benign', 'Likely pathogenic', 'Pathogenic', 'Pathogenic/Likely pathogenic', 'presumed pathogenic']

    colorcode = ['darkblue', 'cornflowerblue', 'lightsteelblue', 'salmon', 'darkred', 'indianred', 'darkred']
    i = 0
    for classi in clasifciations:
        df_sub = df.loc[df['ClinicalSignificance'] == classi]
        vipur_class = df_sub['VIPUR_classification']
        ms = df_sub['%ms']
        plt.scatter(ms, vipur_class, c=colorcode[i], label=classi, alpha=0.4)
        i = i +1

    # plt.legend()
    plt.xlabel('MS percentage')
    plt.ylabel('VIPUR classification')
    plt.title('plot test 1')
    plt.savefig('VIPUR_calculations/plot_test_1.png', bbox_inches='tight')
    plt.clf()
    plt.close()
    # plt.show()
    return


def plot_ms_VIPUR_score_test2():
    df = pd.read_excel('VIPUR_calculations/for_AI_Ella.xlsx', sheet_name='plot test 2')
    df.columns = (df.columns.str.strip()
                                .str.replace(' ', '_')
                                .str.replace(':', ''))

    color = df['ClinicalSignificance']
    clasifciations = ['Benign', 'Benign/Likely benign', 'Likely benign', 'Likely pathogenic', 'Pathogenic', 'Pathogenic/Likely pathogenic', 'presumed pathogenic']

    colorcode = ['darkblue', 'cornflowerblue', 'lightsteelblue', 'salmon', 'darkred', 'indianred', 'darkred']
    i = 0
    for classi in clasifciations:
        df_sub = df.loc[df['ClinicalSignificance'] == classi]
        vipur_class = df_sub['Vipur-score']
        ms = df_sub['%ms']
        plt.scatter(ms, vipur_class, c=colorcode[i], label=classi, alpha=0.4)
        i = i +1

    # plt.legend()
    plt.xlabel('MS percentage')
    plt.ylabel('VIPUR score')
    plt.title('plot test 2')
    plt.savefig('VIPUR_calculations/plot_test_2_score.png', bbox_inches='tight')
    plt.clf()
    plt.close()
    # plt.show()
    return

def plot_ms_VIPUR_classification_test2():
    df = pd.read_excel('VIPUR_calculations/for_AI_Ella.xlsx', sheet_name='plot test 2')
    df.columns = (df.columns.str.strip()
                                .str.replace(' ', '_')
                                .str.replace(':', ''))

    color = df['ClinicalSignificance']
    clasifciations = ['Benign', 'Benign/Likely benign', 'Likely benign', 'Likely pathogenic', 'Pathogenic', 'Pathogenic/Likely pathogenic', 'presumed pathogenic']

    colorcode = ['darkblue', 'cornflowerblue', 'lightsteelblue', 'salmon', 'darkred', 'indianred', 'darkred']
    i = 0
    for classi in clasifciations:
        df_sub = df.loc[df['ClinicalSignificance'] == classi]
        vipur_class = df_sub['VIPUR_classification']
        ms = df_sub['%ms']
        plt.scatter(ms, vipur_class, c=colorcode[i], label=classi, alpha=0.4)
        i = i +1

    # plt.legend()
    plt.xlabel('MS percentage')
    plt.ylabel('VIPUR classification')
    plt.title('plot test 2')
    plt.savefig('VIPUR_calculations/plot_test_2_classification.png', bbox_inches='tight')
    plt.clf()
    plt.close()
    # plt.show()
    return

def find_index(element, df):
    begin = element.find(':g.') + 3;
    end = element.find('>') - 1;
    genomicPos = element[begin:end]
    try:
        genomicPosInt = int(genomicPos)
    except:
        genomicPosInt = 10000
        print(genomicPos + " was not a valid Start position")
        return 10000

    # print(genomicPosInt)
    rightRow = df[df['Start'] == genomicPosInt]
    if rightRow.shape[0] == 1:
        return rightRow['index'].values[0]
    elif rightRow.shape[0] == 0:
        print()
        print(str(genomicPosInt) + " was not found in the Annovar Dataset")
        return 20000
    else:
        alt = element[-1:]
        oneRow = rightRow[rightRow['Alt']==alt]
        return oneRow['index'].values[0]


def merge_annovar_exome_with_ruxandra():
    df_ruxandra = pd.read_excel('VIPUR_calculations/Ruxandra_variants_corresponding_to_vfc.xlsx')
    df_annovar = pd.read_csv('VIPUR_calculations/query.output.exome_summary.csv', delimiter = ',')
    df_annovar = df_annovar.reset_index();
    # drop dupilcated rows, with the same variant as entry
    df_annovar = df_annovar.drop_duplicates(subset=["AAChange.refGene"])

    df_ruxandra['index'] = ""

    df_ruxandra['index'] = df_ruxandra['genomic'].apply(find_index, df=df_annovar)
    df_result = df_ruxandra.merge(df_annovar, how='left', left_on='index', right_on='index')

    df_result.to_csv('VIPUR_calculations/Ruxandra_annovar_exome_merged.csv', sep='\t')
    return df_result

def merge_annovar_genom_with_ruxandra():
    df_ruxandra = pd.read_excel('VIPUR_calculations/Ruxandra_variants_corresponding_to_vfc.xlsx')
    df_annovar = pd.read_csv('VIPUR_calculations/query.output.genome_summary.csv', delimiter = ',')
    df_annovar = df_annovar.reset_index();


    # drop dupilcated rows, with the same variant as entry
    df_annovar = df_annovar.drop_duplicates(subset=["AAChange.refGene", "Start"])

    df_ruxandra['index'] = ""

    df_ruxandra['index'] = df_ruxandra['genomic'].apply(find_index, df=df_annovar)
    df_result = df_ruxandra.merge(df_annovar, how='left', left_on='index', right_on='index')

    df_result.to_csv('VIPUR_calculations/Ruxandra_annovar_genome_merged.csv', sep='\t')
    return df_result

if __name__ == '__main__':
    # merge_annovar_exome_with_ruxandra()
    merge_annovar_genom_with_ruxandra()
    # plot_ms_VIPUR_classification_all_variants()
    # plot_ms_VIPUR_score_test2()
    # plot_ms_VIPUR_classification_test2()
    # plot_ms_VIPUR_classification_test1()