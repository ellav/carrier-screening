import math
import matplotlib.pyplot as plt
from matplotlib.pyplot import figure
from matplotlib import rc
import numpy as np
import pandas as pd
from sklearn.linear_model import LinearRegression
from scipy.stats import linregress
# from sys import exit


class Parameter:
    def __init__(self, f, cadd, n, dis=0):
        self.f = f
        self.cadd = cadd
        self.n = n
        self.dis = dis

    def print_value(self):
        print(self.f, self.cadd, self.n, self.dis)


# ------------------------------------------------------------------


# def gene_to_csv(dataframe):
#     path = '/home/selene/Programm/Diss/' + 'Gene_name.csv'
#     col = dataframe['Gene'].unique()
#     pd.DataFrame(col).to_csv(path)
#     return


def csv_to_pd(file_csv):
    file = pd.read_csv(file_csv, sep=',')
    file.columns = (file.columns.str.strip()
                    .str.replace(' ', '_')
                    .str.replace(':', '')
                    .str.replace('/', '_'))
    return file


def get_disorder_list():
    csv_file_with_disorder = 'Gene_with_disorder.csv'
    gene_disorder_pd = csv_to_pd(csv_file_with_disorder)
    gene_disorder_pd = gene_disorder_pd.drop(['Genes', 'Unnamed_1',
                                              'A_in_right_gene',
                                              'B_in_right_gene'], axis=1)
    # Drop first row with contains total, and drop FAM161A
    # which was a duplicated row
    gene_disorder_pd = gene_disorder_pd.drop([0, 74], axis=0)
    disorder = list(gene_disorder_pd)
    return (disorder[1:], gene_disorder_pd)


def affected_gene_per_disorder(gene_disorder_pd, disorder):
    # returns the affected gene for a disease
    gene_disorder_pd = gene_disorder_pd.dropna(subset=[disorder], axis=0)
    affected_gene = gene_disorder_pd['gene_used']
    return affected_gene


def df_for_disorder(dataframe, affected_gene):
    # returns DataFrame which only contains the affected gene
    result = pd.DataFrame()
    for gene in affected_gene:
        contain_gene = dataframe.loc[dataframe['Gene'] == gene]
        result = result.append(contain_gene)
    return result


def find_carrier_rate(dataframe, param):
    dataframe = dataframe.loc[dataframe['AF'] == param.f]
    dataframe = dataframe.loc[dataframe['CADD'] == param.cadd]
    dataframe = dataframe.loc[dataframe['n'] == param.n]
    print(dataframe)
    return dataframe


# --------------------------------------------------------------------------------


def clear_pathogenic(dataframe):
    # print("There are the follwoin Mutation Functions: ")
    # print(*np.unique(dataframe['Function'].dropna()), sep=', ')
    list_pathogenic = ['Nonsense', 'Frameshift', 'Splice']
    result = pd.DataFrame()
    for item in list_pathogenic:
        end = dataframe.loc[dataframe['Function'] == item]
        result = result.append(end)
    # Returns the rest dataframe, without the clearly pathogentic mutations
    for item in list_pathogenic:
        dataframe = dataframe.loc[dataframe['Function'] != item]
    return result, dataframe


def allel_frequency(dataframe, freq):
    AF_name = 'INFO_AF'
    # print_string(dataframe[AF_name])
    dataframe.loc[:, AF_name] = pd.to_numeric(
        dataframe[AF_name], errors='coerce')
    # fill NaN with 0
    allel_pd = dataframe.fillna(value={AF_name: 0})
    end = allel_pd.loc[allel_pd[AF_name] < freq]
    return end


def CADD(dataframe, cutOff):
    # Mute the warnings
    pd.options.mode.chained_assignment = None
    CADD_name = 'CADD_phred'
    CADD_pd = dataframe.dropna(subset=[CADD_name])
    CADD_numeric = CADD_pd[CADD_name].apply(pd.to_numeric)
    CADD_pd[CADD_name] = CADD_numeric
    end = CADD_pd.loc[CADD_pd[CADD_name] > cutOff]
    return end


def Polyphen2_HVAR(dataframe):
    # D (Probably Damaging), P (Possibly Damaging), B (Benign)
    name = 'Polyphen2_HVAR_pred'
    res_column = dataframe[name]
    res_column = res_column.replace('D', 1)
    res_column = pd.to_numeric(res_column, errors='coerce').fillna(value=0)
    dataframe.loc[:, 'Polyphen2_HVAR_num'] = res_column
    return dataframe


def LRT(dataframe):
    # D (Deleterious), N (Neutral), U (Unkown)
    name = 'LRT_pred'
    res_column = dataframe[name]
    res_column = res_column.replace('D', 1)
    res_column = pd.to_numeric(res_column, errors='coerce').fillna(value=0)
    dataframe.loc[:, 'LRT_num'] = res_column
    return dataframe


def MutationTaster(dataframe):
    # A (Disease causing automatic), D (Disease causing)
    # N (Polymorphismus), P (Polymorphismus automatic)
    name = 'MutationTaster_pred'
    res_column = dataframe[name]
    res_column = res_column.replace('A', 1)
    res_column = pd.to_numeric(res_column, errors='coerce').fillna(value=0)
    dataframe.loc[:, 'MutationTaster_num'] = res_column
    return dataframe


def MutationAssessor(dataframe):
    # L (low functional), N (neutral functional)
    # H (high functional), M (medium functional)
    name = 'MutationAssessor_pred'
    res_column = dataframe[name]
    res_column = res_column.replace('H', 1)
    res_column = pd.to_numeric(res_column, errors='coerce').fillna(value=0)
    dataframe.loc[:, 'MutationAssessor_num'] = res_column
    return dataframe


def SIFT(dataframe):
    # Damaging (D), Tolerated (T)
    name = 'SIFT_pred'
    res_column = dataframe[name]
    res_column = res_column.replace('D', 1)
    res_column = pd.to_numeric(res_column, errors='coerce').fillna(value=0)
    dataframe.loc[:, 'SIFT_num'] = res_column
    return dataframe


def FATHMM(dataframe):
    # Damaging (D), Tolerated (T)
    name = 'FATHMM_pred'
    res_column = dataframe[name]
    res_column = res_column.replace('D', 1)
    res_column = pd.to_numeric(res_column, errors='coerce').fillna(value=0)
    dataframe.loc[:, 'FATHMM_num'] = res_column
    return dataframe


def M_CAP(dataframe):
    # Damaging (D), Tolerated (T)
    name = 'M-CAP_pred'
    res_column = dataframe[name]
    res_column = res_column.replace('D', 1)
    res_column = pd.to_numeric(res_column, errors='coerce').fillna(value=0)
    dataframe.loc[:, 'M_CAP_num'] = res_column
    return dataframe


def Provean(dataframe):
    # Damaging (D), Neutral (N)
    name = 'PROVEAN_pred'
    res_column = dataframe[name]
    res_column = res_column.replace('D', 1)
    res_column = pd.to_numeric(res_column, errors='coerce').fillna(value=0)
    dataframe.loc[:, 'Provean_num'] = res_column
    return dataframe


def at_least_n_pathogenic(dataframe, n):
    path_pred = 8
    dataframe_res = Polyphen2_HVAR(dataframe)
    dataframe_res = LRT(dataframe_res)
    dataframe_res = MutationTaster(dataframe_res)
    dataframe_res = MutationAssessor(dataframe_res)
    dataframe_res = SIFT(dataframe_res)
    dataframe_res = FATHMM(dataframe_res)
    dataframe_res = M_CAP(dataframe_res)
    dataframe_res = Provean(dataframe_res)

    dataframe_res['prediction_software_num'] = 0
    sum_column = dataframe_res.iloc[:, -(path_pred+1):-1].sum(axis=1)
    # make_hist_plot(sum_column)
    dataframe_res['prediction_software_num'] = sum_column
    dataframe_n_muations = dataframe_res.loc[
        dataframe_res['prediction_software_num'] >= n]
    return dataframe_n_muations


def at_least_n_pathogenic_no_MCAP(dataframe, n):
    # Without the M-Cap
    path_pred = 7
    dataframe_res = Polyphen2_HVAR(dataframe)
    dataframe_res = LRT(dataframe_res)
    dataframe_res = MutationTaster(dataframe_res)
    dataframe_res = MutationAssessor(dataframe_res)
    dataframe_res = SIFT(dataframe_res)
    dataframe_res = FATHMM(dataframe_res)
    dataframe_res = Provean(dataframe_res)

    dataframe_res.loc[:, 'prediction_software_num'] = 0
    sum_column = dataframe_res.iloc[:, -(path_pred+1):-1].sum(axis=1)
    # make_hist_plot(sum_column)
    dataframe_res.loc[:, 'prediction_software_num'] = sum_column
    dataframe_n_muations = dataframe_res.loc[
        dataframe_res['prediction_software_num'] >= n]
    dataframe_rest = dataframe_res.loc[
        dataframe_res['prediction_software_num'] < n]
    return dataframe_n_muations, dataframe_rest


def MCAP_CADD_sep(dataframe, param):
    # dataframe already without the clearly pathogenic calls
    # dataframe already with AF > 0.4
    # 0.004, 100, 100: 4 prediction software (inkl > 20 CADD) and M-CAP
    # 0.004, 200, 200: 3 prediction software + CADD > 20 + M-CAP

    if param.cadd == 100 and param.n == 100:
        # give right value to cadd
        cadd = 20
        dataframe_MCAP = M_CAP(dataframe)
        result_MCAP = dataframe_MCAP.loc[dataframe_MCAP['M_CAP_num'] == 1]

        # either 4 prediction softwares
        (result_software_4, rest) = at_least_n_pathogenic_no_MCAP(
            result_MCAP, 4)
        # or 3 prediction softwares and CADD > 20
        (result_software_3, bla) = at_least_n_pathogenic_no_MCAP(rest, 3)
        result_cadd_3 = CADD(result_software_3, cadd)
        result_software_CADD = result_software_4.append(result_cadd_3)

    elif param.cadd == 200 and param.n == 200:
        # give right value to cadd
        cadd = 20
        dataframe_MCAP = M_CAP(dataframe)
        result_MCAP = dataframe_MCAP.loc[dataframe_MCAP['M_CAP_num'] == 1]
        (result_software, bla) = at_least_n_pathogenic_no_MCAP(result_MCAP, 3)
        result_software_CADD = CADD(result_software, cadd)

    return result_software_CADD


def combine_aa_snp(dataframe_aa_snp):

    def replace_splice(row):
        SNP = row['SNP_db_xref']
        AA = row['Amino_Acid_Change']
        if AA == 'Splice':
            return SNP
        else:
            return AA

    return dataframe_aa_snp.apply(replace_splice, axis=1)


def adapt_spreadsheet(dataframe):
    if dataframe.shape[0] == 0:
        return dataframe

    counters = {}
    column_list = ['Amino_Acid_Change', 'SNP_db_xref']
    aa_pos_added = (
        combine_aa_snp(dataframe[column_list]).astype(str) +
        '__' + dataframe['Pos'].astype(str))
    for element in aa_pos_added:
        counters.setdefault(element, 0)
        counters[element] = counters[element] + 1

    dataframe.loc[:, 'AA_or_SNP__Pos'] = aa_pos_added

    def freq_of_variant_yellow(element, dic):
        if dic[element] > 2:
            return 1
        return 0

    def freq_of_variant_green(element, dic):
        if dic[element] == 2:
            return 1
        return 0

    dataframe.loc[:, 'Yellow_Value'] = dataframe[
        'AA_or_SNP__Pos'].apply(
            freq_of_variant_yellow, dic=counters)
    dataframe.loc[:, 'Green_value'] = dataframe[
        'AA_or_SNP__Pos'].apply(
            freq_of_variant_green, dic=counters)

    def clinvar(elements, criteria):
        if elements in criteria:
            return 1
        return 0

    benign_crit = [
        'Benign', 'Likely_pathogenic', 'Benign/Likely_benign']
    unknown_crit = [
        'Conflicting_interpretations_of_pathogenicity',
        'Uncertain_significance']
    patho_crit = [
        'pathogenic', 'Likely_pathogenic',
        'Pathogenic/Likely_pathogenic']

    dataframe.loc[:, 'ClinVar_red'] = dataframe[
        'Clinvar_Significance'].apply(
            clinvar, criteria=benign_crit)
    dataframe.loc[:, 'ClinVar_bold'] = dataframe[
        'Clinvar_Significance'].apply(
            clinvar, criteria=unknown_crit)
    dataframe.loc[:, 'ClinVar_italic'] = dataframe[
        'Clinvar_Significance'].apply(
            clinvar, criteria=patho_crit)

    return dataframe

# --------------------------------------------


def heterozygot_trait(dataframe_patho, param):
    if dataframe_patho.shape[0] == 0:
        print('No variants')
    dis = param.dis
    if dataframe_patho.shape[0] == 0:
        print("0 %% are heteroyzgots carrier.")
        return 0
    number_of_all_patient = 399
    number_of_patho_patient = len(dataframe_patho['patient_id'].unique())
    res = float(number_of_patho_patient/number_of_all_patient)*100
    if dis == 0:
        print("%f %% are heteroyzgots carrier." % (res))
    elif dis != 0:
        print(res)
        return res
    return


def genes_mut_per_pat(dataframe_patho, dataframe_full, param):
    if dataframe_patho.shape[0] == 0:
        print('No variants')
        return
    number_of_variants = []
    number_of_variants_patho = []
    patient_id = np.unique(dataframe_full['patient_id'])
    for i in patient_id:
        res = dataframe_full.loc[
            dataframe_full['patient_id'].astype(int) == i]
        res_patho = dataframe_patho.loc[
            dataframe_patho['patient_id'].astype(int) == i]
        res_s = len(res['Gene'].unique())
        res_s_patho = len(res_patho['Gene'].unique())
        number_of_variants.append(res_s)
        number_of_variants_patho.append(res_s_patho)
    average = float(sum(number_of_variants)/len(number_of_variants))
    average_patho = float(
        sum(number_of_variants_patho)/len(number_of_variants_patho))
    print(("%f is the average number of genes with variants "
           "per patient. IGNORING if it is pathologie/benign" % average))
    print(("%f is the average number of genes with pathogenic "
           "variants per patient." % average_patho))
    return


def find_BBS1(dataframe):
    if dataframe.shape[0] == 0:
        print('No variants')
        return 0
    name = 'BBS1'
    res = dataframe.loc[dataframe['Gene'] == name]
    print('There are %i of BBS1 mutation' % res.shape[0])
    return res


# ----------------------------------------------------

def vairiants_per_patient(dataframe_patho, dataframe_full, param):
    if dataframe_patho.shape[0] == 0:
        print('No variants')
        return

    rc('font')
    plt.rcParams.update({'font.size': 15})
    plt.xticks(fontsize=15)
    plt.yticks(fontsize=15)
    number_of_variants = []
    patient_id = np.unique(dataframe_full['patient_id'])
    for i in patient_id:
        res = dataframe_patho.loc[
            dataframe_patho['patient_id'].astype(int) == i]
        res_s = res.shape[0]
        number_of_variants.append(res_s)
    plt.ylabel('Number of patients')
    plt.xlabel('Number of variants')
    xint = range(min(number_of_variants),
                 math.ceil(max(number_of_variants)) + 1)
    plt.xticks(xint)
    make_hist_plot(
        number_of_variants, 'variants_per_patient.png', param)
    return


def m_variants_in_same_gene_per_patient(dataframe_patho,
                                        dataframe_full, param):
    if dataframe_patho.shape[0] == 0:
        print('No variants')
        return

    rc('font')
    plt.rcParams.update({'font.size': 18})
    plt.xticks(fontsize=18)
    plt.yticks(fontsize=18)
    m = 2
    number_of_variants = []
    patient_id = np.unique(dataframe_full['patient_id'])
    more_than_2_variants = pd.DataFrame()
    for i in patient_id:
        res = dataframe_patho.loc[dataframe_patho['patient_id'] == i]
        count_per_patient = 0
        if res.shape[0] >= 2:
            genes = res['Gene'].unique()
            for gene in genes:
                same_gene_mutation = res.loc[res['Gene'] == gene]
                if same_gene_mutation.shape[0] >= m:
                    count_per_patient = count_per_patient + 1
                    if same_gene_mutation.shape[0] >= 3:
                        more_than_2_variants = more_than_2_variants.append(
                            same_gene_mutation)
        number_of_variants.append(count_per_patient)
    print(more_than_2_variants)
    plt.ylabel('Number of patients')
    plt.xlabel('Numbers of genes which have at least'
               ' 2 variants in same gene')
    xint = range(min(number_of_variants),
                 math.ceil(max(number_of_variants)) + 1)
    plt.xticks(xint)
    print(sum(number_of_variants))
    make_hist_plot(number_of_variants,
                   'm_variants_in_same_gene_per_patient.png', param)
    return


def distribution_per_gene(dataframe_patho, param):
    if dataframe_patho.shape[0] == 0:
        print('No variants')
        return

    figure(num=None, figsize=(100, 30))
    plt.rcParams.update({'font.size': 70})
    plt.ylabel('Number of variants')
    # plt.xticks(fontsize='7')
    plt.xticks(fontsize=70, rotation=90)
    make_hist_plot_rot(dataframe_patho['Gene'],
                       'distribution_per_gene.png', param)
    return


def distribution_per_gene_percent(
        dataframe_patho, dataframe_full, param):

    if dataframe_patho.shape[0] == 0:
        print('No variants')
        return

    figure(num=None, figsize=(100, 30))
    plt.rcParams.update({'font.size': 70})
    number_of_all_patient = len(dataframe_full['patient_id'].unique())
    df_row = dataframe_patho['Gene']
    hist = {}
    for i in df_row:
        hist[i] = hist.get(i, 0) + 100/number_of_all_patient
    plt.bar(x=list(hist.keys()), height=list(hist.values()))
    # path_plot = (path + str(param.f) + '_' + str(param.cadd) + '_' +
    #              str(param.n) + '_distribution_per_gene_percent.png')
    plt.xticks(rotation=90)
    plt.ylabel('Number of variant in percentage of all patients [%]')
    # plt.savefig(path_plot, bbox_inches='tight')
    plt.show()
    plt.clf()
    plt.close()
    return


def type_of_function(dataframe_patho, param):
    if dataframe_patho.shape[0] == 0:
        print('No variants')
        return

    rc('font')
    plt.rcParams.update({'font.size': 15})
    plt.xticks(fontsize=15, rotation=90)
    plt.yticks(fontsize=15)
    plt.ylabel('Number of variants')
    make_hist_plot_rot(dataframe_patho['Function'],
                       'type_of_function.png', param)
    return


def type_of_mutation(dataframe_patho, param):
    if dataframe_patho.shape[0] == 0:
        print('No variants')
        return

    title = 'Mutation_Call_HGVS_Coding'
    plt.xticks(rotation=90)
    make_hist_plot_rot(dataframe_patho[title],
                       'type_of_mutation.png', param)
    # mutations = dataframe_patho[title].unique()
    # for mut in mutations:
    #     all_mut = dataframe_patho.loc[dataframe_patho[title] == mut]
    #     if all_mut.shape[0] >= 3:
    #         print(mut)
    return


def typ_of_function_per_gene(dataframe_patho, param):
    if dataframe_patho.shape[0] == 0:
        print('No variants')
        return

    dis = param.dis
    genes = dataframe_patho['Gene'].unique()
    print(genes.shape)
    if dis == 0:
        length = 100
        fontsize = 70
    elif dis != 0:
        length = genes.shape[0]*2
        print(length)
        fontsize = 90
    figure(num=None, figsize=(length, 30))
    plt.rcParams.update({'font.size': fontsize})
    # y-axis in bold
    function_gene = ['Missense', 'Synonymous', 'Noncoding',
                     'Nonsense', 'Frameshift', 'Splice']

    rc('font')

    # Values of each group
    bars_missense = []
    bars_synomymus = []
    bars_noncoding = []
    bars_nonsense = []
    bars_frameshift = []
    bars_splice = []
    for gene in genes:
        res = dataframe_patho.loc[dataframe_patho['Gene'] == gene]
        miss = res.loc[res['Function'] == 'Missense']
        bars_missense.append(miss.shape[0])
        syn = res.loc[res['Function'] == 'Synonymous']
        bars_synomymus.append(syn.shape[0])
        nc = res.loc[res['Function'] == 'Noncoding']
        bars_noncoding.append(nc.shape[0])
        ns = res.loc[res['Function'] == 'Nonsense']
        bars_nonsense.append(ns.shape[0])
        fram = res.loc[res['Function'] == 'Frameshift']
        bars_frameshift.append(fram.shape[0])
        spl = res.loc[res['Function'] == 'Splice']
        bars_splice.append(spl.shape[0])

    # The position of the bars on the x-axis
    r = list(range(genes.shape[0]))

    # Names of group and bar width
    names = genes
    barWidth = 1
    cmap = plt.cm.coolwarm

    # Create brown bars
    plt.bar(r, bars_missense, color=cmap(0), edgecolor='white', width=barWidth)

    # Create green bars (middle), on top of the firs ones
    plt.bar(r, bars_synomymus, bottom=bars_missense, color=cmap(0.2),
            edgecolor='white', width=barWidth)
    # Create green bars (top)
    bars_miss_syn = [x+y for x, y in zip(bars_missense, bars_synomymus)]
    plt.bar(r, bars_noncoding, bottom=bars_miss_syn, color=cmap(.4),
            edgecolor='white', width=barWidth)
    # Creat next bats
    bars_miss_syn_nc = [x+y for x, y in zip(bars_miss_syn, bars_noncoding)]
    plt.bar(r, bars_nonsense, bottom=bars_miss_syn_nc, color=cmap(0.6),
            edgecolor='white', width=barWidth)
    # Creaat next bar
    bars_miss_syn_nc_ns = [x+y for x, y in zip(bars_miss_syn_nc,
                                               bars_nonsense)]
    plt.bar(r, bars_frameshift, bottom=bars_miss_syn_nc_ns, color=cmap(0.8),
            edgecolor='white', width=barWidth)
    # Creat last bar
    bars_miss_syn_nc_ns_fram = [x+y for x, y in zip(bars_miss_syn_nc_ns,
                                                    bars_frameshift)]
    plt.bar(r, bars_splice, bottom=bars_miss_syn_nc_ns_fram, color=cmap(0.99),
            edgecolor='white', width=barWidth)

    # Custom X axis
    plt.xticks(r, names, rotation=90)
    plt.ylabel('Number of Variants')
    plt.legend(function_gene, loc="upper right", prop={'size': 55})
    if dis != 0:
        plt.title(dis)

    # Save and show graphic
    # if dis == 0:
    #     plt.savefig(path + str(param.f) + '_' + str(param.cadd) + '_' +
    #                 str(param.n) + '_' + 'type_of_function_per_gene.png',
    #                 bbox_inches='tight')
    # else:
    #     plt.savefig(path + '/' + str(dis) + '/' + str(param.f) + '_' +
    #                 str(param.cadd) +  '_' + str(param.n) + '_' +
    #                 'type_of_function_per_gene.png', bbox_inches='tight')
    print("end")
    plt.show()
    plt.clf()
    plt.close()
    return


def make_hist_plot(df_row, name, param):
    hist = {}
    for i in df_row:
        hist[i] = hist.get(i, 0) + 1
    plt.bar(x=list(hist.keys()), height=list(hist.values()))
    # path_plot = (path + str(param.f) + '_' + str(param.cadd) + '_' +
    #              str(param.n) + '_' + name)
    # plt.savefig(path_plot, bbox_inches='tight')
    plt.show()
    plt.clf()
    plt.close()
    return


def make_hist_plot_rot(df_row, name, param):
    # plt.rcParams.update({'font.size': 45})
    hist = {}
    for i in df_row:
        hist[i] = hist.get(i, 0) + 1
    plt.bar(x=list(hist.keys()), height=list(hist.values()))
    # path_plot = (path + str(param.f) + '_' + str(param.cadd) + '_' +
    #              str(param.n) + '_' + name)
    # plt.savefig(path_plot, bbox_inches='tight')
    plt.show()
    plt.clf()
    plt.close()
    return


# ----------------------------------------------------

def damaging_variant(dataframe, param):
    if damaging.shape[0] == 0:
        print('No variants')
        return

    if dataframe.shape[0] == 0:
        return dataframe

    cadd = param.cadd
    n = param.n
    (result_pathogenic, dataframe_rest) = clear_pathogenic(dataframe)

    # get result_AF has at least AF of f
    result_AF = allel_frequency(dataframe_rest, param.f)

    if cadd == 100 and n == 100:
        result_cadd_n = MCAP_CADD_sep(result_AF, param)
    elif cadd == 200 and n == 200:
        result_cadd_n = MCAP_CADD_sep(result_AF, param)
    else:
        if cadd != 0 and n != 0:
            result_prediction_software = at_least_n_pathogenic(
                result_AF, n)
            result_cadd_n = CADD(result_prediction_software, cadd)
        elif cadd != 0 and n == 0:
            result_cadd_n = CADD(result_AF, cadd)
        elif cadd == 0 and n != 0:
            result_cadd_n = at_least_n_pathogenic(
                result_AF, n)
        elif cadd == 0 and n == 0:
            result_cadd_n = pd.DataFrame()

    result_end = result_cadd_n.append(
        result_pathogenic, sort=False)

    return result_end


def more_then_2_mut_per_pat(dataframe_patho, param):
    # magic_num == 0: more then 2 variants per patient
    # magic_num == 1: more then 2 variants per gene per patient
    magic_num = 0
    m = 2
    number_of_variants = []
    more_2_var = pd.DataFrame()
    patient_id = np.unique(dataframe_patho['patient_id'])
    for i in patient_id:
        res = dataframe_patho.loc[dataframe_patho['patient_id'] == i]
        if magic_num == 0:
            if res.shape[0] >= m:
                more_2_var = more_2_var.append(res)
        count_per_patient = 0
        if res.shape[0] >= 2:
            genes = res['Gene'].unique()
            for gene in genes:
                same_gene_mutation = res.loc[res['Gene'] == gene]
                if same_gene_mutation.shape[0] >= m:
                    if magic_num == 1:
                        more_2_var = more_2_var.append(same_gene_mutation)
                    count_per_patient = count_per_patient + 1
        number_of_variants.append(count_per_patient)
    more_2_var['AF_lim'] = param.f
    more_2_var['CADD_lim'] = param.cadd
    more_2_var['n_lim'] = param.n
    return (sum(number_of_variants), more_2_var)


def more_2_variants_spreadsheet(damaging, param):
    if damaging.shape[0] == 0:
        print('No variants')
        return

    # dis = param.dis
    (num, var_2_df) = more_then_2_mut_per_pat(damaging, param)
    # if dis != 0:
    #     name_beg = 'spreadsheet/' + dis
    # if dis == 0:
    #     name_beg = 'spreadsheet/' + 'ciliopathy'
    # file_name = (name_beg + '_more_than_2_var_' + str(param.f) + '_' +
    #              str(param.cadd) + '_' + str(param.n) + '.csv')

    var_2_df = adapt_spreadsheet(var_2_df)
    # var_2_df.to_csv(file_name, sep='\t')
    print(var_2_df)
    return


def damaging_variants_spreadsheet(damaging, param):
    if damaging.shape[0] == 0:
        print('No variants')
        return
    # dis = param.dis
    # if dis != 0:
    #     name_beg = 'spreadsheet/' + dis
    # if dis == 0:
    #     name_beg = 'spreadsheet/' + 'ciliopathy'
    # file_name = (name_beg + '_damaging_variants_' + str(param.f) +
    #              '_' + str(param.cadd) + '_' + str(param.n) + '.csv')

    damaging = adapt_spreadsheet(damaging)
    print(damaging)
    # damaging.to_csv(file_name, sep='\t')
    return


def ex_1_mut_per_gene(damaging, param):
    if damaging.shape[0] == 0:
        print('No variants')
        return
    dis = param.dis
    patient_list = np.unique(damaging['patient_id'])
    ex_1_var = []
    ex_1_var_pd = pd.DataFrame()
    for pat in patient_list:
        num_of_single_var = 0
        variants_of_pat = damaging.loc[damaging['patient_id'] == pat]
        gene_list = variants_of_pat['Gene'].unique()
        for gene in gene_list:
            variants_gene = variants_of_pat.loc[
                variants_of_pat['Gene'] == gene]
            if variants_gene.shape[0] == 1:
                ex_1_var_pd = ex_1_var_pd.append(variants_gene)
                num_of_single_var = num_of_single_var + 1
        ex_1_var.append(num_of_single_var)

    plt.xlabel('Number of genes with exaclty one variant')
    plt.ylabel('Patients')
    if dis == 0:
        name = 'exactly_1_variant_per_gene.png'
    else:
        name = dis + '_exactly_1_variant_per_gene.png'
    make_hist_plot(ex_1_var, name, param)
    ex_1_var_pd = adapt_spreadsheet(ex_1_var_pd)
    # ex_1_var_pd.to_csv(name_spreadsheet, sep='\t')
    print(ex_1_var_pd)
    return

# ------------------------


def CADD_MCAP_corr(dataframe):
    CADD_name = 'CADD_phred'
    MCAP_name = 'M-CAP_score'

    x = dataframe[CADD_name].fillna(0)
    y = dataframe[MCAP_name].fillna(0)
    slope, intercept, r_value, p_value, std_err = linregress(x, y)

    x = x.values.reshape(-1, 1)
    y = y.values.reshape(-1, 1)

    linear_regressor = LinearRegression()  # create object for the class
    linear_regressor.fit(x, y)  # perform linear regression
    y_pred = linear_regressor.predict(x)  # make predictions

    fig, ax = plt.subplots()
    ax.plot(x, y_pred, color='red')
    ax.scatter(x, y)

    text = ('Correlation Coefficient = %.2f \n'
            'p-value = %.4E' % (r_value, p_value))
    props = dict(boxstyle='round', facecolor='white', alpha=0.5)
    ax.text(0.05, 0.95, text, transform=ax.transAxes, fontsize=14,
            verticalalignment='top', bbox=props)

    ax.tick_params(labelsize=14)
    ax.set_xlabel('CADD', fontsize=14)
    ax.set_ylabel('M-CAP score', fontsize=14)

    # path = '/home/selene/Programm/Diss/' + 'correlation_CADD_MCAP.png'
    # plt.savefig(path, bbox_inches='tight')

    plt.show()
    plt.clf()
    plt.close()
    return


if __name__ == '__main__':
    folder_path = '/home/selene/Programm/Diss/' + 'calls_generated.csv'
    true_calls = pd.read_csv(folder_path)
    (disorder, gene_disorder_pd) = get_disorder_list()
    # disorder = ['Joubert', 'Bardet-Biedl_syndrome']

    dis = 0
    n = 100
    cadd = 100
    AF = 0.004

    param = Parameter(AF, cadd, n, dis)
    if dis != 0:
        affected_gene = affected_gene_per_disorder(gene_disorder_pd, dis)
        true_calls = df_for_disorder(true_calls, affected_gene)

    damaging = damaging_variant(true_calls, param)

    # testing :)--------------------------------
    param.print_value()

    damaging_variants_spreadsheet(damaging, param)
    ex_1_mut_per_gene(damaging, param)
    more_2_variants_spreadsheet(damaging, param)

    # vairiants_per_patient(damaging, true_calls, param)
    # m_variants_in_same_gene_per_patient(damaging, true_calls, param)
    # distribution_per_gene(damaging, param)
    # distribution_per_gene_percent(damaging, true_calls, param)
    # type_of_function(damaging, param)
    # type_of_mutation(damaging, param)
    # typ_of_function_per_gene(damaging, param)

    # heterozygot_trait(damaging, param)
    # genes_mut_per_pat(damaging, true_calls, param)
    # find_BBS1(damaging)
