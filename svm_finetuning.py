import numpy as np
import pandas as pd

from sklearn.preprocessing import LabelBinarizer
from sklearn.ensemble import RandomForestClassifier
from sklearn.model_selection import train_test_split, GridSearchCV, StratifiedKFold
from sklearn.metrics import roc_curve, precision_recall_curve, auc, make_scorer, recall_score, accuracy_score, precision_score, confusion_matrix
from sklearn import svm


import matplotlib.pyplot as plt
plt.style.use("ggplot")


csv_data_training = 'alldata.csv'  # the train/ test data

# Get the features for training and testing
train_test_Pandas = pd.read_csv(csv_data_training, sep=',')
train_test_Pandas.columns = (train_test_Pandas.columns.str.strip()
                             .str.replace(' ', '_')
                             .str.replace(':', ''))
train_test_Pandas = train_test_Pandas[['Truth', 'Coverage', 'Alt%']]
df = train_test_Pandas



# by default majority class (benign) will be negative
lb = LabelBinarizer()
df['Truth'] = lb.fit_transform(df['Truth'].values)
targets = df['Truth']

df.drop(['Truth'], axis=1, inplace=True)

X_train, X_test, y_train, y_test = train_test_split(df, targets, stratify=targets)

print(X_test)


# show the distribution
print('y_train class distribution')
print(y_train.value_counts(normalize=True))
print('y_test class distribution')
print(y_test.value_counts(normalize=True))


# clf = RandomForestClassifier(n_jobs=-1)

# param_grid = {
#     'min_samples_split': [3, 5, 10], 
#     'n_estimators' : [100, 300],
#     'max_depth': [3, 5, 15, 25],
#     'max_features': [3, 5, 10, 20]
# }

# scorers = {
#     'precision_score': make_scorer(precision_score),
#     'recall_score': make_scorer(recall_score),
#     'accuracy_score': make_scorer(accuracy_score)
# }

c = 100000
clf = svm.LinearSVC(C=c, class_weight='balanced', max_iter=10000000)


def grid_search_wrapper(refit_score='precision_score'):
    """
    fits a GridSearchCV classifier using refit_score for optimization
    prints classifier performance metrics
    """
    skf = StratifiedKFold(n_splits=10)
    grid_search = GridSearchCV(clf, param_grid, scoring=scorers, refit=refit_score,
                           cv=skf, return_train_score=True, n_jobs=-1)
    grid_search.fit(X_train.values, y_train.values)

    # make the predictions
    y_pred = grid_search.predict(X_test.values)

    print('Best params for {}'.format(refit_score))
    print(grid_search.best_params_)

    # confusion matrix on the test data.
    print('\nConfusion matrix of Random Forest optimized for {} on the test data:'.format(refit_score))
    print(pd.DataFrame(confusion_matrix(y_test, y_pred),
                 columns=['pred_neg', 'pred_pos'], index=['neg', 'pos']))
    return grid_search


# grid_search_clf = grid_search_wrapper(refit_score='precision_score')


# results = pd.DataFrame(clf.cv_results_)
# results = results.sort_values(by='mean_test_precision_score', ascending=False)


y_scores = clf.predict_proba(X_test)[:, 1]

# for classifiers with decision_function, this achieves similar results
# y_scores = classifier.decision_function(X_test)


p, r, thresholds = precision_recall_curve(y_test, y_scores)

print(p)