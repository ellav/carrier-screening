import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
from damaging_filter import (clear_pathogenic, allel_frequency, CADD,
                             at_least_n_pathogenic, MCAP_CADD_sep,
                             adapt_spreadsheet, ClinVar_separation)



def more_then_2_mut_per_pat(dataframe_patho, param):
    # magic_num == 0: more then 2 variants per patient
    # magic_num == 1: more then 2 variants per gene per patient
    magic_num = 1
    m = 2
    number_of_variants = []
    more_2_var = pd.DataFrame()
    patient_id = np.unique(dataframe_patho['patient_id'])
    for i in patient_id:
        res = dataframe_patho.loc[dataframe_patho['patient_id'] == i]
        if magic_num == 0:
            if res.shape[0] >= m:
                more_2_var = more_2_var.append(res)
        count_per_patient = 0
        if res.shape[0] >= 2:
            genes = res['Gene'].unique()
            for gene in genes:
                same_gene_mutation = res.loc[res['Gene'] == gene]
                if same_gene_mutation.shape[0] >= m:
                    if magic_num == 1:
                        more_2_var = more_2_var.append(same_gene_mutation)
                    count_per_patient = count_per_patient + 1
        number_of_variants.append(count_per_patient)
    return (sum(number_of_variants), more_2_var)


def more_2_variants_spreadsheet(damaging, param):
    dis = param.dis
    param.print_value()
    (num, var_2_df) = more_then_2_mut_per_pat(damaging, param)

    file_name = param.path_spreadsheet('_more_than_2_var_')
    var_2_df = adapt_spreadsheet(var_2_df)
    var_2_df.to_csv(file_name, sep='\t')
    return


def damaging_variants_spreadsheet(damaging, param):
    dis = param.dis
    param.print_value()

    file_name = param.path_spreadsheet('_damaging_variants_')
    damaging = adapt_spreadsheet(damaging)
    damaging.to_csv(file_name, sep='\t')
    return


def ex_1_mut_per_gene(damaging, param):
    dis = param.dis
    param.print_value()
    patient_list = np.unique(damaging['patient_id'])
    ex_1_var_pd = pd.DataFrame()
    for pat in patient_list:
        variants_of_pat = damaging.loc[damaging['patient_id'] == pat]
        gene_list = variants_of_pat['Gene'].unique()
        for gene in gene_list:
            variants_gene = variants_of_pat.loc[
                variants_of_pat['Gene'] == gene]
            if variants_gene.shape[0] == 1:
                ex_1_var_pd = ex_1_var_pd.append(variants_gene)

    name_spreadsheet = param.path_spreadsheet('_exactly_1_var_per_gene_')
    ex_1_var_pd = adapt_spreadsheet(ex_1_var_pd)
    ex_1_var_pd.to_csv(name_spreadsheet, sep='\t')
    return


# map[key] -> value
def exist_more_m_diff_variants(dataframe, param, m_ss):
    if param.dis != 0:
        print('Only made for all ciliopathy, not specific diseases!')
        return

    counters = {}
    for element in dataframe['AA_or_SNP__Pos']:
        counters.setdefault(element, 0)
        counters[element] = counters[element] + 1

    counters_filtered_print = {k: v for k, v in counters.items() if v >= m_ss}
    # print('\n'.join(counters_filtered_print.keys()))

    frequent_variant_df = pd.DataFrame()
    for variant in list(counters_filtered_print.keys()):
        temp = dataframe.loc[dataframe['AA_or_SNP__Pos'] == variant]
        frequent_variant_df = frequent_variant_df.append(temp, sort=False)


    name = '_exist_at_least_' + str(m_ss) + '_variants_'
    path_ss = param.path_spreadsheet(name)
    frequent_variant_df = adapt_spreadsheet(frequent_variant_df)
    frequent_variant_df.to_csv(path_ss, sep='\t')
    return


def ex_2_mut_per_patient(damaging, param):
    dis = param.dis
    param.print_value()
    patient_list = np.unique(damaging['patient_id'])
    ex_2_var_pd = pd.DataFrame()
    for pat in patient_list:
        variants_of_pat = damaging.loc[damaging['patient_id'] == pat]
        if variants_of_pat.shape[0] == 2:
            ex_2_var_pd = ex_2_var_pd.append(variants_of_pat)

    name_spreadsheet = param.path_spreadsheet('_exactly_2_var_per_patient_')
    ex_2_var_pd = adapt_spreadsheet(ex_2_var_pd)
    ex_2_var_pd.to_csv(name_spreadsheet, sep='\t')
    print(len(ex_2_var_pd['patient_id'].unique()))
    return


def at_least_3_mut_per_patient(damaging, param):
    dis = param.dis
    param.print_value()
    patient_list = np.unique(damaging['patient_id'])
    ex_2_var_pd = pd.DataFrame()
    for pat in patient_list:
        variants_of_pat = damaging.loc[damaging['patient_id'] == pat]
        if variants_of_pat.shape[0] >= 3:
            ex_2_var_pd = ex_2_var_pd.append(variants_of_pat)
    if ex_2_var_pd.shape[0] != 0:
        print(len(ex_2_var_pd['patient_id'].unique()))
    else:
        print(0)
    name_spreadsheet = param.path_spreadsheet('_at_least_3_var_per_patient_')
    ex_2_var_pd = adapt_spreadsheet(ex_2_var_pd)
    ex_2_var_pd.to_csv(name_spreadsheet, sep='\t')
    return


def true_calls_ss(dataframe, param):
    if param.ch_eng == 0:
        name_beg = 'spreadsheet/ch_'
    elif param.ch_eng == 1:
        name_beg = 'spreadsheet_eng/eng_'
    if param.dis != 0:
        name_beg = name_beg + param.dis
    if param.dis == 0:
        name_beg = name_beg + 'ciliopathy'
    name_spreadsheet = (name_beg + '_true_calls.csv')

    dataframe = adapt_spreadsheet(dataframe)
    dataframe.to_csv(name_spreadsheet, sep='\t')
    return
