from class_def import Parameter
import itertools
import matplotlib.pyplot as plt
import pandas as pd
import numpy as np
import variant_filter as va
# from sys import exit



def more_then_2_mut_per_pat(dataframe_patho, param):
    m = 2
    number_of_variants = []
    more_2_var = pd.DataFrame()
    patient_id = np.unique(dataframe_patho['patient_id'])
    for i in patient_id:
        res = dataframe_patho.loc[dataframe_patho['patient_id'] == i]
        count_per_patient = 0
        if res.shape[0] >= 2:
            genes = res['Gene'].unique()
            for gene in genes:
                same_gene_mutation = res.loc[res['Gene'] == gene]
                if same_gene_mutation.shape[0] >= m:
                    count_per_patient = count_per_patient + 1
        number_of_variants.append(count_per_patient)
    return (sum(number_of_variants))


def bar_plot(dataframe, param_list):
    plt.clf()
    path = '/home/selene/Programm/Diss/'
    bin_value = []
    bin_name = []
    tabel_2_var_res = pd.DataFrame()
    for param in param_list:
        damaging = va.damaging_variant(dataframe, param)
        num = more_then_2_mut_per_pat(damaging, param)
        if param.acmg_only == 0:
            acmg = 'All'
        elif param.acmg_only == 1:
            acmg = 'ACMG'
        if param.cadd == 300:
            software_num = '0'
        elif param.cadd == 200:
            software_num = '3'
        elif param.cadd == 400:
            software_num = '2'
        elif param.cadd == 500:
            software_num = '4'
        elif param.cadd == 600:
            software_num = '5'
        else:
            name = "AF=%.3f. CADD=%i. n=%i, " % (param.f, param.cadd, param.n)
            name = name + acmg
        name = software_num + " prediciton software, " + acmg
        if param.acmg_only == 1:
            name = acmg

        bin_value.append(num)
        bin_name.append(name)

    if param_list[1].ch_eng == 0:
        path = path + 'plot/'
        nati = 'Swiss'
    elif param_list[1].ch_eng == 1:
        path = path + 'plot_eng/'
        nati = 'English'
    plt.gcf().subplots_adjust(bottom=0.5)
    plt.tight_layout()
    plt.bar(bin_name, bin_value)
    plt.xticks(rotation=90)

    title = 'Cases with >= 2 variants in same gene - ' + nati
    plt.title(title)
    plt.ylabel('Number of cases')
    plt.savefig(path + 'individuals_with_2_var_per_gene.png',
                bbox_inches='tight')
    return



# Plot Scatter plot with >= 2 variants per individuals
def scatter_2_var_per_gene_plot_n(dataframe, n):
    plt.clf()
    path = '/home/selene/Programm/Diss/plot/'
    # dataframe are the true calls
    freq = np.delete(np.linspace(0, 0.005, 10), 0)
    CADD_cutOff = np.linspace(10, 25, 8)
    plot_data = pd.DataFrame(columns=['x', 'y', 'number_of_variants'])
    for f in freq:
        print(f)
        result_AF = va.allel_frequency(dataframe, f)
        (result_pathogenic, dataframe_rest) = va.clear_pathogenic(result_AF)
        for cadd in CADD_cutOff:
            result_prediction_software = va.at_least_n_pathogenic(
                dataframe_rest, n)
            result_prediction_software_CADD = va.CADD(
                result_prediction_software, cadd)
            # result AF_prediciton = cleary pathogenic +
            # at least AF of f AND
            # n times classified as pathogenic AND
            # CADD>cadd
            result_AF_prediction_CADD = result_prediction_software_CADD.append(
                result_pathogenic, sort=False)
            param = Parameter(f, cadd, n)
            (color) = more_then_2_mut_per_pat(
                result_AF_prediction_CADD, param)
            plot_data = plot_data.append(
                {'x': f, 'y': cadd, 'number_of_variants': color},
                ignore_index=True)
    plt.scatter(plot_data['x'], plot_data['y'],
                c=plot_data['number_of_variants'], s=40, marker='s')
    plt.colorbar()
    plt.xlabel('Allel Frequency')
    plt.xlim(0, 0.005)
    plt.ylabel('CADD')
    plt.title(str(n) + ' pathogenic predictions')
    plt.savefig(path + 'more_then_2_variants_per_pat' +
                '_' + str(n) + '.png', bbox_inches='tight')
    # plt.show()
    return


if __name__ == "__main__":

    list_param = [Parameter(0.004, 200, 200, acmg_only=1),
                  Parameter(0.004, 200, 200, acmg_only=0),
                  Parameter(0.004, 300, 300, acmg_only=0),
                  Parameter(0.004, 400, 400, acmg_only=0),
                  Parameter(0.004, 500, 500, acmg_only=0),
                  Parameter(0.004, 600, 600, acmg_only=0),]

    # list_param = [Parameter(0.004, 0, 6),
    #               Parameter(0.004, 15, 0),
    #               Parameter(0.004, 15, 4),
    #               Parameter(0.004, 20, 4),
    #               Parameter(0.004, 100, 100),
    #               Parameter(0.004, 200, 200),
    #               Parameter(0.004, 300, 300),
    #               Parameter(0.004, 400, 400, acmg_only=0),
    #               Parameter(0.004, 400, 400, acmg_only=1),
    #               Parameter(0.004, 500, 500, acmg_only=0),
    #               Parameter(0.004, 500, 500, acmg_only=1),
    #               Parameter(0.004, 600, 600)]
    # list_param = [Parameter(0.005, 10, 0),
    #               Parameter(0.005, 15, 0),
    #               Parameter(0.005, 20, 0),
    #               Parameter(0.005, 25, 0),
    #               Parameter(0.001, 10, 0),
    #               Parameter(0.001, 15, 0),
    #               Parameter(0.001, 20, 0),
    #               Parameter(0.001, 25, 0),

    #               Parameter(0.005, 20, 4),
    #               Parameter(0.001, 20, 4),

    #               Parameter(0.005, 0, 4),
    #               Parameter(0.005, 0, 5),
    #               Parameter(0.005, 0, 6),
    #               Parameter(0.001, 0, 4),
    #               Parameter(0.001, 0, 5),
    #               Parameter(0.001, 0, 6)]

    ch_eng = 0
    folder_path = '/home/selene/Programm/Diss'
    if ch_eng == 0:
        pickel_filename = 'true_calls.pkl'
    elif ch_eng == 1:
        pickel_filename = 'true_calls_eng.pkl'
        for param in list_param:
            param.ch_eng = 1
    true_calls = pd.read_pickle(folder_path + '/' + pickel_filename)

    bar_plot(true_calls, list_param)
    exit(0)

    scatter_2_var_per_gene_plot_n(true_calls, 0)
    # scatter_2_var_per_gene_plot_n(true_calls, 1)
    # scatter_2_var_per_gene_plot_n(true_calls, 2)
    # scatter_2_var_per_gene_plot_n(true_calls, 3)
    scatter_2_var_per_gene_plot_n(true_calls, 4)
    # scatter_2_var_per_gene_plot_n(true_calls, 5)
    # scatter_2_var_per_gene_plot_n(true_calls, 6)

