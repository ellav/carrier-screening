import pandas as pd


# delete rows with "-" in Mutation_Call_HGVS_Coding
def del_dash(dataframe):
    new_data = dataframe.loc[dataframe['Mutation_Call_HGVS_Coding'] != '-']
    return new_data


# delete rows with ALMS1 In-Frame muatation
def del_ALMS1(dataframe):
    title = 'Mutation_Call_HGVS_Coding'
    # look only at dataframe with Gene = ALMS1
    new_data = dataframe.loc[dataframe['Gene'] == 'ALMS1']
    # keep only the Mutations_Call_HGVS_Coding at the right location
    df = new_data[new_data[title].str.contains('NM_015120.4:c.')]
    # double check if the Function is In-Frame
    df_n = df.loc[df['Function'] == 'In-Frame']
    res = pd.concat([dataframe, df_n]).drop_duplicates(keep=False)
    return res


# delete row with an POC1B Noncoding Mutations
def del_POC1B(dataframe):
    title = 'Mutation_Call_HGVS_Coding'
    new_data = dataframe.loc[dataframe['Gene'] == 'POC1B']
    df = new_data[new_data[title].str.contains('c.453-12delT')]
    res = pd.concat([dataframe, df]).drop_duplicates(keep=False)
    return res


# Change X Chromosom to 24
def X_to_24(dataframe):
    if dataframe['Chrom'].dtype == 'object':
        dataframe['Chrom'] = dataframe['Chrom'].replace({'X': 24, 'Y': 25})
    dataframe.loc[:, 'Chrom'] = pd.to_numeric(dataframe['Chrom'])
    return dataframe


def del_frameshift(dataframe):
    rm_dataframe = dataframe[(dataframe['patient_id'] == 42625)]
    rm_dataframe = rm_dataframe[rm_dataframe['Gene'] == 'RPGR']
    rm_dataframe = rm_dataframe[rm_dataframe['Function'] == 'Frameshift']
    rm_dataframe = rm_dataframe[rm_dataframe['Allele_Score'] == 0]
    tmp = dataframe[(dataframe['patient_id'] == 42625) & (dataframe['Function'] == 'Frameshift')]
    result = dataframe.append(rm_dataframe).drop_duplicates(keep=False)
    return result

def delete_wrong_gene(dataframe):
    # Genes that were not included in our filter
    list_gene = ['KAAG1', 'PUS3', 'TMEM204', 'LRRC19', 'C10ORF105', 'POC1B-GALNT4', 'C10ORF54', 'STX19']
    for gene in list_gene:
        dataframe = dataframe[dataframe['Gene'] != gene]
    return dataframe


def del_all(dataframe):
    # drop random column
    if 'Unnamed_52' in dataframe.columns:
        dataframe = dataframe.drop(columns=['Unnamed_52'])
    dataframe = delete_wrong_gene(dataframe)
    dataframe = X_to_24(dataframe)
    # drop multiple rows for framshift (with dash, and 
    res_noDash = del_dash(dataframe)
    res_noDash = del_frameshift(res_noDash)
    # drop row which have polymorphism in ALMS1
    res_noDash_noALSM1 = del_ALMS1(res_noDash)
    # drop row which have polymorphism in POC1B
    res_noDash_noALSM1_noPOC1B = del_POC1B(res_noDash_noALSM1)
    return res_noDash_noALSM1_noPOC1B
