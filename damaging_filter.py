import math
import pandas as pd
from sys import exit
import os
# from class_def import Parameter


# Columns: new
# [Index, Chrom, Pos, Coverage, Ref#(F;R), Alt#(F;R), Alt#,
# Alt%, Allele_Score, Overall_Score, Mutation_Call_HGVS_Coding,
# Function, Zygosity, Gene, Strand, CDS, Ambiguous_Gain_Penalty,
# Ambiguous_Loss_Penalty, Read_Balance(Counts),
# Read_Balance(Percentage), SNP_db_xref, Amino_Acid_Change,
# Comments, GERP++_NR, SIFT_score, SIFT_pred, Polyphen2_HVAR_score,
# Polyphen2_HVAR_pred, LRT_score, LRT_pred, MutationTaster_score,
# MutationTaster_pred, MutationAssessor_score, MutationAssessor_pred,
# FATHMM_score, FATHMM_pred, PROVEAN_score, PROVEAN_pred, MetaSVM_pred,
# MetaLR_pred, M-CAP_score, M-CAP_pred, CADD_phred, INFO_CAF, INFO_MAF,
# INFO_AC, INFO_AF, INFO_HOM, INFO_HEMI, INFO_CLNDN,
# Clinvar_Significance, Imported_dbSNP_ID, patient_id, prediction]


def print_string(df_colunn):
    df_colunn = df_colunn.dropna()
    float_col = pd.to_numeric(df_colunn, errors='coerce')
    print(df_colunn[float_col.isna()])
    return


def ClinVar_separation(dataframe):

    df_patho = dataframe.loc[dataframe['ClinVar_italic'] == 1]
    df_benign = dataframe.loc[dataframe['ClinVar_red'] == 1]

    df_rest = dataframe.loc[dataframe['ClinVar_italic'] == 0]
    df_rest = df_rest.loc[df_rest['ClinVar_red'] == 0]

    return (df_patho, df_benign, df_rest)


def HGMD_separation(dataframe, acmg_only):
    name = 'HGMD'
    if name not in list(dataframe):
        print('no HGMD found!!!!!!!!!!!')
        patho = pd.DataFrame()
        return (patho, dataframe)
    res_column = dataframe[name]

    dataframe['HGMD_num'] = dataframe[name].replace(['DM', 'DM?', 'FP', 'DP'], 1)

    dataframe['HGMD_num'] = pd.to_numeric(dataframe['HGMD_num'], errors='coerce').fillna(value=0)
    patho_all = dataframe[dataframe['HGMD_num'] == 1]
    rest = dataframe[dataframe['HGMD_num'] == 0]

    dataframe['HGMD_num_restrict'] = dataframe[name].replace(['DM'], 1)
    dataframe['HGMD_num_restrict'] = pd.to_numeric(dataframe['HGMD_num_restrict'], errors='coerce').fillna(value=0)
    patho_restricted = dataframe[dataframe['HGMD_num_restrict'] == 1]
    # Patho res are the variants with DM?, FP and DP
    patho_rest = dataframe[dataframe['HGMD_num'] == 1]
    patho_rest = patho_rest[patho_rest['HGMD_num_restrict'] == 0]

    if acmg_only == 0:
        patho = patho_restricted.append(patho_rest, sort=False)
    elif acmg_only == 1:
        patho = patho_restricted
        rest = patho_rest.append(rest, sort=False)
    return (patho, rest)


def clear_pathogenic(dataframe):
    # print("There are the follwoin Mutation Functions: ")
    # print(*np.unique(dataframe['Function'].dropna()), sep=', ')
    list_pathogenic = ['Nonsense', 'Frameshift', 'Splice']
    result = pd.DataFrame()
    for item in list_pathogenic:
        end = dataframe.loc[dataframe['Function'] == item]
        result = result.append(end)
    # Returns the rest dataframe, without the clearly pathogentic mutations
    for item in list_pathogenic:
        dataframe = dataframe.loc[dataframe['Function'] != item]
    return result, dataframe


def af_homo_cohort(dataframe, freq, ch_eng):
    # dataframe = frequent_in_cohort(dataframe, ch_eng)
    dataframe = allel_frequency(dataframe, freq)
    dataframe = homo(dataframe)
    dataframe = frequent_in_cohort(dataframe, ch_eng)
    return dataframe


def allel_frequency(dataframe, freq):
    AF_name = 'INFO_AF'
    # print_string(dataframe[AF_name])
    dataframe.loc[:, AF_name] = pd.to_numeric(
        dataframe[AF_name], errors='coerce')
    # fill NaN with 0
    allel_pd = dataframe.fillna(value={AF_name: 0})
    end = allel_pd.loc[allel_pd[AF_name] < freq]
    return end


def homo(dataframe):
    cutOff = 2 # including this CutOff
    AF_name = 'INFO_HOM'
    dataframe.loc[:, AF_name] = pd.to_numeric(
        dataframe[AF_name], errors='coerce')
    allel_pd = dataframe.fillna(value={AF_name: 0})
    end = allel_pd.loc[allel_pd[AF_name] <= cutOff]

    Hemi_name = 'INFO_HEMI'
    end.loc[:, Hemi_name] = pd.to_numeric(
        end[Hemi_name], errors='coerce')
    result = end.fillna(value={Hemi_name: 0})
    result = result.loc[result[Hemi_name] <= cutOff]
    return result


def frequent_in_cohort(dataframe, ch_eng):
    result = frequent_in_own_cohort(dataframe, ch_eng)
    result = frequent_in_other_cohort(result, ch_eng)
    return result


def frequent_in_own_cohort(dataframe, ch_eng):
    if ch_eng == 0:
        nat = 'CH'
        total_individual = 400
    elif ch_eng == 1:
        nat = 'ENG'
        total_individual = 1000
    CutOff_freq = int(float(total_individual)*0.01)
    counters = {}

    for element in dataframe['AA_or_SNP__Pos']:
        counters.setdefault(element, 0)
        counters[element] = counters[element] + 1

    counters_not_saved = {k: v for k, v in counters.items() if v > CutOff_freq}
    file_name = nat + '-frquent_variants.txt'
    with open(file_name, 'w') as file:
        file.write('\n'.join(counters_not_saved.keys()))
    # counters_not_saved = {k: v for k, v in counters.items() if v > CutOff_freq}
    # print('\n'.join(counters_filtered_print.keys()))

    frequent_variant_df = pd.DataFrame()
    for variant in list(counters_not_saved.keys()):
        temp = dataframe.loc[dataframe['AA_or_SNP__Pos'] == variant]
        frequent_variant_df = frequent_variant_df.append(temp, sort=False)
    # print(frequent_variant_df)
    # print(allel_frequency(frequent_variant_df, 0.004))
    # print('----')
    # exit(0)
    unfrequent_variant_df = dataframe.append(frequent_variant_df, sort=False)
    unfrequent_variant_df = unfrequent_variant_df.drop_duplicates(keep=False)
    return unfrequent_variant_df


def frequent_in_other_cohort(dataframe, ch_eng):
    # check if the OTHER file exists!!
    if ch_eng == 0:
        nat = 'ENG'
    elif ch_eng == 1:
        nat = 'CH'
    file_name = nat + '-frquent_variants.txt'
    if os.path.exists(file_name) == False:
        text_exception = 'Execute damaging filter with the ' + nat + ' parameter first!'
        raise Exception(text_exception)

    frequent_variant_df = pd.DataFrame()
    with open(file_name, 'r') as f:
        for line in f:
            temp = dataframe.loc[dataframe['AA_or_SNP__Pos'] == line]
            frequent_variant_df = frequent_variant_df.append(temp, sort=False)
    # print(frequent_variant_df)
    unfrequent_variant_df = dataframe.append(frequent_variant_df, sort=False)
    unfrequent_variant_df = unfrequent_variant_df.drop_duplicates(keep=False)
    return dataframe


def CADD(dataframe, cutOff):
    # Mute the warnings
    pd.options.mode.chained_assignment = None
    CADD_name = 'CADD_phred'
    CADD_pd = dataframe.dropna(subset=[CADD_name])
    CADD_numeric = CADD_pd[CADD_name].apply(pd.to_numeric)
    CADD_pd[CADD_name] = CADD_numeric
    end = CADD_pd.loc[CADD_pd[CADD_name] > cutOff]
    return end


def VEST3(dataframe):
    # ToDo
    return


def Polyphen2_HDIV(dataframe):
    # ToDo
    return


def Polyphen2_HVAR(dataframe):
    # D (Probably Damaging), P (Possibly Damaging), B (Benign)
    name = 'Polyphen2_HVAR_pred'
    res_column = dataframe[name]
    res_column = res_column.replace('D', 1)
    res_column = pd.to_numeric(res_column, errors='coerce').fillna(value=0)
    dataframe.loc[:, 'Polyphen2_HVAR_num'] = res_column
    return dataframe


def LRT(dataframe):
    # D (Deleterious), N (Neutral), U (Unkown)
    name = 'LRT_pred'
    res_column = dataframe[name]
    res_column = res_column.replace('D', 1)
    res_column = pd.to_numeric(res_column, errors='coerce').fillna(value=0)
    dataframe.loc[:, 'LRT_num'] = res_column
    return dataframe


def MutationTaster(dataframe):
    # A (Disease causing automatic), D (Disease causing)
    # N (Polymorphismus), P (Polymorphismus automatic)
    name = 'MutationTaster_pred'
    res_column = dataframe[name]
    res_column = res_column.replace('A', 1)
    res_column = res_column.replace('D', 1)
    res_column = pd.to_numeric(res_column, errors='coerce').fillna(value=0)
    dataframe.loc[:, 'MutationTaster_num'] = res_column
    return dataframe


def MutationAssessor(dataframe):
    # L (low functional), N (neutral functional)
    # H (high functional), M (medium functional)
    name = 'MutationAssessor_pred'
    res_column = dataframe[name]
    res_column = res_column.replace('H', 1)
    res_column = res_column.replace('M', 1)
    res_column = pd.to_numeric(res_column, errors='coerce').fillna(value=0)
    dataframe.loc[:, 'MutationAssessor_num'] = res_column
    return dataframe


def SIFT(dataframe):
    # Damaging (D), Tolerated (T)
    name = 'SIFT_pred'
    res_column = dataframe[name]
    res_column = res_column.replace('D', 1)
    res_column = pd.to_numeric(res_column, errors='coerce').fillna(value=0)
    dataframe.loc[:, 'SIFT_num'] = res_column
    return dataframe


def FATHMM(dataframe):
    # Damaging (D), Tolerated (T)
    name = 'FATHMM_pred'
    res_column = dataframe[name]
    res_column = res_column.replace('D', 1)
    res_column = pd.to_numeric(res_column, errors='coerce').fillna(value=0)
    dataframe.loc[:, 'FATHMM_num'] = res_column
    return dataframe


def M_CAP(dataframe):
    # Damaging (D), Tolerated (T)
    name = 'M-CAP_pred'
    res_column = dataframe[name]
    res_column = res_column.replace('D', 1)
    res_column = pd.to_numeric(res_column, errors='coerce').fillna(value=0)
    dataframe.loc[:, 'M_CAP_num'] = res_column
    return dataframe


def Provean(dataframe):
    # Damaging (D), Neutral (N)
    name = 'PROVEAN_pred'
    res_column = dataframe[name]
    res_column = res_column.replace('D', 1)
    res_column = pd.to_numeric(res_column, errors='coerce').fillna(value=0)
    dataframe.loc[:, 'Provean_num'] = res_column
    return dataframe


def Meta_SVM(dataframe):
    name = 'MetaSVM_pred'
    print(dataframe[name].unique())
    return


def at_least_n_pathogenic(dataframe, n):
    path_pred = 8
    dataframe_res = Polyphen2_HVAR(dataframe)
    dataframe_res = LRT(dataframe_res)
    dataframe_res = MutationTaster(dataframe_res)
    dataframe_res = MutationAssessor(dataframe_res)
    dataframe_res = SIFT(dataframe_res)
    dataframe_res = FATHMM(dataframe_res)
    dataframe_res = M_CAP(dataframe_res)
    dataframe_res = Provean(dataframe_res)

    dataframe_res['prediction_software_num'] = 0
    sum_column = dataframe_res.iloc[:, -(path_pred+1):-1].sum(axis=1)
    # make_hist_plot(sum_column)
    dataframe_res['prediction_software_num'] = sum_column
    dataframe_n_muations = dataframe_res.loc[
        dataframe_res['prediction_software_num'] >= n]
    return dataframe_n_muations


def at_least_n_pathogenic_no_MCAP(dataframe, n):
    if dataframe.shape[0] == 0:
        return (dataframe, dataframe)
    # Without the M-Cap
    path_pred = 7
    dataframe_res = Polyphen2_HVAR(dataframe)
    dataframe_res = LRT(dataframe_res)
    dataframe_res = MutationTaster(dataframe_res)
    dataframe_res = MutationAssessor(dataframe_res)
    dataframe_res = SIFT(dataframe_res)
    dataframe_res = FATHMM(dataframe_res)
    dataframe_res = Provean(dataframe_res)

    dataframe_res.loc[:, 'prediction_software_num'] = 0
    sum_column = dataframe_res.iloc[:, -(path_pred+1):-1].sum(axis=1)
    # make_hist_plot(sum_column)
    dataframe_res.loc[:, 'prediction_software_num'] = sum_column
    dataframe_n_muations = dataframe_res.loc[
        dataframe_res['prediction_software_num'] >= n]
    dataframe_rest = dataframe_res.loc[
        dataframe_res['prediction_software_num'] < n]
    return dataframe_n_muations, dataframe_rest


def MCAP_CADD_sep(dataframe, param):
    # dataframe already without the clearly pathogenic calls
    # dataframe already with AF > 0.4
    # 0.004, 100, 100: 4 prediction software (inkl > 20 CADD) and M-CAP
    # 0.004, 200, 200: 3 prediction software + CADD > 20 + M-CAP
    # 0,004, 300, 300: CADD>20 + M-CAP

    if param.cadd == 100 and param.n == 100:
        # give right value to cadd
        cadd = 20
        dataframe_MCAP = M_CAP(dataframe)
        result_MCAP = dataframe_MCAP.loc[dataframe_MCAP['M_CAP_num'] == 1]

        # either 4 prediction softwares
        (res_software_4, rest) = at_least_n_pathogenic_no_MCAP(result_MCAP, 4)
        # or 3 prediction softwares and CADD > 20
        (res_software_3, bla) = at_least_n_pathogenic_no_MCAP(rest, 3)
        result_cadd_3 = CADD(res_software_3, cadd)
        result_software_CADD = res_software_4.append(result_cadd_3)

    elif param.cadd == 200 and param.n == 200:
        # give right value to cadd
        cadd = 20
        dataframe_MCAP = M_CAP(dataframe)
        result_MCAP = dataframe_MCAP.loc[dataframe_MCAP['M_CAP_num'] == 1]
        (result_software, bla) = at_least_n_pathogenic_no_MCAP(result_MCAP, 3)
        result_software_CADD = CADD(result_software, cadd)
    elif param.cadd == 300 and param.n == 300:
        cadd = 20
        dataframe_MCAP = M_CAP(dataframe)
        result_MCAP = dataframe_MCAP.loc[dataframe_MCAP['M_CAP_num'] == 1]
        result_software_CADD = CADD(result_MCAP, cadd)
    elif param.cadd == 400 and param.n == 400:
        cadd = 20
        dataframe_MCAP = M_CAP(dataframe)
        result_MCAP = dataframe_MCAP.loc[dataframe_MCAP['M_CAP_num'] == 1]
        (result_software, bla) = at_least_n_pathogenic_no_MCAP(result_MCAP, 2)
        result_software_CADD = CADD(result_software, cadd)
    elif param.cadd == 500 and param.n == 500:
        cadd = 20
        dataframe_MCAP = M_CAP(dataframe)
        result_MCAP = dataframe_MCAP.loc[dataframe_MCAP['M_CAP_num'] == 1]
        (result_software, bla) = at_least_n_pathogenic_no_MCAP(result_MCAP, 4)
        result_software_CADD = CADD(result_software, cadd)
    elif param.cadd == 600 and param.n == 600:
        cadd = 20
        dataframe_MCAP = M_CAP(dataframe)
        result_MCAP = dataframe_MCAP.loc[dataframe_MCAP['M_CAP_num'] == 1]
        (result_software, bla) = at_least_n_pathogenic_no_MCAP(result_MCAP, 5)
        result_software_CADD = CADD(result_software, cadd)

    return result_software_CADD


def adapt_spreadsheet(dataframe):
    if dataframe.shape[0] == 0:
        return dataframe

    counters = {}
    for element in dataframe['AA_or_SNP__Pos']:
        counters.setdefault(element, 0)
        counters[element] = counters[element] + 1

    def freq_of_variant_yellow(element, dic):
        if dic[element] > 2:
            return 1
        return 0

    def freq_of_variant_green(element, dic):
        if dic[element] == 2:
            return 1
        return 0

    dataframe.loc[:, 'Yellow_Value'] = dataframe[
        'AA_or_SNP__Pos'].apply(
            freq_of_variant_yellow, dic=counters)
    dataframe.loc[:, 'Green_value'] = dataframe[
        'AA_or_SNP__Pos'].apply(
            freq_of_variant_green, dic=counters)
    return dataframe


if __name__ == '__main__':
    ch_eng = 0
    if ch_eng == 0:
        pickel_filename = 'true_calls.pkl'
    elif ch_eng == 1:
        pickel_filename = 'true_calls_eng.pkl'
    folder_path = '/home/selene/Programm/Diss'
    true_calls = pd.read_pickle(folder_path + '/' + pickel_filename)

    ClinVar_separation(true_calls)
    exit(0)

    test_ss = 'spreadsheet/new_tool/ciliopathy_damaging_variants_0.004_20_4.csv'
    df_before = pd.read_csv(test_ss, sep='\t')
    df = adapt_spreadsheet(df_before)


