import pandas as pd
import numpy as np
from class_def import Parameter
from variant_filter import damaging_variant
from damaging_filter import (adapt_spreadsheet, HGMD_separation)
from scipy.stats import linregress
import matplotlib.pyplot as plt
from sklearn.linear_model import LinearRegression
import seaborn as sns
import split_by_disorder as split


def seperate_damaging_rest(dataframe, param, seperator):
    print(dataframe.shape)
    dataframe = dataframe.reset_index()
    if seperator == 'Our_Modell':
        param.acmg_only = 0
        damaging = damaging_variant(dataframe, param)
    elif seperator == 'HGMD':
        (damaging, bla) = HGMD_separation(dataframe)
    elif seperator == 'ACMG':
        param.acmg_only = 1
        damaging = damaging_variant(dataframe, param)
    damaging['predic'] = 1


    dataframe = dataframe.drop(list(damaging.index.values))
    dataframe['predic'] = 0

    result = damaging.append(dataframe)

    result = adapt_spreadsheet(result)
    return result


def plot_correlataion(dataframe):
    CADD_name = 'predic'
    MCAP_name = 'ClinVar_italic'

    x = dataframe[CADD_name].fillna(0)
    y = dataframe[MCAP_name].fillna(0)
    slope, intercept, r_value, p_value, std_err = linregress(x, y)

    x = x.values.reshape(-1, 1)
    y = y.values.reshape(-1, 1)

    linear_regressor = LinearRegression()  # create object for the class
    linear_regressor.fit(x, y)  # perform linear regression
    y_pred = linear_regressor.predict(x)  # make predictions

    fig, ax = plt.subplots()
    ax.plot(x, y_pred, color='red')
    ax.scatter(x, y)
    # sns.relplot(x, y, size=(15,200))

    text = ('Correlation Coefficient = %.2f \n'
            'p-value = %.4E' % (r_value, p_value))
    props = dict(boxstyle='round', facecolor='white', alpha=0.5)
    ax.text(0.05, 0.95, text, transform=ax.transAxes, fontsize=14,
            verticalalignment='top', bbox=props)

    ax.tick_params(labelsize=14)
    ax.set_xlabel('Damaging Variant', fontsize=14)
    ax.set_ylabel('ClinVar Pathogenic', fontsize=14)

    path = '/home/selene/Programm/Diss/' + 'correlation_with_ClinVar.png'
    plt.savefig(path, bbox_inches='tight')

    plt.clf()
    plt.close()
    return


def crosstab(dataframe):
    # we predicted benign
    predict_0 = dataframe.loc[dataframe['predic'] == 0]
    ClinVar_10 = predict_0.loc[predict_0['ClinVar_italic'] == 1].shape[0]
    ClinVar_ben_0 = predict_0.loc[predict_0['ClinVar_red'] == 1].shape[0]
    ClinVar_uncertain_0 = predict_0.loc[predict_0['ClinVar_uncertain'] == 1].shape[0]
    ClinVar_conf_0 = predict_0.loc[predict_0['ClinVar_conflict'] == 1].shape[0]
    rest0 = predict_0.shape[0] - ClinVar_10 - ClinVar_ben_0 - ClinVar_uncertain_0 - ClinVar_conf_0

    # we predicted pathogenic
    predict_1 = dataframe.loc[dataframe['predic'] == 1]
    ClinVar_11 = predict_1.loc[predict_1['ClinVar_italic'] == 1].shape[0]
    ClinVar_ben_1 = predict_1.loc[predict_1['ClinVar_red'] == 1].shape[0]
    ClinVar_uncertain_1 = predict_1.loc[predict_1['ClinVar_uncertain'] == 1].shape[0]
    ClinVar_conf_1 = predict_1.loc[predict_1['ClinVar_conflict'] == 1].shape[0]
    rest1 = predict_1.shape[0] - ClinVar_11 - ClinVar_ben_1 - ClinVar_uncertain_1 - ClinVar_conf_1

    print(ClinVar_11 + ClinVar_ben_1+ ClinVar_uncertain_1+ ClinVar_conf_1+ rest1)
    print(ClinVar_10 + ClinVar_ben_0+ ClinVar_uncertain_0+ ClinVar_conf_0+ rest0)

    print('\t ClinVar_path\tClinVar_ben \tuncertain \tconflicted \trest \ttotal')
    print('damag\t%d \t\t%d \t\t%d \t\t%d \t\t%d \t\t%d' %(ClinVar_11, ClinVar_ben_1, ClinVar_uncertain_1, ClinVar_conf_1, rest1, predict_1.shape[0]))
    print('benign\t%d \t\t%d \t\t%d \t\t%d \t\t%d \t\t%d' %(ClinVar_10, ClinVar_ben_0, ClinVar_uncertain_0, ClinVar_conf_0, rest0, predict_0.shape[0]))
    print('total\t%d \t\t%d \t\t%d \t\t%d \t\t%d' % (ClinVar_11 + ClinVar_10, ClinVar_ben_1+ClinVar_ben_0, ClinVar_uncertain_1 + ClinVar_uncertain_0, ClinVar_conf_1+ClinVar_conf_0, rest1+rest0))
    
    return


if __name__=='__main__':
    ch_eng = 0
    if ch_eng == 0:
        pickel_filename = 'true_calls.pkl'
    elif ch_eng == 1:
        pickel_filename = 'true_calls_eng.pkl'
    folder_path = '/home/selene/Programm/Diss'
    true_calls = pd.read_pickle(folder_path + '/' + pickel_filename)

    param = Parameter(0.004, 500, 500, ch_eng, dis=0)


    list_separator = ['Our_Modell', 'HGMD', 'ACMG']
    for sep in list_separator:
        print('======================================================================================================')
        print(sep)
        df = seperate_damaging_rest(true_calls, param, sep)
        crosstab(df)
        # plot_correlataion(df)
