import pandas as pd
import numpy as np
from class_def import Parameter
from variant_filter import damaging_variant
import split_by_disorder as split



def txt_for_hgmd_ch(dataframe, param, all_damaging):
    if param.ch_eng == 1:
        print('Only for Swiss data')
        return
    result = dataframe['hg19/hg38_coordinate']
    result = result.rename('hg19/hg38 coordinate')


    path = '/home/selene/Programm/Diss/'
    if all_damaging == 1:
        end = '_' + str(param.f) + '_' + str(param.cadd) + '_' + str(param.n)
        if param.dis == 0:
            dis = 'ciliopathy'
        else:
            dis = str(param.dis)
    elif all_damaging == 0:
        end = ''
        dis = 'trueCalls'

    path_txt = 'Swiss_' + dis + end
    path_txt = path + path_txt + '_HGMD_batch.txt'
    result.to_csv(path_txt, sep='\n', index=False, header=True)
    return


def txt_for_hgmd_eng(dataframe, param):
    if param.ch_eng == 0:
        print('Only for English data')
        return

    swiss_chr_pos_series = pd.read_csv('Swiss_trueCalls_HGMD_batch.txt', sep='; ', header=0)

    # find the ones we already know from Swiss data
    def matches(elemtent, list_to_match):
        if elemtent in list_to_match:
            return 1
        return 0

    dataframe['in_ch_data'] = dataframe['hg19/hg38_coordinate'].apply(matches, list_to_match=swiss_chr_pos_series.values[:,0])
    only_in_eng_df = dataframe[dataframe['in_ch_data'] == 0]
    in_eng_and_ch = dataframe[dataframe['in_ch_data'] == 1]

    # find the Series with all the SNP numbers
    df_with_rs = only_in_eng_df.dropna(subset=['SNP_db_xref'])
    result_rs = df_with_rs['SNP_db_xref'].drop_duplicates()

    # rest without the SNP number
    df_no_rs = only_in_eng_df.append(df_with_rs).drop_duplicates(keep=False)
    result_no_rs = pd.DataFrame({'hg19/hg38 coordinate' : df_no_rs['hg19/hg38_coordinate'].drop_duplicates()})
    result_eng_swiss_same = pd.DataFrame({'hg19/hg38 coordinate' : in_eng_and_ch['hg19/hg38_coordinate'].drop_duplicates()})

    # Save the two files
    path = '/home/selene/Programm/Diss/'
    path_txt = 'Eng_trueCalls'
    path_no_rs = path + path_txt + '_HGMD_batch.txt'
    path_rs = path + path_txt + '_HGMD_batch_rs.txt'
    path_eng_ch_same = path + path_txt + '_HGMD_batch_eng_swiss_same.txt'

    result_no_rs.to_csv(path_no_rs, sep='\n', index=False, header=True)
    result_rs.to_csv(path_rs, sep='\n', index=False, header=True)
    result_eng_swiss_same.to_csv(path_eng_ch_same, sep='\n', index=False, header=True)
    return


if __name__=='__main__':
    ch_eng = 1
    if ch_eng == 0:
        pickel_filename = 'true_calls.pkl'
    elif ch_eng == 1:
        pickel_filename = 'true_calls_eng.pkl'
    folder_path = '/home/selene/Programm/Diss'
    true_calls = pd.read_pickle(folder_path + '/' + pickel_filename)

    param = Parameter(0.004, 200, 200, ch_eng, 0)
    damaging = damaging_variant(true_calls, param)

    # batch for all true calls
    txt_for_hgmd_eng(true_calls, param)
    txt_for_hgmd_ch(true_calls, param, 0)

    # batch for the 200,200 case
    txt_for_hgmd_ch(damaging, param, 1)
    
    # batch for Joubert 200,200
    param = Parameter(0.004, 200, 200, ch_eng, 'Joubert')
    (disorder, gene_disorder_pd) = split.get_disorder_list()
    affected_gene = split.affected_gene_per_disorder(
                            gene_disorder_pd, param.dis)
    true_calls_dis = split.df_for_disorder(true_calls, affected_gene)
    damaging = damaging_variant(true_calls_dis, param)
    txt_for_hgmd_ch(damaging, param, 1)