import enum
from manual_cleaning import del_all
import numpy as np
from plot_svm import plot_svm, plot_all_data
import pandas as pd
from sklearn import svm
from preprocess_true_calls import plot_num_of_variant
# from sys import exit


cutOff = 0.8  # how to split the test and train data

class PreprocessingOption(enum.Enum):
    unlabeled_data = 0
    labeled_data = 1

# ##################################
# Test
def check_column(df1, df2):
    res1 = df1.columns.difference(df2.columns)
    res2 = df2.columns.difference(df1.columns)
    res = res1.shape[0] + res2.shape[0]
    if res:
        print(res1)
        print(res2)
    return not res


def check_for_string(dataframe):
    orig = dataframe
    for column in dataframe:
        c1_as_float = pd.to_numeric(orig[column], errors='coerce')
        print(orig[column][c1_as_float.isna()])
# ##################################

def get_train_test_data(np_dataform, cutOff):
    # np_dataform has no NaN and no strings
    cutOffInt = int(np_dataform.shape[0]*cutOff)
    train = np_dataform[0:cutOffInt, :]
    test = np_dataform[cutOffInt:np_dataform.shape[0], :]
    return (train, test)


def get_wrong_labled_points(dataframe, prediction):
    dataframe['pred'] = prediction
    res = dataframe[dataframe['pred'] != dataframe['Truth']]
    if res.shape[0] == 0:
        print('No wrong labled points')
        return
    print(res[['patient_id', 'Truth', 'pred', 'Index', 'Chrom', 'Coverage', 'Alt%']])
    return


def get_sens_spez(y_pred, y):
    d = {'Truth': y, 'predicted_true': y_pred}
    df_with_predic = pd.DataFrame(data = d)

    def predicted_vs_truth(dataframe, pred, truth):
        dataframe = dataframe[dataframe['predicted_true'] == pred]
        dataframe = dataframe[dataframe['Truth'] == truth]
        return dataframe.shape[0]
    
    true_positive = predicted_vs_truth(df_with_predic, 1, 1)
    true_negative = predicted_vs_truth(df_with_predic, 0, 0)
    false_positive = predicted_vs_truth(df_with_predic, 1, 0)
    false_negative = predicted_vs_truth(df_with_predic, 0, 1)

    sens = float(true_positive/(true_positive + false_negative))
    spez = float(true_negative/(true_negative + false_positive))
    spez_1 = float(1-spez)
    print('sens %f' % sens)
    print('1-spez %f' % spez_1)

    return sens, spez


def transform_to_np_data(data_pd, is_learning):
    if is_learning == PreprocessingOption.labeled_data:
        columns = ['Truth', 'Coverage', 'Alt%']
        power_vec = [1, -1, -1]
    elif is_learning == PreprocessingOption.unlabeled_data:
        columns = ['Coverage', 'Alt%']
        power_vec = [-1, -1]
    data_np = data_pd[columns].values
    data_np = np.power(data_np, power_vec)
    return data_np


def return_x_y(data_np):
    return data_np[:, 1:], data_np[:, 0]


def train_model(x_train, y_train):
    # data format:  first train then test data point
    #               the first column is the truth
    #               data has no NaN and no strings
    #               Pandas DataFrame
    c = 100000
    clf = svm.LinearSVC(C=c, class_weight='balanced', max_iter=10000000)
    clf.fit(x_train, y_train)

    return clf


def evaulate_model(clf, x_values, y_values, name):
    y_predict= clf.predict(x_values)
    print(name)
    get_sens_spez(y_pred=y_predict, y=y_values)
    # less important part
    print("Percentage of true Calls %.4f" % float(sum(y_predict)/y_predict.shape[0]))
    diff1 = y_values - y_predict
    diff_abs1 = np.absolute(diff1)
    diff_sum1 = np.sum(diff_abs1)
    false_call_truth = y_values.shape[0]-np.sum(y_values)
    false_calls_pred = y_predict.shape[0]-np.sum(y_predict)
    print("There are %d predicted instead of %d false calls in %s" % (false_calls_pred, false_call_truth, name))
    print("Fales prediction in %s set = %d" % (name, diff_sum1))
    return


def preprocess_data_frame(dataframe, is_learning_data, ch_eng):
    dataframe = del_all(dataframe)

    def keep_Alt_Coverage(dataframe, is_learning):
        dataframe.loc[:, 'Alt%'] = pd.to_numeric(dataframe['Alt%'], errors='coerce').astype(float)
        dataframe.loc[:, 'Coverage'] = pd.to_numeric(dataframe['Coverage'], errors='coerce').astype(float)
        dataframe.loc[:, 'patient_id'] = pd.to_numeric(dataframe['patient_id'], errors='coerce').astype(float)
        result = dataframe.dropna(subset=['patient_id', 'Coverage', 'Alt%'])
        if is_learning == PreprocessingOption.labeled_data:
            result.loc[:, 'Truth'] = pd.to_numeric(result['Truth'])
        print('Passing pre process: %i'  % result.shape[0])
        return result

    if ch_eng == 1:
        return keep_Alt_Coverage(dataframe, is_learning_data)

    # delete data as disscussed with Pascale
    if is_learning_data == PreprocessingOption.labeled_data:
        dataframe_res = dataframe.dropna(axis=1)
        dataframe_end = dataframe_res[dataframe_res.apply(
                            pd.to_numeric, errors='coerce').notna()]
        dataframe_end = dataframe_end.dropna(axis=1)
    elif is_learning_data == PreprocessingOption.unlabeled_data:
        # delete the 5 rows which have been deleted before
        # without loosing any columns!
        list_column_to_drop = [column for column in dataframe
                               if not int(dataframe[column].isna().sum()) > 5]
        dataframe_res = dataframe.dropna(subset=list_column_to_drop)
        # delete the row with the string in the learning columns
        dataframe_end = dataframe_res[
            ~dataframe_res.Allele_Score.str.contains('24.37,24.05', na=False)]
    dataframe_end.loc[:, 'Alt%'] = pd.to_numeric(dataframe_end['Alt%'])
    dataframe_end.loc[:, 'Allele_Score'] = pd.to_numeric(dataframe_end['Allele_Score'])

    print('Passing pre process: %i'  % dataframe_end.shape[0])
    return dataframe_end


def open_file_adapt_column_name(file_name, delimiter):
    old_name = 'patient_id'
    if file_name == 'validation_data.csv':
        old_name = 'Pat'
    dataframe = pd.read_csv(file_name, sep=delimiter)
    dataframe.columns = (dataframe.columns.str.strip()
                                 .str.replace(old_name, 'patient_id')
                                 .str.replace(' ', '_')
                                 .str.replace(':', ''))
    return dataframe


def main_function(filename_traintest, filename_alldata, filename_validation, sep, ch_eng):
    nat = 'England'
    if ch_eng == 0:
        nat = 'Swiss'
    print('---------' + nat + '---------')
    # Get the dataframe for training, testing, validation
    train_test_pd = open_file_adapt_column_name(file_name=filename_traintest, delimiter=sep)
    validation_pd = open_file_adapt_column_name(file_name=filename_validation, delimiter='\t')

    # Get dataframe for all the data
    folder_path = '/home/selene/Programm/Diss'
    all_data_pd = pd.read_pickle(folder_path + '/' + filename_alldata)
    print('All data size: %i' % all_data_pd.shape[0])

    trainingdata_post_preprocess = preprocess_data_frame(dataframe=train_test_pd, is_learning_data=PreprocessingOption.labeled_data, ch_eng=ch_eng)
    all_post_preprocess = preprocess_data_frame(dataframe=all_data_pd, is_learning_data=PreprocessingOption.unlabeled_data, ch_eng=ch_eng)
    validation_post_preprocess = preprocess_data_frame(dataframe=validation_pd, is_learning_data=PreprocessingOption.labeled_data, ch_eng=ch_eng)

    trainingdata_np = transform_to_np_data(data_pd=trainingdata_post_preprocess, is_learning=PreprocessingOption.labeled_data)
    (train, test) = get_train_test_data(trainingdata_np, cutOff)
    (x_train, y_train) = return_x_y(train)
    (x_test, y_test) = return_x_y(test)

    validation_np = transform_to_np_data(data_pd=validation_post_preprocess, is_learning=PreprocessingOption.labeled_data)
    (x_val, y_val) = return_x_y(validation_np)


    clf = train_model(x_train=x_train, y_train=y_train)
    C = 100000
    evaulate_model(clf=clf, x_values=x_train, y_values=y_train, name='Train')
    evaulate_model(clf=clf, x_values=x_test, y_values=y_test, name='Test')
    evaulate_model(clf=clf, x_values=x_val, y_values=y_val, name='Validation')
    evaulate_model(clf=clf, x_values=np.append(x_test, x_val, axis=0), y_values=np.append(y_test, y_val), name='Validation + Test')

    plot_svm(x_train, y_train, clf, inv_const=1, data_type='train', c=C, ch_eng=ch_eng)
    plot_svm(x_test, y_test, clf, inv_const=1, data_type='test', c=C, ch_eng=ch_eng)
    plot_svm(x_val, y_val, clf, inv_const=1, data_type='validation', c=C, ch_eng=ch_eng)

    get_wrong_labled_points(trainingdata_post_preprocess, np.append(clf.predict(x_train), clf.predict(x_test)))
    get_wrong_labled_points(validation_post_preprocess, clf.predict(x_val))

    # Get prediction for all data and add it to a new column
    all_np = transform_to_np_data(data_pd=all_post_preprocess, is_learning=PreprocessingOption.unlabeled_data)
    y_data_all = clf.predict(all_np)
    end_data = all_post_preprocess.assign(prediction=y_data_all)
    plot_all_data(all_np, y_data_all, clf, inv_const=1, data_type='all_data', c=C, ch_eng=ch_eng)

    print("Percentage of true Calls %.4f" % float(sum(y_data_all)/y_data_all.shape[0]))

    return end_data


def save_variants(dataframe, filename_true_call, filename_false_call):
    path = '/home/selene/Programm/Diss/'
    true_calls = dataframe.loc[dataframe['prediction'] == 1]
    false_calls = dataframe.loc[dataframe['prediction'] == 0]
    print('False Calls = %i' % false_calls.shape[0])
    print('True Calls = %i' % true_calls.shape[0])
    save_path = path + filename_true_call
    # true_calls.to_pickle(save_path)
    save_path_false = path + 'spreadsheet_excluded' + filename_false_call
    # false_calls.to_csv(save_path_false, sep='\t')
    return


def merge_two_data_set(name1, name2):
    df1 = pd.read_csv(name1, sep='\t')
    df2 = pd.read_csv(name2, sep='\t')
    df_result = df1.append(df2)
    df_result.to_csv('alldata_eng_1_2.csv', sep='\t')
    return 


if __name__ == "__main__":
    merge_two_data_set('alldata_eng.csv', 'alldata_eng_2.csv')
    # merge_two_data_set('alldata_eng_adapted.csv', 'alldata_eng_2.csv')
    name_traintest = ['alldata.csv', 'alldata_eng_1_2.csv']
    name_sep = [',', '\t']
    name_alldata = ['real_data_2.pkl', 'real_data_2_eng.pkl']
    name_validation = ['validation_data.csv', 'val_eng.csv']
    name_true_call = ['pred_true_calls.pkl','pred_true_calls_eng.pkl']
    name_false_call = ['pred_false_calls.csv', 'pred_false_calls_eng.csv']


    for i in range(2):
        ch_eng = i
        final_matrix = main_function(filename_traintest=name_traintest[i], filename_alldata=name_alldata[i], filename_validation=name_validation[i] ,sep=name_sep[i], ch_eng=ch_eng)
        save_variants(dataframe=final_matrix, filename_true_call=name_true_call[i], filename_false_call=name_false_call[i])

