import math
import matplotlib.pyplot as plt
from matplotlib.pyplot import figure
from matplotlib import rc
import numpy as np
import pandas as pd
import seaborn as sns
from split_by_disorder import order_gene
# from sys import exit

# path = '/home/selene/Programm/Diss/plot/'


def full_analysis(dataframe_patho, dataframe_full, param):
    print("f=%.3f. CADD=%i. n=%i" % (param.f, param.cadd, param.n))
    # ex_1_mut_per_gene(dataframe_patho, param)
    ex_2_mut_per_patient_plot(dataframe_patho, param)
    # vairiants_per_patient(dataframe_patho, dataframe_full, param)
    # genes_per_patient(dataframe_patho, dataframe_full, param)
    # m_variants_in_same_gene_per_patient(
    #       dataframe_patho, dataframe_full, param)
    # distribution_per_gene(dataframe_patho, param)
    # distribution_per_gene_percent(dataframe_patho, dataframe_full, param)
    # type_of_function(dataframe_patho, param)
    # type_of_mutation(dataframe_patho, param)
    # typ_of_function_per_gene(dataframe_patho, param)
    # typ_of_function_per_gene_unique(dataframe_patho, param)
    # exist_more_m_diff_variants(dataframe_patho, param)
    # overall_score(dataframe_patho, param)
    # heterozygot_trait(dataframe_patho, param)
    # genes_mut_per_pat(dataframe_patho, dataframe_full, param)
    find_BBS1(dataframe_patho)
    return


def full_analysis_per_disease(
        dataframe_patho, dataframe_full, param):
    print("f=%.3f. CADD=%i. n=%i" % (param.f, param.cadd, param.n))
    # ex_1_mut_per_gene(dataframe_patho, param)
    ex_2_mut_per_patient_plot(dataframe_patho, param)
    # vairiants_per_patient(dataframe_patho, dataframe_full, param)
    # m_variants_in_same_gene_per_patient(
    #     dataframe_patho, dataframe_full, param)
    # distribution_per_gene(dataframe_patho, param)
    # distribution_per_gene_percent(dataframe_patho, dataframe_full, param)
    # type_of_function(dataframe_patho, param)
    # type_of_mutation(dataframe_patho, param)
    # typ_of_function_per_gene(dataframe_patho, param)
    # typ_of_function_per_gene_unique(dataframe_patho, param)
    # heterozygot_trait(dataframe_patho, param)
    # genes_mut_per_pat(dataframe_patho, dataframe_full, param)
    # find_BBS1(dataframe_patho)
    return



def ex_1_mut_per_gene(damaging, param):
    if damaging.shape[0] == 0:
        return
    dis = param.dis

    # adapt figure size
    rc('font')
    plt.rcParams.update({'font.size': 18})
    plt.xticks(fontsize=18)
    plt.yticks(fontsize=18)

    patient_list = np.unique(damaging['patient_id'])
    ex_1_var = []
    for pat in patient_list:
        num_of_single_var = 0
        variants_of_pat = damaging.loc[damaging['patient_id'] == pat]
        gene_list = variants_of_pat['Gene'].unique()
        for gene in gene_list:
            variants_gene = variants_of_pat.loc[
                variants_of_pat['Gene'] == gene]
            if variants_gene.shape[0] == 1:
                num_of_single_var = num_of_single_var + 1
        ex_1_var.append(num_of_single_var)

    plt.xlabel('Number of genes with exaclty one variant')
    plt.ylabel('Patients')

    name = 'exactly_1_variant_per_gene'

    make_hist_plot(ex_1_var, name, param)
    return

def ex_2_mut_per_patient_plot(damaging, param):
    if damaging.shape[0] == 0:
        return
    dis = param.dis

    # adapt figure size
    rc('font')
    # plt.rcParams.update({'font.size': 18})
    # plt.xticks(fontsize=18)
    # plt.yticks(fontsize=18)
    if dis == 0:
        plt.figure(figsize=(20,20))
    elif dis != 0:
        plt.figure(figsize=(5,5))
    sns.set(font_scale=1)

    patient_list = np.unique(damaging['patient_id'])
    ex_2_var_pd = pd.DataFrame()
    for pat in patient_list:
        variants_of_pat = damaging.loc[damaging['patient_id'] == pat]
        if variants_of_pat.shape[0] == 2:
            ex_2_var_pd = ex_2_var_pd.append(variants_of_pat)

    # order the genes in the right order
    def check_if_in_damaging(element, compare):
        if compare[compare[0] == element].shape[0] == 0:
            return 0
        else:
            return 1
    columnt_name_damaging = ex_2_var_pd['Gene'].unique()

    missing_genes = sorted(set(columnt_name_damaging) - set(order_gene()['gene_used'].unique()))

    columnt_name_df = pd.DataFrame(columnt_name_damaging)
    order_gene_df = order_gene()
    order_gene_df['in_damaging'] = order_gene_df['gene_used'].apply(check_if_in_damaging, compare=columnt_name_df)
    damaging_genes_ordered = order_gene_df[order_gene_df['in_damaging'] > 0]
    columnt_name = damaging_genes_ordered['gene_used'].unique()
    if len(missing_genes) > 0:
        print(missing_genes)
        columnt_name = np.append(columnt_name, missing_genes)
    print(columnt_name)

    result = pd.DataFrame(0, index=columnt_name, columns=columnt_name)
    patient_list = ex_2_var_pd['patient_id'].unique()
    print(len(patient_list))
    for patient in patient_list:
        df_of_one_patient = ex_2_var_pd.loc[ex_2_var_pd['patient_id'] == patient]
        gene_list = list(df_of_one_patient['Gene'].sort_values())
        result.at[gene_list[0], gene_list[1]] += 1
        if gene_list[0] != gene_list[1]:
            result.at[gene_list[1], gene_list[0]] += 1

    mask = np.full_like(result, 0)
    mask[np.triu_indices_from(mask, k=1)] = True
    with sns.axes_style("white"):
        ax = sns.heatmap(result, mask=mask, square=True, yticklabels=True, xticklabels=True, linewidths=.5, cmap='cool')

    if param.dis != 0:
        title = param.dis + ' - '
    elif param.dis == 0:
        title = 'Ciliopathy - '
    if param.ch_eng == 0:
        title = title + 'Swiss Data'
    elif param.ch_eng == 1:
        title = title + 'English Data'
    if param.acmg_only == 1:
        title = title + ' ACMG'

    path_plot = param.return_path_plot('ex_2_mut_per_patient')
    plt.xticks(rotation=90)
    plt.title(title)
    plt.savefig(path_plot, bbox_inches='tight')

    plt.close()
    plt.clf()
    return



def vairiants_per_patient(dataframe_patho, dataframe_full, param):
    if dataframe_patho.shape[0] == 0:
        return
    rc('font')
    plt.rcParams.update({'font.size': 15})
    plt.xticks(fontsize=15)
    plt.yticks(fontsize=15)
    number_of_variants = []
    patient_id = np.unique(dataframe_full['patient_id'])
    for i in patient_id:
        res = dataframe_patho.loc[
            dataframe_patho['patient_id'].astype(int) == i]
        res_s = res.shape[0]
        number_of_variants.append(res_s)
    plt.ylabel('Number of patients')
    plt.xlabel('Number of variants')


    xint = range(min(number_of_variants),
                 math.ceil(max(number_of_variants)) + 1)
    plt.xticks(xint)
    make_hist_plot(
        number_of_variants, 'variants_per_patient', param)
    return


def genes_per_patient(dataframe_patho, dataframe_full, param):
    if dataframe_patho.shape[0] == 0:
        return
    rc('font')
    plt.rcParams.update({'font.size': 15})
    plt.xticks(fontsize=15)
    plt.yticks(fontsize=15)
    number_of_genes = []
    patient_id = np.unique(dataframe_full['patient_id'])
    for i in patient_id:
        res = dataframe_patho.loc[
            dataframe_patho['patient_id'].astype(int) == i]
        res_s = len(res['Gene'].unique())
        number_of_genes.append(res_s)
    plt.ylabel('Number of patients')
    plt.xlabel('Number of damaging genes')


    xint = range(min(number_of_genes),
                 math.ceil(max(number_of_genes)) + 1)
    plt.xticks(xint)
    make_hist_plot(
        number_of_genes, 'genes_per_patient', param)
    return


def m_variants_in_same_gene_per_patient(dataframe_patho,
                                        dataframe_full, param):
    if dataframe_patho.shape[0] == 0:
        return
    rc('font')
    plt.rcParams.update({'font.size': 18})
    plt.xticks(fontsize=18)
    plt.yticks(fontsize=18)
    m = 2
    number_of_variants = []
    patient_id = np.unique(dataframe_full['patient_id'])
    more_than_2_variants = pd.DataFrame()
    for i in patient_id:
        res = dataframe_patho.loc[dataframe_patho['patient_id'] == i]
        count_per_patient = 0
        if res.shape[0] >= 2:
            genes = res['Gene'].unique()
            for gene in genes:
                same_gene_mutation = res.loc[res['Gene'] == gene]
                if same_gene_mutation.shape[0] >= m:
                    count_per_patient = count_per_patient + 1
                    if same_gene_mutation.shape[0] >= 3:
                        more_than_2_variants = more_than_2_variants.append(
                            same_gene_mutation)
        number_of_variants.append(count_per_patient)
    if more_than_2_variants.shape[0] > 0:
        print(more_than_2_variants)
    plt.ylabel('Number of patients')
    plt.xlabel('Numbers of times with at least'
               ' 2 variants in same gene in the same patient')

    xint = range(min(number_of_variants),
                 math.ceil(max(number_of_variants)) + 1)
    plt.xticks(xint)
    print(sum(number_of_variants))
    make_hist_plot(number_of_variants,
                   'm_variants_in_same_gene_per_patient', param)
    return


def distribution_per_gene(dataframe_patho, param):
    if dataframe_patho.shape[0] == 0:
        return
    figure(num=None, figsize=(100, 30))
    plt.rcParams.update({'font.size': 70})
    plt.ylabel('Number of variants')
    # plt.xticks(fontsize='7')
    plt.xticks(fontsize=70, rotation=90)
    make_hist_plot_rot(dataframe_patho['Gene'],
                       'distribution_per_gene', param)
    return


def distribution_per_gene_percent(
        dataframe_patho, dataframe_full, param):
    if dataframe_patho.shape[0] == 0:
        return

    if param.dis != 0:
        title = param.dis + ' - '
    elif param.dis == 0:
        title = 'Ciliopathy - '
    if param.ch_eng == 0:
        title = title + 'Swiss Data'
    elif param.ch_eng == 1:
        title = title + 'English Data'
    if param.acmg_only == 1:
        title = title + ' ACMG'

    figure(num=None, figsize=(100, 30))
    plt.rcParams.update({'font.size': 70})
    number_of_all_patient = len(dataframe_full['patient_id'].unique())
    df_row = dataframe_patho['Gene']
    hist = {}
    for i in df_row:
        hist[i] = hist.get(i, 0) + 100/number_of_all_patient
    plt.bar(x=list(hist.keys()), height=list(hist.values()))
    plt.xticks(rotation=90)
    plt.title(title)
    plt.ylabel('Number of variant in percentage of all patients [%]')

    path_plot = param.return_path_plot('distribution_per_gene_percent')
    plt.savefig(path_plot, bbox_inches='tight')
    # plt.show()
    plt.clf()
    plt.close()
    return


def type_of_function(dataframe_patho, param):
    if dataframe_patho.shape[0] == 0:
        return
    rc('font')
    plt.rcParams.update({'font.size': 15})
    plt.xticks(fontsize=15, rotation=90)
    plt.yticks(fontsize=15)
    plt.ylabel('Number of variants')
    make_hist_plot_rot(dataframe_patho['Function'],
                       'type_of_function', param)
    return


def type_of_mutation(dataframe_patho, param):
    if dataframe_patho.shape[0] == 0:
        return
    title = 'Mutation_Call_HGVS_Coding'
    plt.xticks(rotation=90)
    make_hist_plot_rot(dataframe_patho[title],
                       'type_of_mutation', param)
    # mutations = dataframe_patho[title].unique()
    # for mut in mutations:
    #     all_mut = dataframe_patho.loc[dataframe_patho[title] == mut]
    #     if all_mut.shape[0] >= 3:
    #         print(mut)
    return


def typ_of_function_per_gene_unique(dataframe_patho, param):
    if dataframe_patho.shape[0] == 0:
        return
    dataframe = dataframe_patho.drop_duplicates(subset='AA_or_SNP__Pos', keep="first")
    typ_of_function_per_gene(dataframe, param, 1)
    return


def typ_of_function_per_gene(dataframe_patho, param, unique_var = 0, gene_list2 = []):
    if dataframe_patho.shape[0] == 0:
        return
    dis = param.dis
    if any(gene_list2) == False:
        genes = dataframe_patho['Gene'].sort_values().unique()
    else:
        genes1 = dataframe_patho['Gene'].sort_values().unique()
        genes2 = gene_list2

        difference = list(set(genes2) - set(genes1))
        if any(difference) == False:
            genes = genes1
        else:
            genes = list(set(genes1)) + difference
            genes = sorted(genes)
    if dis == 0:
        length = 100
        fontsize = 70
    elif dis != 0:
        length = len(genes)*2
        fontsize = 90
    figure(num=None, figsize=(length, 30))
    plt.rcParams.update({'font.size': fontsize})
    # y-axis in bold
    function_gene = ['Missense', 'Synonymous', 'Noncoding',
                     'Nonsense', 'Frameshift', 'Splice',
                     'In-Frame', 'No-stop', 'Unknown']

    rc('font')

    # def return_size(element):
    #     return element.shape[0]

    # def return_type_of_variants(element):
    #     res = element.groupby('Function').apply(return_size)
    #     return res

    # tmp = dataframe_patho.groupby('Gene').apply(return_type_of_variants)
    # print(tmp)
    # df = pd.DataFrame(tmp)
    # pivot_df = df.pivot(index='Gene', columns='Function', values='0')
    # print(pivot_df)
    # exit()
    # Values of each group
    bars_missense = []
    bars_synomymus = []
    bars_noncoding = []
    bars_nonsense = []
    bars_frameshift = []
    bars_splice = []
    bars_inframe = []
    bars_nostop = []
    bars_unknwon = []
    for gene in genes:
        res = dataframe_patho.loc[dataframe_patho['Gene'] == gene]
        miss = res.loc[res['Function'] == 'Missense']
        bars_missense.append(miss.shape[0])
        syn = res.loc[res['Function'] == 'Synonymous']
        bars_synomymus.append(syn.shape[0])
        nc = res.loc[res['Function'] == 'Noncoding']
        bars_noncoding.append(nc.shape[0])
        ns = res.loc[res['Function'] == 'Nonsense']
        bars_nonsense.append(ns.shape[0])
        fram = res.loc[res['Function'] == 'Frameshift']
        bars_frameshift.append(fram.shape[0])
        spl = res.loc[res['Function'] == 'Splice']
        bars_splice.append(spl.shape[0])

        infr = res.loc[res['Function'] == 'In-Frame']
        bars_inframe.append(infr.shape[0])
        nostop = res.loc[res['Function'] == 'No-stop']
        bars_nostop.append(nostop.shape[0])
        unknown = res.loc[res['Function'] == 'Unknown']
        bars_unknwon.append(unknown.shape[0])
    # print(len(genes))
    # print(bars_missense)
    # print(len(bars_missense))
    # exit(0)

    # The position of the bars on the x-axis
    r = list(range(len(genes)))

    # Names of group and bar width
    names = genes
    barWidth = 1
    cmap = plt.cm.coolwarm
    cmap2 = plt.cm.summer

    # Create brown bars: Missense
    if sum(bars_missense) != 0:
        plt.bar(r, bars_missense, color=cmap(0), edgecolor='white', width=barWidth)
    else:
        function_gene.remove('Missense')

    # Create green bars (middle), on top of the firs ones: Synonymous
    if sum(bars_synomymus) != 0:
        plt.bar(r, bars_synomymus, bottom=bars_missense, color=cmap(0.2),
                edgecolor='white', width=barWidth)
    else:
        function_gene.remove('Synonymous')
    
    # Create green bars (top): Noncoding
    bars_miss_syn = [x+y for x, y in zip(bars_missense, bars_synomymus)]
    if sum(bars_noncoding) != 0:
        plt.bar(r, bars_noncoding, bottom=bars_miss_syn, color=cmap(.4),
                edgecolor='white', width=barWidth)
    else:
        function_gene.remove('Noncoding')

    # Creat next bats: Nonsense
    bars_miss_syn_nc = [x+y for x, y in zip(bars_miss_syn, bars_noncoding)]
    if sum(bars_nonsense) != 0:
        plt.bar(r, bars_nonsense, bottom=bars_miss_syn_nc, color=cmap(0.6),
                edgecolor='white', width=barWidth)
    else:
        function_gene.remove('Nonsense')

    # Creaat next bar: Frameshift
    bars_miss_syn_nc_ns = [x+y for x, y in zip(bars_miss_syn_nc,
                                               bars_nonsense)]
    if sum(bars_frameshift) != 0:
        plt.bar(r, bars_frameshift, bottom=bars_miss_syn_nc_ns, color=cmap(0.8),
                edgecolor='white', width=barWidth)
    else:
        function_gene.remove('Frameshift')

    # Creat next bar: Splice
    bars_miss_syn_nc_ns_fram = [x+y for x, y in zip(bars_miss_syn_nc_ns,
                                                    bars_frameshift)]
    if sum(bars_splice) != 0:
        plt.bar(r, bars_splice, bottom=bars_miss_syn_nc_ns_fram, color=cmap(0.99),
                edgecolor='white', width=barWidth)
    else:
        function_gene.remove('Splice')

    # Creat next barbar: In-Frame
    bars_miss_syn_nc_ns_fram_spl = [x+y for x, y in zip(bars_miss_syn_nc_ns_fram,
                                                    bars_splice)]
    if sum(bars_inframe) != 0:
        plt.bar(r, bars_inframe, bottom=bars_miss_syn_nc_ns_fram_spl, color=cmap2(0),
                edgecolor='white', width=barWidth)
    else:
        function_gene.remove('In-Frame')

    # Creat next barbar: No-stop
    bars_miss_syn_nc_ns_fram_spl_infr = [x+y for x, y in zip(bars_miss_syn_nc_ns_fram_spl,
                                                bars_inframe)]
    if sum(bars_nostop) != 0:
        plt.bar(r, bars_nostop, bottom=bars_miss_syn_nc_ns_fram_spl_infr, color=cmap2(0.5),
                edgecolor='white', width=barWidth)
    else:
        function_gene.remove('No-stop')

    # Creat next barbar: Unknown
    bars_miss_syn_nc_ns_fram_spl_infr_nost = [x+y for x, y in zip(bars_miss_syn_nc_ns_fram_spl_infr,
                                                    bars_nostop)]
    if sum(bars_unknwon) != 0:
        plt.bar(r, bars_unknwon, bottom=bars_miss_syn_nc_ns_fram_spl_infr_nost, color=cmap2(0.999),
                edgecolor='white', width=barWidth)
    else:
        function_gene.remove('Unknown')


    # Custom X axis
    plt.xticks(r, names, rotation=90)
    if unique_var == 1:
        plt.ylabel('Number unique Variants')
        name = 'unique_type_of_function_per_gene'
    elif unique_var == 0:
        plt.ylabel('Number of Variants')
        name = 'type_of_function_per_gene'
    plt.legend(function_gene, loc="upper left", prop={'size': 55})

    if param.dis != 0:
        title = param.dis + ' - '
    elif param.dis == 0:
        title = 'Ciliopathy - '
    if param.ch_eng == 0:
        title = title + 'Swiss Data'
    elif param.ch_eng == 1:
        title = title + 'English Data'
    if param.acmg_only == 1:
        title = title + ' ACMG'
    plt.title(title)

    # Save and show graphic
    path_plot = param.return_path_plot(name)
    plt.savefig(path_plot, bbox_inches='tight')
    plt.clf()
    plt.close()
    return


# map[key] -> value
def exist_more_m_diff_variants(dataframe_patho, param):
    if dataframe_patho.shape[0] == 0:
        return
    m_plot = 1
    counters = {}
    for element in dataframe_patho['AA_or_SNP__Pos']:
        counters.setdefault(element, 0)
        counters[element] = counters[element] + 1
    counters_filtered = {k: v for k, v in counters.items() if v >= m_plot}

    # counters_filtered_print = {k: v for k, v in counters.items() if v >= 5}
    # print('\n'.join(counters_filtered_print))

    hist = {}
    for i in counters_filtered.values():
        hist[i] = hist.get(i, 0) + 1
    plt.rcParams.update({'font.size': 18})
    plt.bar(x=list(hist.keys()), height=list(hist.values()))
    plt.xlabel('Repetition of each Variant')
    plt.ylabel('Number of Variants')

    titel = 'Distribution of damaging Unique Variant'
    if param.ch_eng == 0:
        titel = titel + ' - Swiss Data'
    elif param.ch_eng == 1:
        titel = titel + ' - English Data'
    if param.acmg_only == 1:
        titel = titel + ' ACMG'


    path = param.return_path_plot('exist_at_least_variants_distribution')
    plt.title(titel)
    plt.savefig(path, bbox_inches='tight')
    plt.clf()
    plt.close()
    return


def overall_score(dataframe_patho, param):
    if dataframe_patho.shape[0] == 0:
        return
    rc('font')
    plt.rcParams.update({'font.size': 15})
    plt.xticks(fontsize=15, rotation=90)
    plt.yticks(fontsize=15)
    plt.ylabel('Number of variants')
    make_hist_plot(dataframe_patho['Overall_Score'].astype(int),
                       'distribution_overall_score', param)
    return



def make_hist_plot(df_row, name, param):
    if param.dis != 0:
        title = param.dis + ' - '
    elif param.dis == 0:
        title = 'Ciliopathy - '
    if param.ch_eng == 0:
        title = title + 'Swiss Data'
    elif param.ch_eng == 1:
        title = title + 'English Data'
    if param.acmg_only == 1:
        title = title + ' ACMG'
    plt.title(title)

    hist = {}
    for i in df_row:
        hist[i] = hist.get(i, 0) + 1
    plt.bar(x=list(hist.keys()), height=list(hist.values()))

    path_plot = param.return_path_plot(name)
    plt.savefig(path_plot, bbox_inches='tight')
    plt.clf()
    plt.close()
    return


def make_hist_plot_rot(df_row, name, param):
    if param.dis != 0:
        title = param.dis + ' - '
    elif param.dis == 0:
        title = 'Ciliopathy - '
    if param.ch_eng == 0:
        title = title + 'Swiss Data'
    elif param.ch_eng == 1:
        title = title + 'English Data'
    if param.acmg_only == 1:
        title = title + ' ACMG'
    plt.title(title)

    hist = {}
    if df_row.name == 'Gene':
        df_row = df_row.sort_values()
    for i in df_row:
        hist[i] = hist.get(i, 0) + 1
    plt.bar(x=list(hist.keys()), height=list(hist.values()))

    path_plot = param.return_path_plot(name)
    plt.savefig(path_plot, bbox_inches='tight')
    plt.clf()
    plt.close()
    return


def heterozygot_trait(dataframe_patho, param):
    dis = param.dis
    if dataframe_patho.shape[0] == 0:
        print("0 %% are heteroyzgots carrier.")
        return 0
    if param.ch_eng == 0:
        number_of_all_patient = 399
    elif param.ch_eng == 1:
        number_of_all_patient = 999
    number_of_patho_patient = len(dataframe_patho['patient_id'].unique())
    res = float(number_of_patho_patient/number_of_all_patient)*100
    if dis == 0:
        print("%f %% are heteroyzgots carrier." % (res))
    elif dis != 0:
        print("%f %% are heteroyzgots carrier." % (res))
    return


def genes_mut_per_pat(dataframe_patho, dataframe_full, param):
    if dataframe_patho.shape[0] == 0:
        print('No pathologic variants')
        return
    if dataframe_full.shape[0] == 0:
        print('No variants')
        return
    number_of_variants = []
    number_of_variants_patho = []
    patient_id = np.unique(dataframe_full['patient_id'])
    for i in patient_id:
        res = dataframe_full.loc[
            dataframe_full['patient_id'].astype(int) == i]
        res_patho = dataframe_patho.loc[
            dataframe_patho['patient_id'].astype(int) == i]
        res_s = len(res['Gene'].unique())
        res_s_patho = len(res_patho['Gene'].unique())
        number_of_variants.append(res_s)
        number_of_variants_patho.append(res_s_patho)
    average = float(sum(number_of_variants)/len(number_of_variants))
    average_patho = float(
        sum(number_of_variants_patho)/len(number_of_variants_patho))
    print(("%f is the average number of genes with variants "
           "per patient. IGNORING if it is pathologie/benign" % average))
    print(("%f is the average number of genes with pathogenic "
           "variants per patient." % average_patho))
    return


def find_BBS1(dataframe):
    if dataframe.shape[0] == 0:
        return
    name = 'BBS1'
    res = dataframe.loc[dataframe['Gene'] == name]
    print('There are %i of BBS1 mutation' % res.shape[0])
    return
