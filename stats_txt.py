from class_def import Parameter
import matplotlib.pyplot as plt
import pandas as pd
from variant_filter import damaging_variant
import damaging_filter as dam
import numpy as np
from recurring_variants import af_homo_cohort, df_cohort_homo_af
# import seaborn as sns


def count_function(dataframe, txt_file, ch_eng, dis=0):
    all_function = dataframe['Function'].unique()

    file = open(txt_file, 'w')
    if ch_eng == 0:
        text = 'Swiss Data'
    elif ch_eng == 1:
        text = 'English Data'

    if dis == 0:
        text = text + ' - ciliopathy'
    elif dis != 0:
        text = text + ' - ' + str(dis)
    text = text + '\n\n'
    file.write(text)

    all_variants = dataframe.shape[0]
    text_all_variants = 'Total variants: %d \n' % all_variants
    file.write(text_all_variants)

    variant_count = pd.Series()
    counters = {}
    for pat in dataframe['patient_id'].unique():
        num_var = dataframe[dataframe['patient_id']== pat].shape[0]

        counters.setdefault(num_var, 0)
        counters[num_var] = counters[num_var] + 1
        variant_count = variant_count.append(pd.Series([num_var]))

    keys_list = list(counters.keys())
    text_range = 'The range is %d to %d \n' % (variant_count.min(), variant_count.max())
    file.write(text_range)
    text_average = 'The average is %0.3f \n' % variant_count.mean()
    file.write(text_average)
    text_median = 'The median is %0.3f \n' % variant_count.median()
    file.write(text_median)


    for fun in all_function:
        fun_df = dataframe.loc[dataframe['Function'] == fun]
        count_people = fun_df.shape[0]
        list_people = fun_df['patient_id'].unique()

        number_of_pat = len(list_people)
        if number_of_pat == 1:
            sing = ' person'
        else:
            sing = ' persons'
        text = ('%i' + sing) % number_of_pat
        text = text + ' with ' + str(fun) + ' variants (total of '
        text = (text + '%i ' + str(fun)) % count_people
        if count_people == 1:
            text = text + ' variant'
        else:
            text = text + ' variants'
        text = text + ')\n'
        file.write(text)
    file.write('\n')
    file.close()
    return


def count_damaging(dataframe, txt_file, dis=0):
    file = open(txt_file, 'a')

    param = Parameter(0.004, 500, 500)
    if dis != 0:
        param.dis = dis
    damaging = damaging_variant(dataframe, param)

    text = 'AF = %.3f, CADD = %i, n = %i' % (param.f, param.cadd, param.n)
    if dis == 0:
        text = text + ', all ciliopathy:'
    else:
        text = text + ' ' + dis + ' :'

    count_people = len(damaging['patient_id'].unique())
    coutn_var = damaging.shape[0]
    text = text + ' %i persons have damaging ' % count_people
    text = text + 'variants (total of %i variants) \n\n' % coutn_var

    file.write(text)
    file.close()
    return

def write_damaging(dataframe, txt_file):
    file = open(txt_file, 'a')

    file.close()
    return


def dic_value_higher_than_key_m(dict, m):
    result = 0
    for (k, v) in dict.items():
        if k >= m:
            result = result + v

    return result


# map[key] -> value
def exist_more_m_diff_variants(dataframe, txt_file, ch_eng):
    m_plot = 1
    m_write = 3

    counters = {}

    for element in dataframe['AA_or_SNP__Pos']:
        counters.setdefault(element, 0)
        counters[element] = counters[element] + 1

    counters_filtered = {k: v for k, v in counters.items() if v >= m_plot}
    # counters_filtered_print = {k: v for k, v in counters.items() if v >= 40}
    # print('\n'.join(counters_filtered_print.keys()))

    # sns.distplot(list(counters_filtered.values()))
    # plt.show()

    hist = {}
    for i in counters_filtered.values():
        hist[i] = hist.get(i, 0) + 1
    plt.rcParams.update({'font.size': 18})
    plt.bar(x=list(hist.keys()), height=list(hist.values()))
    plt.xlabel('Repetition of each Variant')
    plt.ylabel('Number of Variants')
    titel = 'Distribution of all Unique Variant'

    path = folder_path + 'distributino_of_different_variant_alldata'
    if ch_eng == 0:
        path = path + '_ch.png'
        titel = titel + ' - Swiss Data'
    elif ch_eng == 1:
        path = path + '_eng.png'
        titel = titel + ' - English Data'

    plt.title(titel)
    plt.savefig(path, bbox_inches='tight')
    plt.clf()
    plt.close()

    file = open(txt_file, 'a')
    recure_more_than_m_times = dic_value_higher_than_key_m(hist, m_write)
    text = 'There are %i ' % recure_more_than_m_times
    text = text + 'variants which recur more than %i times \n\n' % m_write
    file.write(text)
    file.close()
    return


# map[key] -> value
def exist_more_m_diff_variants_damaging(damaging, ch_eng):
    m_plot = 1

    counters = {}

    for element in damaging['AA_or_SNP__Pos']:
        counters.setdefault(element, 0)
        counters[element] = counters[element] + 1

    counters_filtered = {k: v for k, v in counters.items() if v >= m_plot}

    hist = {}
    for i in counters_filtered.values():
        hist[i] = hist.get(i, 0) + 1
    plt.rcParams.update({'font.size': 18})
    plt.bar(x=list(hist.keys()), height=list(hist.values()))
    plt.xlabel('Repetition of each Variant')
    plt.ylabel('Number of Variants')
    titel = 'Distribution of all Unique Damaging Variant'

    path = folder_path + 'distributino_of_different_variant_damaging'
    if ch_eng == 0:
        path = path + '_ch.png'
        titel = titel + ' - Swiss Data'
    elif ch_eng == 1:
        path = path + '_eng.png'
        titel = titel + ' - English Data'

    plt.title(titel)
    plt.savefig(path, bbox_inches='tight')
    plt.clf()
    plt.close()
    return


def damaging_variant_print(dataframe, param, txt_file, acmg_only=0):
    total_individual = len(dataframe['patient_id'].unique())
    file = open(txt_file, 'a')
    param.print_value()
    if dataframe.shape[0] == 0:
        return dataframe

    text = 'Numbe of true calls %d \n' % dataframe.shape[0]
    dataframe = dam.af_homo_cohort(dataframe, 0.004, param.ch_eng)
    text1 = 'Numbe of rare calls %d \n' % dataframe.shape[0]
    file.write(text + text1)

    cadd = param.cadd
    n = param.n

    (df_ClinVar_patho, df_ClinVar_benign, df_ClinVar_rest) = dam.ClinVar_separation(dataframe)
    text = 'ClinVar patho: %d \n' % df_ClinVar_patho.shape[0]
    print(text)
    df_ClinVar_rest = df_ClinVar_rest.append(df_ClinVar_benign)

    HGMD_patho_only, _  = dam.HGMD_separation(dataframe, acmg_only)
    text2 = 'HGMD patho: %d \n' % HGMD_patho_only.shape[0]
    (HGMD_patho, HGMD_rest) = dam.HGMD_separation(df_ClinVar_rest, acmg_only)
    text3 = 'ClinVar + HGMD patho: %d \n' % (HGMD_patho.shape[0] + df_ClinVar_patho.shape[0])
    print(text2)
    print(text3)

    file.write((text + text2 + text3))
    (result_pathogenic, dataframe_rest) = dam.clear_pathogenic(HGMD_rest)
    text4 = 'Loss of Function mutation: %d \n' % (result_pathogenic.shape[0])
    print(text4)

    # get result_AF has at least AF of f
    result_AF = dam.allel_frequency(dataframe_rest, param.f)

    if cadd == n and n in (100, 200, 300, 400, 500, 600):
        result_cadd_n = dam.MCAP_CADD_sep(dataframe_rest, param)
    elif cadd != 0 and n != 0:
        result_prediction_software = dam.at_least_n_pathogenic(
            dataframe_rest, n)
        result_cadd_n = dam.CADD(result_prediction_software, cadd)
    elif cadd != 0 and n == 0:
        result_cadd_n = dam.CADD(dataframe_rest, cadd)
    elif cadd == 0 and n != 0:
        result_cadd_n = dam.at_least_n_pathogenic(
            dataframe_rest, n)
    elif cadd == 0 and n == 0:
        result_cadd_n = dataframe_rest



    if acmg_only == 1:
        text5 = '---------ACMG only----------\n'
        text5 = text5 + 'No remaining variants \n'
        result_end = result_pathogenic
    elif acmg_only == 0:
        text5 = '---------All variants-------\n'
        text5 = text5 + 'Remaining damaging variants: %d \n' % (result_cadd_n.shape[0])
        result_end = result_cadd_n.append(result_pathogenic, sort=False)

    result_end = result_end.append(df_ClinVar_patho, sort=False)
    result_end = result_end.append(HGMD_patho, sort=False)

    text6 = 'Total number of damaging variants: %d \n' %(result_end.shape[0])
    file.write((text4 + text5 + text6))

    variant_count = pd.Series()
    variants_in_same_gene = 0
    variants_at_least_3 = 0
    text_gene = ''
    for pat in result_end['patient_id'].unique():
        temp = result_end[result_end['patient_id']== pat]
        num_var = temp.shape[0]
        variant_count = variant_count.append(pd.Series([num_var]))
        gene_unique = temp['Gene'].unique()
        for gene in gene_unique:
            one_gene = temp[temp['Gene'] == gene]
            if one_gene.shape[0] > 1:
                variants_in_same_gene = variants_in_same_gene + 1
            if one_gene.shape[0] > 2:
                variants_at_least_3 = variants_at_least_3 + 1
                text_gene = gene

    diff = total_individual - variant_count.shape[0]
    affected = variant_count.shape[0]

    no_mut = np.zeros(diff)
    no_mut_serie = pd.Series(no_mut)
    variant_count = variant_count.append(no_mut_serie)

    text_effected = 'Number of Indivuduals with damaging variants: %d, %.2f \n' % (affected, float(affected)*100/total_individual)
    text_not_effected = 'Number of Indivuduals wihtout damaging variants: %d, %.2f \n' % (diff, float(diff)*100/total_individual)
    file.write(text_effected + text_not_effected)
    text_range = 'The range of damaging variants for an individual is %d to %d \n' % (variant_count.min(), variant_count.max())
    file.write(text_range)
    text_average = 'The average number of damaging variants per indivudal is %0.3f \n' % variant_count.mean()
    file.write(text_average)
    text_median = 'The median number of damaging variants per indivudal is %0.3f \n' % variant_count.median()
    file.write(text_median)
    text_two_damaging_variants = 'There are %d, %.2f people with two or more variants in same gene \n' % (variants_in_same_gene, float(variants_in_same_gene)*100/total_individual)
    text_more_3 = 'From those people %d, %.2f person/people have at least 3 varitants in same gene - ' % (variants_at_least_3, float(variants_at_least_3)*100/total_individual)
    text_more_3 = text_more_3 + text_gene + '\n'
    file.write((text_two_damaging_variants + text_more_3))
    file.close()
    return


def damaging_variant_acmg_af_hom(dataframe, param, txt_file, acmg_only):
    file = open(txt_file, 'a')
    file.write('===============\n')
    print(dataframe.shape)
    param.print_value()
    if dataframe.shape[0] == 0:
        return dataframe

    cadd = param.cadd
    n = param.n

    if acmg_only == 0:
        text = 'DM, DM?, FP and DP \n'
    if acmg_only == 1:
        text = 'DM only\n'
    file.write(text)

    # HGMD
    (HGMD_p, HGMD_r) = dam.HGMD_separation(dataframe, acmg_only)
    HGMD_p_af_homo = dam.af_homo_cohort(HGMD_p, 0.004, param.ch_eng)
    text = 'HGMD only: %d \n' % HGMD_p.shape[0]
    text1 = 'HGMD AF and Hom: %d \n' % HGMD_p_af_homo.shape[0]
    file.write(text + text1)

    # ClinVar
    (df_ClinVar_patho, df_ClinVar_benign, df_ClinVar_rest) = dam.ClinVar_separation(dataframe)
    df_ClinVar_patho_af_homo = dam.af_homo_cohort(df_ClinVar_patho, 0.004, param.ch_eng)
    text = 'ClinVar only: %d \n' % df_ClinVar_patho.shape[0]
    text1 = 'ClinVar AF and Hom: %d \n' % df_ClinVar_patho_af_homo.shape[0]
    file.write(text + text1)

    # truncating
    (trun_path, trunc_rest) = dam.clear_pathogenic(dataframe)
    trun_patho_af_homo = dam.af_homo_cohort(trun_path, 0.004, param.ch_eng)
    text = 'Truncating only: %d \n' % trun_path.shape[0]
    text1 = 'Truncating AF and Hom: %d \n' % trun_patho_af_homo.shape[0]
    file.write(text + text1)


    # ClinVar and HGMD
    df_ClinVar_rest = df_ClinVar_rest.append(df_ClinVar_benign)
    (HGMD_patho, HGMD_rest) = dam.HGMD_separation(df_ClinVar_rest, acmg_only)
    ClinVar_HGMD = df_ClinVar_patho.append(HGMD_patho, sort=False)
    HGMD_CliVar_af_homo = dam.af_homo_cohort(ClinVar_HGMD, 0.004, param.ch_eng)
    text = 'HGMD & ClinVar only: %d \n' % ClinVar_HGMD.shape[0]
    text1 = 'HGMD & ClinVar AF and Hom: %d \n' % HGMD_CliVar_af_homo.shape[0]
    file.write(text + text1)

    (result_pathogenic, dataframe_rest) = dam.clear_pathogenic(HGMD_rest)

    result_end = result_pathogenic
    result_end = result_end.append(df_ClinVar_patho, sort=False)
    result_end = result_end.append(HGMD_patho, sort=False)

    # get result_AF has at least AF of f
    res_af_homo = dam.af_homo_cohort(result_end, 0.004, param.ch_eng)

    text1 = 'ACMG (=HGMD + ClinVAr + truncating): %d \n' % result_end.shape[0]
    text2 = 'ACMG and AF/Hom: %d \n' % res_af_homo.shape[0]
    file.write(text1 + text2)


    file.close()
    return


def ClinVar_versus_HGMD(dataframe, txt_file, acmg_only):
    file = open(txt_file, 'a')
    if dataframe.shape[0] == 0:
        return dataframe


    (df_ClinVar_patho, df_ClinVar_benign, df_ClinVar_rest) = dam.ClinVar_separation(dataframe)
    text = '-------\nClinVar patho: %d \n' % df_ClinVar_patho.shape[0]
    print(text)
    # df_ClinVar_rest = df_ClinVar_rest.append(df_ClinVar_benign)

    (HGMD_patho_only, HGMD_rest ) = dam.HGMD_separation(dataframe, acmg_only)
    text2 = 'HGMD patho: %d \n' % HGMD_patho_only.shape[0]
    print(text2)


    file.write((text + text2))

    not_overlapping_ClinVar_HGMD = HGMD_patho_only.append(df_ClinVar_patho).drop_duplicates(keep=False)
    txt_not_overlapping = 'Not overlapping are %d \n' %not_overlapping_ClinVar_HGMD.shape[0]
    all_variants = HGMD_patho_only.append(df_ClinVar_patho).drop_duplicates()
    txt_overlapping = 'Overlapping are %d \n' % (all_variants.shape[0] - not_overlapping_ClinVar_HGMD.shape[0])

    file.write(txt_not_overlapping + txt_overlapping)



    # file.write(('In the Clinvar pathogenic there were the following HGMD classifications: \n' + str(df_ClinVar_patho['HGMD'].unique())))

    print(HGMD_patho_only['Clinvar_Significance'].unique())
    txt = 'In the HGMD pathogenic there were the following ClinVar classifications: \n' + str(HGMD_patho_only['Clinvar_Significance'].unique())
    (df_patho, df_benign, df_rest) = dam.ClinVar_separation(HGMD_patho_only)

    txt_benign = '\nin the HGMD pathogenic %d are classified as ClinVar benign \n' % (df_benign['Clinvar_Significance'].shape[0])
    txt_both_pathp = 'Classified twice pathogenic ClinVar and HGMD %d \n' % (df_patho.shape[0])


    text3 = 'ClinVar + HGMD patho: %d \n' % (HGMD_patho_only.shape[0] + df_ClinVar_patho.shape[0])
    print(txt_benign + txt_both_pathp + text3)
    file.write((txt + txt_benign + txt_both_pathp + text3))
    file.close()
    return


def oligogenicity(dataframe, txt_file, ch_eng, acmg_only):
    file = open(txt_file, 'a')
    if dataframe.shape[0] == 0:
        return dataframe
    param = Parameter(0.004, 500, 500, ch_eng=ch_eng, dis=0, acmg_only=acmg_only)
    damaging = damaging_variant(dataframe, param)

    counters = {}
    for pat in dataframe['patient_id'].unique():
        num_var = damaging[damaging['patient_id']== pat]
        num_gene = num_var['Gene'].unique().size
        counters.setdefault(num_gene, 0)
        counters[num_gene] = counters[num_gene] + 1
    print(counters)
    total_individual = dataframe['patient_id'].unique().size
    oligo = (total_individual - counters[0] - counters[1])
    txt = 'Number of patient with  >=2 damaging variants: %i, %.2f \n' % (oligo, float(oligo)*100/total_individual)
    txt2 = str(counters) + '\n\n'
    file.write(txt + txt2)
    file.close()
    return

def compare_freq(dataframe, freq, ch_eng, txt_file):
    file = open(txt_file, 'a')
    def return_size(element):
        return element.shape[0]

    df = af_homo_cohort(dataframe=dataframe, freq=freq, ch_eng=ch_eng)
    df_cohort = df_cohort_homo_af(df, cohort=1, homo=0, af=0)
    counts_per_aa = df_cohort.groupby('AA_or_SNP__Pos').apply(return_size)
    max_aa = counts_per_aa.idxmax()
    df_of_max_aa = dataframe[dataframe['AA_or_SNP__Pos'] == max_aa]
    mutation_name = df_of_max_aa['Mutation_Call_HGVS_Coding'].unique()



    text = 'Recurring Variants which are rare in GnomAD and Homo but appier often in our Cohort\n'
    text = text + 'Max occurence:' + str(counts_per_aa.max()) + '\n'
    text = text + 'Min occurence: (by definition) ' + str(counts_per_aa.min()) + '\n'
    text = text + 'Mean occurence: ' + str(counts_per_aa.mean()) + '\n'
    text = text + 'Median occurence: ' + str(counts_per_aa.median()) + '\n'
    text = text + 'Most common: ' + str(mutation_name[0]) + '\n\n'

    file.write(text)
    file.close()
    return



def make_txt_file(dataframe, txt_file, ch_eng, dis=0):
    param = Parameter(0.004, 500, 500, ch_eng)
    count_function(dataframe, txt_file, ch_eng)
    count_damaging(dataframe, txt_file)
    damaging_variant_print(dataframe, param, txt_file, acmg_only=1)
    oligogenicity(dataframe, txt_file, ch_eng, acmg_only=1)
    damaging_variant_print(dataframe, param, txt_file, acmg_only=0)
    oligogenicity(dataframe, txt_file, ch_eng, acmg_only=0)
    damaging_variant_acmg_af_hom(dataframe, param, txt_file, acmg_only=1)
    damaging_variant_acmg_af_hom(dataframe, param, txt_file, acmg_only=0)
    ClinVar_versus_HGMD(dataframe, txt_file, acmg_only=1)
    ClinVar_versus_HGMD(dataframe, txt_file, acmg_only=0)
    exist_more_m_diff_variants(dataframe, txt_file, ch_eng)
    exist_more_m_diff_variants_damaging(damaging_variant(dataframe, param), ch_eng)
    compare_freq(dataframe, freq=0.004, ch_eng=ch_eng, txt_file=txt_file)
    return


if __name__ == '__main__':

    folder_path = '/home/selene/Programm/Diss/'

    pickel_filename = 'true_calls.pkl'
    pickel_filename_eng = 'true_calls_eng.pkl'

    true_calls = pd.read_pickle(folder_path + pickel_filename)
    true_calls_eng = pd.read_pickle(folder_path + pickel_filename_eng)

    # ch_eng = 1
    for i in range(2):
        print(i)
        ch_eng = i
        if ch_eng == 0:
            name = 'stats_ch.txt'
            calls = true_calls
        elif ch_eng == 1:
            name = 'stats_eng.txt'
            calls = true_calls_eng

        make_txt_file(calls, name, ch_eng)

