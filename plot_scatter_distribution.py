import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
import pylab
import variant_filter as va
# from sys import exit


pickel_filename = 'true_calls.pkl'
folder_path = '/home/selene/Programm/Diss'
true_calls = pd.read_pickle(folder_path + '/' + pickel_filename)
AF = [0.005, 0.001]
CADD_cutOffs = [10, 15, 20, 25]
# prediction_software == 0: only look at CADD
# prediction_software == 1: CADD>20 and 4 times pathgenic
# prediction_software == 2: only look at 7 pathgenic tests
prediction_software = 2

# ----------- Ploting ------------------


def make_hist_plot(df_row):
    hist = {}
    for i in df_row:
        hist[i] = hist.get(i, 0) + 1
    plt.bar(x=list(hist.keys()), height=list(hist.values()))
    pylab.show()
    return


def scatter_plot_n(dataframe, n):
    plt.clf()
    path = '/home/selene/Programm/Diss/plot/'
    # dataframe are the true calls
    freq = np.delete(np.linspace(0, 0.005, 50), 0)
    CADD_cutOff = np.linspace(10, 25, 16)
    plot_data = pd.DataFrame(columns=['x', 'y', 'number_of_variants'])
    for f in freq:
        print(f)
        result_AF = va.allel_frequency(dataframe, f)
        (result_pathogenic, dataframe_rest) = va.clear_pathogenic(result_AF)
        for cadd in CADD_cutOff:
            result_prediction_software = va.at_least_n_pathogenic(
                dataframe_rest, n)
            result_prediction_software_CADD = va.CADD(
                result_prediction_software, cadd)
            # result AF_prediciton = cleary pathogenic +
            # at least AF of f AND
            # n times classified as pathogenic AND
            # CADD>cadd
            result_AF_prediction_CADD = result_prediction_software_CADD.append(
                result_pathogenic, sort=False)
            color = result_AF_prediction_CADD.shape[0]
            plot_data = plot_data.append(
                {'x': f, 'y': cadd, 'number_of_variants': color},
                ignore_index=True)
    plt.scatter(plot_data['x'], plot_data['y'],
                c=plot_data['number_of_variants'], s=40, marker='s')
    plt.colorbar()
    plt.xlabel('Allel Frequency')
    plt.xlim(0, 0.005)
    plt.ylabel('CADD')
    plt.title(str(n) + ' pathogenic predictions')
    plt.savefig(path + str(n) + '_' +
                'number_of_variants_per AF_and_CADD.png', bbox_inches='tight')
    # plt.show()
    return


def get_plotting_data(dataframe, n):
    # dataframe are the true calls
    freq = np.delete(np.linspace(0, 0.005, 10), 0)
    CADD_cutOff = np.linspace(10, 25, 15)
    plot_data = pd.DataFrame(columns=['x', 'y', 'number_of_variants', 'n'])
    for f in freq:
        result_AF = va.allel_frequency(dataframe, f)
        (result_pathogenic, dataframe_rest) = va.clear_pathogenic(result_AF)
        for cadd in CADD_cutOff:
            result_prediction_software = va.at_least_n_pathogenic(
                dataframe_rest, n)
            result_prediction_software_CADD = va.CADD(
                result_prediction_software, cadd)
            # result AF_prediciton = cleary pathogenic +
            # at least AF of f AND
            # n times classified as pathogenic AND
            # CADD>cadd
            result_AF_prediction_CADD = result_prediction_software_CADD.append(
                result_pathogenic, sort=False)
            color = result_AF_prediction_CADD.shape[0]
            plot_data = plot_data.append(
                {'x': f, 'y': cadd, 'number_of_variants': color, 'n': n},
                ignore_index=True)
    return plot_data


def plot_something(data, n, ax, min_value, max_value):
    ax.set_xlim(0, 0.006)
    ax.set_title(str(n) + ' pathogenic predictions')
    ax.set_ylabel('CADD')
    ax.set_xlabel('Allel Frequency')
    return ax.scatter(data['x'], data['y'], c=data['number_of_variants'],
                      s=100, marker='s', vmin=min_value, vmax=max_value)


def scatter_plot(dataframe):
    path = '/home/selene/Programm/Diss/plot/'
    n_max = 7
    n_min = 2
    n_number = n_max - n_min + 1
    n_list = np.linspace(n_min, n_max, n_number)
    result_data = pd.DataFrame()
    for n in n_list:
        result_data = result_data.append(get_plotting_data(dataframe, n))
    min_val = result_data['number_of_variants'].min()
    max_val = result_data['number_of_variants'].max()
    fig, axes = plt.subplots(ncols=n_number+1, figsize=((n_number+1)*5, 5))
    n = n_min
    for ax in axes.flat:
        if n == n_max + 1:
            break
        subgroup = result_data.loc[result_data['n'] == n]
        scatters = plot_something(subgroup, n, ax, min_val, max_val)
        n = n + 1
    fig.colorbar(scatters, axes[n_number])
    plt.savefig(path + str(n_min) + '_' + str(n_max) + '_' +
                'number_of_variants_per AF_and_CADD.png', bbox_inches='tight')
    # plt.show()
    return


if __name__ == "__main__":
    # scatter_plot(true_calls)
    scatter_plot_n(true_calls, 0)
    scatter_plot_n(true_calls, 2)
    scatter_plot_n(true_calls, 4)
    scatter_plot_n(true_calls, 5)
    scatter_plot_n(true_calls, 6)
    scatter_plot_n(true_calls, 7)
    scatter_plot_n(true_calls, 8)
