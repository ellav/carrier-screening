import pandas as pd
import matplotlib.pyplot as plt
from second_analysis import (full_analysis, full_analysis_per_disease, typ_of_function_per_gene)
from damaging_filter import (CADD, ClinVar_separation, MCAP_CADD_sep,
                             at_least_n_pathogenic, clear_pathogenic,
                             allel_frequency, HGMD_separation, af_homo_cohort)
import split_by_disorder as split
from class_def import Parameter
from spreadsheet_generating import (damaging_variants_spreadsheet, more_2_variants_spreadsheet,
                                    ex_1_mut_per_gene, exist_more_m_diff_variants,
                                    ex_2_mut_per_patient, at_least_3_mut_per_patient,
                                    true_calls_ss)


# spreadsheet excluded:
# false calls from predict 4.0
# failing manual cleaning in preporcess true calls
# common variants in spreadsheet_excluded_gen

def damaging_variant(dataframe, param):
    param.print_value()
    if dataframe.shape[0] == 0:
        return dataframe

    dataframe = af_homo_cohort(dataframe, param.f, param.ch_eng)

    cadd = param.cadd
    n = param.n
    (df_ClinVar_patho, df_ClinVar_benign, df_ClinVar_rest) = ClinVar_separation(dataframe)
    df_ClinVar_rest = df_ClinVar_rest.append(df_ClinVar_benign)
    (HGMD_patho, HGMD_rest) = HGMD_separation(df_ClinVar_rest, param.acmg_only)
    (result_pathogenic, dataframe_rest) = clear_pathogenic(HGMD_rest)

    result_end = result_pathogenic.append(df_ClinVar_patho, sort=False)
    result_end = result_end.append(HGMD_patho, sort=False)
    if param.acmg_only == 1:
        return result_end


    if cadd == n and n in (100, 200, 300, 400, 500, 600):
        result_cadd_n = MCAP_CADD_sep(dataframe_rest, param)
    elif cadd != 0 and n != 0:
        result_prediction_software = at_least_n_pathogenic(
            dataframe_rest, n)
        result_cadd_n = CADD(result_prediction_software, cadd)
    elif cadd != 0 and n == 0:
        result_cadd_n = CADD(dataframe_rest, cadd)
    elif cadd == 0 and n != 0:
        result_cadd_n = at_least_n_pathogenic(
            dataframe_rest, n)
    elif cadd == 0 and n == 0:
        result_cadd_n = dataframe_rest

    # add the missense variants to the acmg damaging
    result_end = result_end.append(result_cadd_n, sort=False)

    return result_end


def damaging_variant_no_hgmd(dataframe, param):
    print(dataframe.shape)
    param.print_value()
    if dataframe.shape[0] == 0:
        return dataframe

    cadd = param.cadd
    n = param.n
    (df_ClinVar_patho, df_ClinVar_benign, df_ClinVar_rest) = ClinVar_separation(dataframe)
    df_ClinVar_rest = df_ClinVar_rest.append(df_ClinVar_benign)
    (result_pathogenic, dataframe_rest) = clear_pathogenic(df_ClinVar_rest)

    # get result_AF has at least AF of f
    result_AF = allel_frequency(dataframe_rest, param.f)

    if cadd == n and n in (100, 200, 300, 400, 500, 600):
        result_cadd_n = MCAP_CADD_sep(dataframe_rest, param)
    elif cadd != 0 and n != 0:
        result_prediction_software = at_least_n_pathogenic(
            dataframe_rest, n)
        result_cadd_n = CADD(result_prediction_software, cadd)
    elif cadd != 0 and n == 0:
        result_cadd_n = CADD(dataframe_rest, cadd)
    elif cadd == 0 and n != 0:
        result_cadd_n = at_least_n_pathogenic(
            dataframe_rest, n)
    elif cadd == 0 and n == 0:
        result_cadd_n = dataframe_rest

    print(result_cadd_n.shape)
    result_end = result_cadd_n.append(result_pathogenic, sort=False)
    result_end = result_end.append(df_ClinVar_patho, sort=False)

    return result_end


def find_low_overall_score(dataframe):
    low_score_df = dataframe[dataframe['Overall_Score'] <= 8.5]
    low_score_pat = low_score_df['patient_id'].unique()
    print(low_score_df[['patient_id', 'Index', 'Chrom', 'Pos']])
    print(low_score_pat)
    return


def typ_of_function_per_gene_two(dataframe, param1, param2):
    damaging1 = damaging_variant(dataframe, param1)
    gene1 = damaging1['Gene'].unique()
    damaging2 = damaging_variant(dataframe, param2)
    gene2 = damaging2['Gene'].unique()
    typ_of_function_per_gene(damaging1, param1, unique_var=0, gene_list2=gene2)
    typ_of_function_per_gene(damaging2, param2, unique_var=0, gene_list2=gene1)
    return


if __name__ == "__main__":
    ch_eng = 1
    if ch_eng == 0:
        pickel_filename = 'true_calls.pkl'
    elif ch_eng == 1:
        pickel_filename = 'true_calls_eng.pkl'
    folder_path = '/home/selene/Programm/Diss'
    true_calls = pd.read_pickle(folder_path + '/' + pickel_filename)



    (disorder, gene_disorder_pd) = split.get_disorder_list()

    disorder = ['Joubert', 'Bardet-Biedl_syndrome']

    list_param = [Parameter(0.002, 0, 6),
                  Parameter(0.002, 15, 0),
                  Parameter(0.002, 15, 4),
                  Parameter(0.002, 20, 4),
                  Parameter(0.004, 0, 6),
                  Parameter(0.004, 15, 0),
                  Parameter(0.004, 15, 4),
                  Parameter(0.004, 20, 4),
                  Parameter(0.004, 100, 100),
                  Parameter(0.004, 200, 200),
                  Parameter(0.004, 300, 300),
                  Parameter(0.004, 400, 400),
                  Parameter(0.004, 0, 0)]
    list_param = [Parameter(0.004, 100, 100),
                  Parameter(0.004, 200, 200),
                  Parameter(0.004, 300, 300),
                  Parameter(0.004, 400, 400),
                  Parameter(0.004, 20, 4)]
    list_param = [Parameter(0.004, 500, 500, acmg_only=0),
                  Parameter(0.004, 500, 500, acmg_only=1)]


    for dis in disorder:
        print('Now looking at ' + dis)
        affected_gene = split.affected_gene_per_disorder(
                            gene_disorder_pd, dis)
        true_calls_dis = split.df_for_disorder(true_calls, affected_gene)

        for param in list_param:
            param.dis = dis
            param.ch_eng = ch_eng
            param.print_value()
            damaging = damaging_variant(true_calls_dis, param)
            # full_analysis_per_disease(damaging, true_calls_dis, param)
            # damaging_variants_spreadsheet(damaging, param)
            # more_2_variants_spreadsheet(damaging, param)
            # ex_1_mut_per_gene(damaging, param)
            # ex_2_mut_per_patient(damaging, param)
            # at_least_3_mut_per_patient(damaging, param)
            # true_calls_ss(true_calls_dis, param)

    print('---------------------------')
    print('Now looking at all the data')

    for param in list_param:
        param.dis = 0
        param.ch_eng = ch_eng
        param.print_value()
        damaging = damaging_variant(true_calls, param)
        # find_low_overall_score(damaging)
        full_analysis(damaging, true_calls, param)
        # exist_more_m_diff_variants(damaging, param, 4)
        # damaging_variants_spreadsheet(damaging, param)
        # more_2_variants_spreadsheet(damaging, param)
        # ex_1_mut_per_gene(damaging, param)
        ex_2_mut_per_patient(damaging, param)
        at_least_3_mut_per_patient(damaging, param)
        # true_calls_ss(true_calls, param)

    # typ_of_function_per_gene_two(true_calls,
    #     Parameter(0.004, 500, 500, dis=0, ch_eng = ch_eng, acmg_only=0),
    #     Parameter(0.004, 500, 500, dis=0, ch_eng = ch_eng, acmg_only=1))    
