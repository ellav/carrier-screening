import pandas as pd
import numpy as np
import os
# Import the library
import matplotlib.pyplot as plt
from matplotlib_venn import venn3



def af_homo_cohort(dataframe, freq, ch_eng):
    dataframe = frequent_in_cohort(dataframe, ch_eng)
    dataframe = homo(dataframe)

    dataframe = allel_frequency(dataframe, freq)

    return dataframe


def allel_frequency(dataframe, freq):
    AF_name = 'INFO_AF'
    dataframe.loc[:, AF_name] = pd.to_numeric(
        dataframe[AF_name], errors='coerce')
    # fill NaN with 0
    allel_pd = dataframe.fillna(value={AF_name: 0})
    end = allel_pd.loc[allel_pd[AF_name] < freq]
    end.loc[:, 'frequent_in_AF'] = 0
    result = allel_pd.join(end['frequent_in_AF'], on='merge_index', how='left')
    result['frequent_in_AF'] = result['frequent_in_AF'].fillna(1).astype('int')
    return result


def homo(dataframe):
    cutOff = 2 # including this CutOff
    AF_name = 'INFO_HOM'
    dataframe.loc[:, AF_name] = pd.to_numeric(
        dataframe[AF_name], errors='coerce')
    allel_pd = dataframe.fillna(value={AF_name: 0})
    end = allel_pd.loc[allel_pd[AF_name] <= cutOff]

    Hemi_name = 'INFO_HEMI'
    end.loc[:, Hemi_name] = pd.to_numeric(
        end[Hemi_name], errors='coerce')
    result = end.fillna(value={Hemi_name: 0})
    result = result.loc[result[Hemi_name] <= cutOff]
    result.loc[:,'frequent_homo_hemi'] = 0
    result_added_col = dataframe.join(result['frequent_homo_hemi'], on='merge_index', how='left')
    result_added_col['frequent_homo_hemi'] = result_added_col['frequent_homo_hemi'].fillna(1).astype('int')
    return result_added_col


def frequent_in_cohort(dataframe, ch_eng):
    result = frequent_in_own_cohort(dataframe, ch_eng)
    result = frequent_in_other_cohort(result, ch_eng)
    return result


def frequent_in_own_cohort(dataframe, ch_eng):
    if ch_eng == 0:
        nat = 'CH'
        total_individual = 400
    elif ch_eng == 1:
        nat = 'ENG'
        total_individual = 1000
    CutOff_freq = int(float(total_individual)*0.01)
    counters = {}

    for element in dataframe['AA_or_SNP__Pos']:
        counters.setdefault(element, 0)
        counters[element] = counters[element] + 1

    counters_not_saved = {k: v for k, v in counters.items() if v > CutOff_freq}
    # file_name = nat + '-frquent_variants.txt'
    # with open(file_name, 'w') as file:
    #     file.write('\n'.join(counters_not_saved.keys()))
    # counters_not_saved = {k: v for k, v in counters.items() if v > CutOff_freq}
    # print('\n'.join(counters_filtered_print.keys()))

    frequent_variant_df = pd.DataFrame()
    for variant in list(counters_not_saved.keys()):
        temp = dataframe.loc[dataframe['AA_or_SNP__Pos'] == variant]
        frequent_variant_df = frequent_variant_df.append(temp, sort=False)

    frequent_variant_df.loc[:,'frequent_in_our_cohort'] = 1
    dataframe = dataframe.set_index('merge_index')
    frequent_variant_df = frequent_variant_df.set_index('merge_index')
    result = dataframe.join(frequent_variant_df['frequent_in_our_cohort'], on='merge_index', how='left')
    result['frequent_in_our_cohort'] = result['frequent_in_our_cohort'].fillna(0).astype('int')

    return result


def frequent_in_other_cohort(dataframe, ch_eng):
    # check if the OTHER file exists!!
    if ch_eng == 0:
        nat = 'ENG'
    elif ch_eng == 1:
        nat = 'CH'
    file_name = nat + '-frquent_variants.txt'
    if os.path.exists(file_name) == False:
        text_exception = 'Execute damaging filter with the ' + nat + ' parameter first!'
        raise Exception(text_exception)

    frequent_variant_df = pd.DataFrame()
    with open(file_name, 'r') as f:
        for line in f:
            temp = dataframe.loc[dataframe['AA_or_SNP__Pos'] == line]
            frequent_variant_df = frequent_variant_df.append(temp, sort=False)

    frequent_variant_df.loc[:, 'frequent_in_other_cohort'] = 1
    result = dataframe.join(frequent_variant_df['frequent_in_other_cohort'], on='merge_index', how='left')
    result['frequent_in_other_cohort'] = result['frequent_in_other_cohort'].fillna(0).astype('int')
    return result


def df_cohort_homo_af(dataframe, cohort, homo, af):
    dataframe = dataframe[dataframe['frequent_in_our_cohort'] == cohort]
    dataframe = dataframe[dataframe['frequent_homo_hemi'] == homo]
    dataframe = dataframe[dataframe['frequent_in_AF'] == af]
    return dataframe


def compare_freq(dataframe, freq, ch_eng):
    nat = 'ch'
    nat_lable = 'Swiss'
    if ch_eng == 1:
        nat = 'eng'
        nat_lable = 'English'

    df = af_homo_cohort(dataframe=dataframe, freq=freq, ch_eng=ch_eng)
    df = df.drop_duplicates(subset=['AA_or_SNP__Pos'])

    df_cohort = df_cohort_homo_af(df, cohort=1, homo=0, af=0)
    df_cohort_af = df_cohort_homo_af(df, cohort=1, homo=0, af=1)
    df_af = df_cohort_homo_af(df, cohort=0, homo=0, af=1)
    df_homo = df_cohort_homo_af(df, cohort=0, homo=1, af=0)
    df_cohort_homo = df_cohort_homo_af(df, cohort=1, homo=1, af=0)
    df_af_homo = df_cohort_homo_af(df, cohort=0, homo=1, af=1)
    df_cohort_af_homo = df_cohort_homo_af(df, cohort=1, homo=1, af=1)

    # Custom text labels: change the label of group A
    v=venn3(subsets = (df_cohort.shape[0], df_af.shape[0], df_cohort_af.shape[0], 
                       df_homo.shape[0], df_cohort_homo.shape[0], df_af_homo.shape[0],
                       df_cohort_af_homo.shape[0]), set_labels = ((nat_lable + ' Cohort'), 'Allele frequency-GnomAD', 'Homozygosity/Hemizygosity-GnomAD'))
    plt.title('Too frequent occurence of each variant in different populations')
    path_plot = 'plot/recurring_variants_'+ nat + '.png'
    plt.savefig(path_plot, bbox_inches='tight')
    plt.clf()
    plt.close()
    return


if __name__ == "__main__":

    for i in range(2):
        ch_eng = i
        if ch_eng == 0:
            pickel_filename = 'true_calls.pkl'
        elif ch_eng == 1:
            pickel_filename = 'true_calls_eng.pkl'
        folder_path = '/home/selene/Programm/Diss'

        true_calls = pd.read_pickle(folder_path + '/' + pickel_filename)
        compare_freq(dataframe=true_calls, freq=0.004, ch_eng=ch_eng)

        
