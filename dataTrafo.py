import os
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
# import pylab


folder_location_full = "/home/selene/UNI/Ella/Mutation_Report5"
folder_location_full_eng = "/home/selene/UNI/Ella/1000_UK_exomes"

# Files need those columns to be complet
columns_train = ['Coverage',
                 'Alt#', 'Alt%', 'Allele Score', 'Overall Score',
                 'Read Balance(Counts)', 'Read Balance(Percentage)']

folder_location_testing = "/home/selene/UNI/Ella/Testing_Sample"


# check if the file is part of the data
def right_files(file):
    if file.endswith("_Mutation_Report5.txt"):
        return 1
    else:
        print(file)
        return


# check if there columns exist, which later will be neccessairy for training
def right_file_format(file_path, columns):
    # 5 files are in wrong format, which I am allowed to drop
    data = pd.read_csv(file_path, sep="\t", skiprows=4)
    for element in columns:
        if element not in data:
            return 0
    return 1


def get_mutation(file_path):
    data = pd.read_csv(file_path, sep="\t", skiprows=4)
    data.columns = data.columns.str.strip().str.replace(
        ' ', '_').str.replace(':', '')
    return data


# key to lable the patients with IDs (equals to the file name)
def get_key(file):
    name = file[:5]
    return int(name)


def get_key_eng(file):
    # example file name
    # EGAR00001237831_Mutation_Report5
    file = file.replace('EGAR0000', '')
    name = file[:7]
    return int(name)


def make_anonymous(file_path, ch_eng):
    file_name = os.path.basename(file_path)
    if ch_eng == 0:
        patient_id = get_key(file_name)
    if ch_eng == 1:
        patient_id = get_key_eng(file_name)
    data = get_mutation(file_path)
    num_rows = data.shape[0]
    patient_id_vector = np.ones(num_rows)*patient_id
    data['patient_id'] = patient_id_vector
    cols = data.columns.tolist()
    cols = cols[-1:] + cols[:-1]
    data = data[cols]
    return data


# Remove rows, discussed with Pascale, not important with new allignment
# def remove_row_pascale(data):
#     errors = ['NM_172240.2:c.453-11_453-10delCT',
#               'NM_015120.4:c.35_36insGGA,c.36delG']
#     for i in range(2):
#         data = data[data.Mutation_Call_HGVS_Coding != errors[i]]
#     return data


test_file_path = ("/home/selene/UNI/Ella/Mutation_Report5"
                  "27882_Mutation_Report5.txt")
test_file_name = os.path.basename(test_file_path)


def all_in_one(folder_path, columns, ch_eng):
    right_format = 0
    wrong_format = []
    too_many_calls = []
    complete_data = pd.DataFrame()
    for file in os.listdir(folder_path):
        if right_files(file) == 1:
            file_path = folder_path + "/" + file
            if right_file_format(file_path, columns) == 1:
                right_format = right_format + 1
                data = make_anonymous(file_path, ch_eng)
                if data.shape[0] < 150:
                    complete_data = complete_data.append(data)
                else:
                    too_many_calls.append(file)
            else:
                wrong_format.append(file)
    print("There are %d right formated reports" % right_format)
    if len(wrong_format) != 0:
        print(wrong_format)
    if ch_eng == 0:
        save_path = "/home/selene/Programm/Diss" + "/real_data_2.pkl"
    elif ch_eng == 1:
        save_path = "/home/selene/Programm/Diss" + "/real_data_2_eng.pkl"
    n = complete_data.shape[0]
    complete_data['merge_index'] = np.linspace(0, n-1, n, dtype='int')
    # complete_data.to_pickle(save_path)
    return complete_data


def make_plot(folder_path, columns, ch_eng):
    path = '/home/selene/Programm/Diss/plot/'
    shape_of_data = []
    for file in os.listdir(folder_path):
        if right_files(file) == 1:
            file_path = folder_path + "/" + file
            if right_file_format(file_path, columns) == 1:
                data = make_anonymous(file_path, ch_eng)
                shape_of_data.append(data.shape[0])
    hist = {}
    for i in shape_of_data:
        hist[i] = hist.get(i, 0) + 1
    plt.bar(x=list(hist.keys()), height=list(hist.values()))
    if ch_eng == 0:
        plt.title('Distribution of Variant Calls per Patients - Switzerland')
    elif ch_eng == 1:
        plt.title('Distribution of Variant Calls per Patients - England')

    plt.xlabel('Number of Variant Calls')
    plt.ylabel('Number of Patients')

    # pylab.show()
    if ch_eng == 0:
        path_plot = path + 'Calls_per_patient.png'
    elif ch_eng == 1:
        path_plot = path + 'Calls_per_patient_eng.png'
    plt.savefig(path_plot, bbox_inches='tight')
    plt.clf()
    return 0


if __name__ == '__main__':
    all_data = all_in_one(folder_location_full, columns_train, 0)
    all_data_eng = all_in_one(folder_location_full_eng, columns_train, 1)
    # make_plot(folder_location_full, columns_train, 0)
    # make_plot(folder_location_full_eng, columns_train, 1)

    print("All done :)")
    # print(all_data)
