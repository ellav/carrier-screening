import pandas as pd
from preprocess_true_calls import make_unique_col, ClinVar_cols
import numpy as np
import matplotlib.pyplot as plt
import matplotlib_venn
from matplotlib_venn import venn2, venn2_circles
from manual_cleaning import del_all
from predict_5 import preprocess_data_frame, PreprocessingOption


def load_data(ch_eng):
    folder_path = '/home/selene/Programm/Diss'
    if ch_eng == 0:
        data_name = 'real_data_2.pkl'
    elif ch_eng == 1:
        data_name = 'real_data_2_eng.pkl'
    all_calls = pd.read_pickle(folder_path + '/' + data_name)
    return all_calls


def add_columns_for_unique_col(df_original, df_new):
    result = df_new.join(df_original[['AA_or_SNP__Pos']], how='left')
    return result


def remove_false_calls(dataframe_original, cutoff, labled):

    def consider_cutoff(element, cutOff):
        if element > cutOff:
            return 1
        else:
            return 0

    if labled == PreprocessingOption.unlabeled_data:
        dataframe = preprocess_data_frame(dataframe_original, is_learning_data=labled, ch_eng=0)
        dataframe = make_unique_col(dataframe)
    else:
        dataframe = make_unique_col(dataframe_original)
        dataframe = preprocess_data_frame(dataframe, is_learning_data=PreprocessingOption.labeled_data, ch_eng=0)
        dataframe = add_columns_for_unique_col(dataframe_original, dataframe)

    df_for_alts = dataframe
    df_for_alts['Alt%'] = pd.to_numeric(df_for_alts['Alt%'], errors='coerce')
    df_for_alts = df_for_alts[['Alt%', 'AA_or_SNP__Pos']]


    alt_quants = df_for_alts.groupby('AA_or_SNP__Pos').quantile(.1)[['Alt%']]
    # alt_quants = df_for_alts.groupby('AA_or_SNP__Pos').mean()[['Alt%']]

    alt_quants = alt_quants.rename(columns={"Alt%" : "Alt_quantile"})
    dataframe = dataframe.set_index("AA_or_SNP__Pos")

    result = alt_quants.join(dataframe, on='AA_or_SNP__Pos', how='inner')
    result['predicted_true_para'] = result['Alt_quantile'].apply(consider_cutoff, cutOff=cutoff)
    result = result.reset_index()

    return result


def get_labeld_data(ch_eng):
    if ch_eng == 0:
        csv_data_training = 'alldata.csv'
        seperator = ','
    elif ch_eng == 1:
        csv_data_training = 'alldata_eng.csv'
        seperator = '\t'
    train_test_pd = pd.read_csv(csv_data_training, sep=seperator)
    train_test_pd.columns = (train_test_pd.columns.str.strip()
                                 .str.replace(' ', '_')
                                 .str.replace(':', ''))
    return train_test_pd


def get_sens_spez(dataframe_original, cutOff):
    # dataframe = make_unique_col(dataframe_original)
    # dataframe = preprocess_data_frame(dataframe, is_learning_data=PreprocessingOption.labeled_data, ch_eng=0)
    # dataframe = add_columns_for_unique_col(dataframe_original, dataframe)


    def predicted_vs_truth(dataframe, pred, truth):
        dataframe = dataframe[dataframe['predicted_true_para'] == pred]
        dataframe = dataframe[dataframe['Truth'] == truth]
        return dataframe.shape[0]

    df_with_predic = remove_false_calls(dataframe_original, cutoff=cutOff, labled=PreprocessingOption.labeled_data)
    
    
    true_positive = predicted_vs_truth(df_with_predic, pred=1, truth=1)
    true_negative = predicted_vs_truth(df_with_predic, pred=0, truth=0)
    false_positive = predicted_vs_truth(df_with_predic, pred=1, truth=0)
    false_negative = predicted_vs_truth(df_with_predic, pred=0, truth=1)

    sens = float(true_positive/(true_positive + false_negative))
    spez = float(true_negative/(true_negative + false_positive))

    return sens, spez


def plot_sens_spez(dataframe):
    paras_value = 28
    plt.clf()
    cutOff_list = np.linspace(25, 60, (60-25+1))
    cutOff_list = np.concatenate((cutOff_list, [paras_value]))
    sens_list = []
    spez_1_list = []
    random = np.linspace(0, 1, 100)
    spezial_sens = 0
    spezial_spez_1 = 0
    for cutOff in cutOff_list:
        print(cutOff)
        sens, spez = get_sens_spez(dataframe, cutOff)
        sens_list.append(sens)
        spez_1_list.append(float(1-spez))
        if cutOff == paras_value:
            spezial_sens = sens
            spezial_spez_1 = 1-spez


    plt.scatter(spez_1_list, sens_list, label='Paras Modell with different Alts')
    plt.scatter(random, random, label='Random choice')
    plt.scatter(spezial_spez_1, spezial_sens, color='red', label='Paras Choice')
    plt.scatter((1-0.987), 0.947, color='black', label = 'Training')
    plt.scatter((1-0.971), 1.0, color='green', label = 'Testing')
    plt.xlabel('1-Spezifität')
    plt.ylabel('Sensivität')
    plt.legend()
    plt.xlim(0,1.1)
    plt.ylim(0,1.1)
    plt.title("ROC curve")
    path_plot = 'plot/ROC.png'
    plt.savefig(path_plot, bbox_inches='tight')

    # plt.show()
    plt.clf()
    plt.close()
    return


def compare_paras_true_calls(all_calls):
    dataframe_para_all = remove_false_calls(all_calls, cutoff=29, labled=PreprocessingOption.unlabeled_data)
    dataframe_para = dataframe_para_all[dataframe_para_all['predicted_true_para'] == 1]
    pickel_filename = 'true_calls.pkl'
    folder_path = '/home/selene/Programm/Diss'
    dataframe_our = pd.read_pickle(folder_path + '/' + pickel_filename)

    dataframe_our['predicted_true_our'] = 1

    dataframe_our = dataframe_our.set_index('merge_index')
    dataframe_para_all = dataframe_para_all.set_index('merge_index')
    columns_to_add = list(set(list(dataframe_our)) - set(list(dataframe_para_all)))
    
    result = dataframe_para_all.join(dataframe_our[columns_to_add], on='merge_index', how='left')
    result['predicted_true_our'] = result['predicted_true_our'].fillna(0)

    # generate different df for venn diagramm and spreadsheet
    only_in_our_model = result[result['predicted_true_our'] == 1]
    only_in_our_model = only_in_our_model[only_in_our_model['predicted_true_para'] == 0]

    only_in_paras_model = result[result['predicted_true_our'] == 0]
    only_in_paras_model = only_in_paras_model[only_in_paras_model['predicted_true_para'] == 1]

    in_both_models = result[result['predicted_true_our'] == 1]
    in_both_models = in_both_models[in_both_models['predicted_true_para'] == 1]

    # make venn diagramm
    v = venn2(subsets=(only_in_paras_model.shape[0],only_in_our_model.shape[0], in_both_models.shape[0]), 
              set_labels= ('Paras Model-Fix Alt% over 29', 'Our Model-SVM'))
    plt.title("Compare the modells")
    path_plot = 'plot/venn_diagram.png'
    plt.savefig(path_plot, bbox_inches='tight')

    # save spreadsheets
    file_name_same = 'spreadsheet/all_calls_both_models.csv'
    result.to_csv(file_name_same, sep='\t')
    file_name_only_our = 'spreadsheet/true_calls_only_in_our_model.csv'
    only_in_our_model.to_csv(file_name_only_our, sep='\t')
    file_name_same = 'spreadsheet/true_calls_in_both_model.csv'
    in_both_models.to_csv(file_name_same, sep='\t')

    return


def spreadsheet_with_missense():
    pickel_filename = 'true_calls.pkl'
    folder_path = '/home/selene/Programm/Diss'
    dataframe_our = pd.read_pickle(folder_path + '/' + pickel_filename)
    dataframe_our_no_benign = dataframe_our[dataframe_our['ClinVar_red'] == 0]
    no_benign_missense = dataframe_our_no_benign[dataframe_our_no_benign['Function'] == 'Missense']
    file_name_missense = 'spreadsheet/true_calls_missense_without_Clinvar_ben.csv'
    no_benign_missense.to_csv(file_name_missense, sep='\t')
    return


if __name__ == '__main__':
    ch_eng = 0
    all_calls = load_data(ch_eng)
    pickel_filename = 'true_calls.pkl'
    folder_path = '/home/selene/Programm/Diss'
    dataframe_our = pd.read_pickle(folder_path + '/' + pickel_filename)


    labeld_calls = get_labeld_data(ch_eng)
    plot_sens_spez(labeld_calls)

    compare_paras_true_calls(all_calls)
    spreadsheet_with_missense()
