import split_by_disorder as split
import seaborn as sns
import matplotlib.pyplot as plt
import pandas as pd
from class_def import Parameter
from variant_filter import damaging_variant
from scipy.stats import (linregress, spearmanr, normaltest)
import numpy as np


folder_path = '/home/selene/Programm/Diss'


def disease_heatmap():
    (disorder, gene_disorder_pd) = split.get_disorder_list()

    gene_disorder_pd = gene_disorder_pd.set_index('gene_used')
    gene_disorder_pd.index.names = [None]
    gene_disorder_pd.loc[:,'Meckel_syndrome'] = gene_disorder_pd.loc[:, 'Meckel_syndrome'].str.replace(' ', '0')
    gene_disorder_pd.loc[:,'Meckel_syndrome'] = gene_disorder_pd.loc[:, 'Meckel_syndrome'].astype(float)

    gene_disorder_pd = gene_disorder_pd.fillna(0)

    # fig, (ax, ax2) = plt.subplots(2, 1)
    plt.figure(figsize=(20,5))
    sns.set(font_scale=1)
    ax = sns.heatmap(gene_disorder_pd.T, annot=False, yticklabels=True, xticklabels=True, cmap='terrain', linewidths=.5, linecolor='silver')
    # ax2 = sns.clustermap(gene_disorder_pd.T)

    plt.savefig(folder_path + '/plot/gene_disease_heatmap.png', bbox_inches='tight')
    # plt.show()

    plt.close()
    plt.clf()
    return


def find_difference_in_gene_list(dataframe):
    filter_list = pd.read_csv('Cilia_filter.txt', sep='; ')
    variant_list = dataframe['Gene'].unique()
    difference = set(filter_list).symmetric_difference(set(variant_list))
    # print(difference)
    diff1 = set(variant_list) - set(filter_list)
    print(list(diff1))
    diff2 = set(filter_list) - set(variant_list)
    # print(diff2)
    return



if __name__=="__main__":

    ch_eng = 0
    if ch_eng == 0:
        pickel_filename = 'true_calls.pkl'
    elif ch_eng == 1:
        pickel_filename = 'true_calls_eng.pkl'
    folder_path = '/home/selene/Programm/Diss'
    true_calls = pd.read_pickle(folder_path + '/' + pickel_filename)

    
    (disorder, gene_disorder_pd) = split.get_disorder_list()
    param = Parameter(0.004, 200, 200)
    damaging = damaging_variant(true_calls, param)
    # find_difference_in_gene_list(true_calls)
    # find_difference_in_gene_list(damaging)

    disease_heatmap()
