class Parameter:
    def __init__(self, f, cadd, n, ch_eng=0, dis=0, acmg_only=0):
        self.f = f
        self.cadd = cadd
        self.n = n
        self.dis = dis
        self.ch_eng = ch_eng
        self.acmg_only = acmg_only


    def print_value(self):
        if self.ch_eng == 0:
            ch_eng = 'CH'
        elif self.ch_eng == 1:
            ch_eng = 'ENG'
        if self.dis == 0:
            dis = 'Ciliopathy'
        elif self.dis != 0:
            dis = self.dis
        if self.acmg_only == 0:
            acmg = 'All'
        elif self.acmg_only == 1:
            acmg = 'ACMG'
        print(self.f, self.cadd, self.n, ch_eng, dis, acmg)


    def path_generall(self):
        path_end = str(self.f) + '_' + str(self.cadd) + '_' + str(self.n)

        if self.acmg_only == 1:
            path_end = 'ACMG_' + path_end

        return path_end


    def path_spreadsheet(self, name):
        if self.ch_eng == 0:
            begin = 'spreadsheet/'
        elif self.ch_eng == 1:
            begin = 'spreadsheet_eng/'

        if self.dis == 0:
            dis = 'Ciliopathy'
        else:
            dis = str(self.dis)

        result = begin + dis + name + self.path_generall() + '.csv'
        return result


    def return_path_plot(self, name):
        if self.ch_eng == 0:
            begin = 'plot/'
        elif self.ch_eng == 1:
            begin = 'plot_eng/'

        if self.dis != 0:
            begin = begin + str(self.dis) + '/'

        result = begin + self.path_generall() + '_' + name + '.png'
        return result
