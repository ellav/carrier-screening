import pandas as pd
from scipy import stats
from variant_filter import damaging_variant
from class_def import Parameter
# from scipy.stats import (linregress, spearmanr, normaltest)


def stats_tests(ch, eng):
    k2, p = stats.normaltest(ch)
    print("Ch p = {:g}".format(p))
    k2, p = stats.normaltest(eng)
    print("Eng p = {:g}".format(p))
    k2, p = stats.mannwhitneyu(ch, eng)
    print(p)
    return


def compare_stuff(calls_ch, calls_eng, stuff):
    print(stuff)
    cov_ch = calls_ch[stuff]
    cov_eng = calls_eng[stuff]
    stats_tests(ch=cov_ch, eng=cov_eng)
    return


def compare_nr_varints(calls_ch, calls_eng):
    print('Number of variants per patient')
    def count_nr(element):
        return element.shape[0]

    def nr_of_variants(dataframe):
        nr_var = dataframe.groupby('patient_id').apply(count_nr)
        nr_var = nr_var.to_frame()
        nr_var = nr_var.reset_index().rename(columns={0:'Nr_variants'})
        return nr_var['Nr_variants']

    nr_var_ch = nr_of_variants(calls_ch)
    nr_var_eng = nr_of_variants(calls_eng)

    stats_tests(ch=nr_var_ch, eng=nr_var_eng)

    # stats_tests(ch=cov_ch, eng=cov_eng)
    return



if __name__ == '__main__':
    
    pickel_filename_ch = 'true_calls.pkl'
    pickel_filename_eng = 'true_calls_eng.pkl'
    folder_path = '/home/selene/Programm/Diss'
    true_call_ch = pd.read_pickle(folder_path + '/' + pickel_filename_ch)
    true_call_eng = pd.read_pickle(folder_path + '/' + pickel_filename_eng)
    compare_stuff(true_call_ch, true_call_eng, 'Coverage')
    compare_stuff(true_call_ch, true_call_eng, 'Overall_Score')
    compare_nr_varints(true_call_ch, true_call_eng)
    print('Damaging')
    for i in range(2):
        acmg_only = i
        param_ch = Parameter(0.004, 500, 500, dis=0, ch_eng = 0, acmg_only=acmg_only)
        param_eng = Parameter(0.004, 500, 500, dis=0, ch_eng = 1, acmg_only=acmg_only)
        damaging_ch = damaging_variant(true_call_ch, param_ch)
        damaging_eng = damaging_variant(true_call_eng, param_eng)
        compare_nr_varints(damaging_ch, damaging_eng)

