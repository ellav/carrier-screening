import matplotlib.pyplot as plt
import numpy as np
import seaborn as sns
import pandas as pd
# from sklearn import svm


def make_meshgrid(x, y, inv_const):
    if inv_const == 0:
        h = 0.2
        mar = 10
    elif inv_const == 1:
        h = 0.0005
        mar = 0.01
    # x_min, x_max = float(x.min() - mar), float(x.max() + mar)
    x_min = -0.02
    x_max = 0.12
    y_min = 0
    y_max = 0.07
    # y_min, y_max = float(y.min() - mar), float(y.max() + mar)
    xx, yy = np.meshgrid(
        np.arange(x_min, x_max, h), np.arange(y_min, y_max, h))
    return xx, yy


def plot_contours(ax, clf, xx, yy, **params):
    # Z = clf.predict(np.c_[1/xx.ravel(), 1/yy.ravel()])
    Z = clf.predict(np.c_[xx.ravel(), yy.ravel()])
    Z = Z.reshape(xx.shape)
    out = ax.contourf(xx, yy, Z, **params)
    return out


def plot_svm(x_test, y_test, clf, inv_const, data_type, c, ch_eng):
    # Select 2 features / variable for the 2D plot that we are going to create.
    X = x_test
    y = y_test
    # model = svm.LinearSVC(C=c, class_weight='balanced', max_iter=10000000)
    # clf = model.fit(X, y)
    # X = 1/X

    fig, ax = plt.subplots()
    # title for the plots
    title = 'Decision surface of linear SVM '
    if inv_const == 1:
        title = title + '- inverse features '
    title = title + '(' + data_type + ' data)'

    # Set-up grid for plotting.
    X0, X1 = X[:, 0], X[:, 1]
    xx, yy = make_meshgrid(X0, X1, inv_const)

    # y = clf.predict(X)
    y = y_test

    plot_contours(ax, clf, xx, yy, cmap=plt.cm.coolwarm, alpha=0.8)
    ax.scatter(X0, X1, c=y, cmap=plt.cm.coolwarm, s=20)
    plt.xlim((-0.02, 0.12))
    if inv_const == 0:
        ax.set_xlabel('Coverage')
        ax.set_ylabel('Alt% [%]')
    elif inv_const == 1:
        ax.set_xlabel('1/Coverage')
        ax.set_ylabel('1/Alt% [1/%]')
    ax.get_xticks(())
    ax.get_yticks(())
    ax.set_title(title)

    # plt.show()

    # Save plot
    path = '/home/selene/Programm/Diss/plot/'
    name = 'LinearSVC_'
    if inv_const == 1:
        name = name + 'inv_'
    name = name + data_type + '_C_'
    if ch_eng == 1:
        name = 'eng_' + name

    path_plot = path + name + str(c) + '.png'
    plt.savefig(path_plot, bbox_inches='tight')

    # Cleare current plot
    plt.close()
    return


def plot_all_data(x_test, y_test, clf, inv_const, data_type, c, ch_eng):
    # Select 2 features / variable for the 2D plot that we are going to create.
    df = pd.DataFrame(x_test, columns=["1/Coverage", "1/Alt%"])
    g = sns.jointplot(x="1/Coverage", y="1/Alt%", data=df, kind="kde", color="m")
    g.plot_joint(plt.scatter, c="w", s=30, linewidth=1, marker="+")
    g.ax_joint.collections[0].set_alpha(0)
    g.set_axis_labels("$X$", "$Y$")
    return

    # get correct data
    # mar = 0.005
    # x_min, x_max = float(df['1/Coverage'].min() - mar), float(df['1/Coverage'].max() + mar)
    # y_min, y_max = float(df['1/Alt%'].min() - mar), float(df['1/Alt%'].max() + mar)
    # plt.xlim((x_min, x_max))
    # plt.ylim((y_min, y_max))

    # set limits
    # plt.xlim((-0.005, 0.02))
    # plt.ylim((0.0125, 0.03))
    X = x_test
    plt.show()

    fig, ax = plt.subplots()
    # title for the plots
    title = 'Decision surface of linear SVM '
    if inv_const == 1:
        title = title + '- inverse features '
    title = title + '(' + data_type + ' data)'

    # Set-up grid for plotting.
    xx, yy = make_meshgrid(x=0, y=0, inv_const=inv_const)

    # y = clf.predict(X)
    y = y_test

    plot_contours(ax, clf, xx, yy, cmap=plt.cm.coolwarm, alpha=0.8)
    # sns.jointplot(x="1/Coverage", y="1/Alt%", data=df)


    # plt.show()

    # Cleare current plot
    plt.close()
    return
