import enum
from manual_cleaning import del_all
import numpy as np
from plot_svm import plot_svm
import pandas as pd
from sklearn import svm
from preprocess_true_calls import plot_num_of_variant
# from sys import exit

# New: is able to process the data with all the columns
#      but only looks at two....

cutOff = 0.8  # how to split the test and train data


class PreprocessingOption(enum.Enum):
    drop_row = 0
    drop_feature = 1
    convert_to_mean = 2
    drop_bad_features = 3


preprocessingType = PreprocessingOption.drop_bad_features


csv_data_training = 'alldata.csv'  # the train/ test data
csv_data_training_eng = 'alldata_eng.csv'
folder_path = '/home/selene/Programm/Diss'
real_data = 'real_data_2.pkl'
real_data_eng = 'real_data_2_eng.pkl'


# Get the features for training and testing
train_test_Pandas = pd.read_csv(csv_data_training, sep=',')
train_test_Pandas.columns = (train_test_Pandas.columns.str.strip()
                             .str.replace(' ', '_')
                             .str.replace(':', ''))

train_test_Pandas_eng = pd.read_csv(csv_data_training_eng, sep='\t')
train_test_Pandas_eng.columns = (train_test_Pandas_eng.columns.str.strip()
                                 .str.replace(' ', '_')
                                 .str.replace(':', ''))

all_data_eng_pd = pd.read_pickle(folder_path + '/' + real_data_eng)

# ##################################
# get validation data

csv_validation = 'validation_data.csv'

validation_Pandas = pd.read_csv(csv_validation, sep='\t')

validation_Pandas.columns = (validation_Pandas.columns.str.strip()
                             .str.replace('Pat', 'patient_id')
                             .str.replace(' ', '_')
                             .str.replace(':', ''))


# ##################################
# Test
def check_column(df1, df2):
    res1 = df1.columns.difference(df2.columns)
    res2 = df2.columns.difference(df1.columns)
    res = res1.shape[0] + res2.shape[0]
    if res:
        print(res1)
        print(res2)
    return not res


def check_for_string(dataframe):
    orig = dataframe
    for column in dataframe:
        c1_as_float = pd.to_numeric(orig[column], errors='coerce')
        print(orig[column][c1_as_float.isna()])
# ##################################


# Magic Value 0
def drop_nan_strings(dataframe):
    dataframe = dataframe[dataframe.apply(
        pd.to_numeric, errors='coerce').notna()]
    dataframe = dataframe.dropna(axis=0)
    np_dataform = dataframe.values
    return (np_dataform, dataframe)


# Magic Value 1
# drop each feature, which has missing values
# ToDo, so far not working :(
def drop_feature(dataframe_train, dataframe_all):
    # Truth = dataframe_train['Truth']
    dataframe_train_merge = dataframe_train.drop(columns='Truth')
    dataframe_merge = dataframe_train_merge.append(dataframe_all)
    print(dataframe_merge)
    print(dataframe_merge.isnull().sum())
    dataframe_merge = dataframe_merge.dropna(axis=1)
    print("##########################")
    print(dataframe_merge)
    return (dataframe_merge, dataframe_merge)


# Magic Value 2
def NaN_to_mean(dataframe_train, dataframe_all):
    dataframe_train_merge = dataframe_train.drop(columns='Truth')
    dataframe_train = dataframe_train.apply(pd.to_numeric, errors='coerce')
    dataframe_all = dataframe_all.apply(pd.to_numeric, errors='coerce')
    dataframe_merge = dataframe_train_merge.append(dataframe_all)

    for column in dataframe_merge:
        mean = np.nanmean(dataframe_merge[column])
        dataframe_train[column] = dataframe_train[column].fillna(mean)
        dataframe_all[column] = dataframe_all[column].fillna(mean)
    return (dataframe_all, dataframe_train)


# Magic Value 3
def drop_nan_column_in_learning_set(
        dataframe_learning, dataframe_all, dataframe_val):
    dataframe_learning_preprocessed = preprocess_data_frame(
            dataframe_learning, 1)
    dataframe_all_preprocessed = preprocess_data_frame(dataframe_all, 0)
    all_column = preprocess_data_frame(dataframe_all, 2)
    dataframe_val_preprocessed = preprocess_data_frame(dataframe_val, 1)
    check_column(dataframe_learning_preprocessed.drop(columns='Truth'),
                 dataframe_all_preprocessed)
    return (dataframe_all_preprocessed, dataframe_learning_preprocessed,
            all_column, dataframe_val_preprocessed)


def preprocess_data_frame(dataframe, is_learning_data):
    # delete data as disscussed with Pascale
    dataframe = del_all(dataframe)
    if is_learning_data != 2:
        # delete patient_id and Mutation_Call_HGVS_Coding
        dataframe = dataframe.drop(columns=[
            'patient_id', 'Mutation_Call_HGVS_Coding'])
    # for learning data drop all NaN
    if is_learning_data == 0:
        # for all the data drop same features as learning data
        # AND drop patient of the remaining NaN
        list_column_to_drop = [column for column in dataframe
                               if int(dataframe[column].isna().sum()) > 5]
        dataframe = dataframe.drop(columns=list_column_to_drop)
        print(dataframe[dataframe.isna().any(axis=1)])
        dataframe_res = dataframe.dropna(axis=0)
        # drop the row with the strings in Allele Score, Alt#, Alt%
        print("Excluding this")
        print(dataframe_res[
            dataframe_res['Allele_Score'].str.contains(
                '24.37,24.05', na=False)])
        dataframe_first_clean = dataframe_res[
            ~dataframe_res.Allele_Score.str.contains('24.37,24.05', na=False)]
        dataframe_end = dataframe_first_clean[dataframe_first_clean.apply(
                            pd.to_numeric, errors='coerce').notna()]
        dataframe_end = dataframe_end.dropna(axis=1)
    elif is_learning_data == 1:
        dataframe_res = dataframe.dropna(axis=1)
        dataframe_end = dataframe_res[dataframe_res.apply(
                            pd.to_numeric, errors='coerce').notna()]
        dataframe_end = dataframe_end.dropna(axis=1)
    else:
        # find the 5 rows which have been deleted before
        # without loosing any columns!
        list_column_to_drop = [column for column in dataframe
                               if not int(dataframe[column].isna().sum()) > 5]
        dataframe_res = dataframe.dropna(subset=list_column_to_drop)
        # delete the row with the string in the learning columns
        dataframe_first_clean = dataframe_res[
            ~dataframe_res.Allele_Score.str.contains('24.37,24.05', na=False)]
        dataframe_end = dataframe_first_clean
    dataframe_end.loc[:, 'Alt%'] = pd.to_numeric(dataframe_end['Alt%'])
    dataframe_end.loc[:, 'Allele_Score'] = pd.to_numeric(
        dataframe_end['Allele_Score'])

    print('Passing pre process: %i'  % dataframe_end.shape[0])
    return dataframe_end


def keep_Alt_Coverage(dataframe, is_learning):
    dataframe = del_all(dataframe)
    if is_learning == 0:
        result = dataframe.dropna(subset=['Coverage', 'Alt%'])
        # delete row with '36.00,20.00' as Alt%
        result = result.loc[result['Pos'] != 76895789.0]
        result.loc[:, 'Alt%'] = pd.to_numeric(result['Alt%'])
        result.loc[:, 'Coverage'] = pd.to_numeric(result['Coverage'])

    if is_learning == 1:
        result = dataframe[['Truth', 'Coverage', 'Alt%']]
        print(result.isna().sum())
    return result


def get_train_test_data(np_dataform, cutOff):
    # np_dataform has no NaN and no strings
    cutOffInt = int(np_dataform.shape[0]*cutOff)
    train = np_dataform[0:cutOffInt, :]
    test = np_dataform[cutOffInt:np_dataform.shape[0], :]
    return (train, test)


def get_wrong_labled_points(dataframe, prediction):
    print('------------------------------')
    dataframe['pred'] = prediction
    res = dataframe[dataframe['pred'] != dataframe['Truth']]
    if res.shape[0] == 0:
        print('No wrong labled points')
        return
    print(res)
    return


def get_sens_spez(y_pred, y):
    d = {'Truth': y, 'predicted_true': y_pred}
    df_with_predic = pd.DataFrame(data = d)

    def predicted_vs_truth(dataframe, pred, truth):
        dataframe = dataframe[dataframe['predicted_true'] == pred]
        dataframe = dataframe[dataframe['Truth'] == truth]
        return dataframe.shape[0]

    # df_with_predic = add_predicted_true_to_df(dataframe, cutOff)
    
    true_positive = predicted_vs_truth(df_with_predic, 1, 1)
    true_negative = predicted_vs_truth(df_with_predic, 0, 0)
    false_positive = predicted_vs_truth(df_with_predic, 1, 0)
    false_negative = predicted_vs_truth(df_with_predic, 0, 1)

    sens = float(true_positive/(true_positive + false_negative))
    spez = float(true_negative/(true_negative + false_positive))
    spez_1 = float(1-spez)
    print('sens %f' % sens)
    print('1-spez %f' % spez_1)

    return sens, spez


def train_model(data_pd, val_pd, cutOff, inv_const, ch_eng):
    # data format:  first train then test data point
    #               the first column is the truth
    #               data has no NaN and no strings
    #               Pandas DataFrame
    #
    # get the train and test set, split y fromm the data

    data = data_pd[['Truth', 'Coverage', 'Alt%']].values
    data_val = val_pd[['Truth', 'Coverage', 'Alt%']].values
    if inv_const == 1:
        power_vec = [1, -1, -1]
        data = np.power(data, power_vec)
        data_val = np.power(data_val, power_vec)
    (train, test) = get_train_test_data(data, cutOff)
    print('-----')
    print(ch_eng)
    print(train.shape)
    print(test.shape)
    print('----')

    y_train = train[:, 0]
    x_train = train[:, 1:]

    y_test = test[:, 0]
    x_test = test[:, 1:]

    y_val = data_val[:, 0]
    x_val = data_val[:, 1:]

    # train the model
    # parameters = {'C': [0.1, 0,5, 1]}
    c = 100000
    clf = svm.LinearSVC(C=c, class_weight='balanced', max_iter=10000000)
    clf.fit(x_train, y_train)
    print("Coef")

    # find the values which are predicted wrong
    y_predict_train = clf.predict(x_train)
    y_predict_test = clf.predict(x_test)
    y_predict_val = clf.predict(x_val)
    print('Training')
    get_sens_spez(y_pred=y_predict_train, y=y_train)
    print('Test')
    get_sens_spez(y_pred=y_predict_test, y=y_test)
    print('Validation')
    get_sens_spez(y_pred=y_predict_val, y=y_val)
    print('Test + Validation')
    get_sens_spez(y_pred=np.append(y_predict_test, y_predict_val), y=np.append(y_test, y_val))
    for i in range(len(y_predict_train)):
        if y_train[i] != y_predict_train[i]:
            print(x_train[i, :])

    print("Percentage of true Calls")
    print(sum(y_predict_train)/y_predict_train.shape[0])
    diff1 = y_train - y_predict_train
    diff_abs1 = np.absolute(diff1)
    diff_sum1 = np.sum(diff_abs1)

    diff2 = y_test - y_predict_test
    diff_abs2 = np.absolute(diff2)
    diff_sum2 = np.sum(diff_abs2)

    diff3 = y_val - y_predict_val
    diff_abs3 = np.absolute(diff3)
    diff_sum3 = np.sum(diff_abs3)
    errors = y_train.shape[0]-np.sum(y_train)
    print("there are %d of false mutations call in y_train" % errors)
    print(np.sum(diff1))
    print("Fales prediction in train set = %d" % diff_sum1)
    print("Fales prediction in test set = %d" % diff_sum2)
    print("Fales prediction in validation set = %d" % diff_sum3)

    # val_pd['difference'] = diff_abs3
    # print(val_pd)
    # exit(0)

    # is_train = 0 with testing data,
    # is_train = 1 with training data,
    # is_train = 2 with validation data
    plot_svm(x_train, y_train, clf, inv_const, 'train', c, ch_eng)
    plot_svm(x_test, y_test, clf, inv_const, 'test', c, ch_eng)
    plot_svm(x_val, y_val, clf, inv_const, 'validation', c, ch_eng)

    get_wrong_labled_points(data_pd, np.append(y_predict_train, y_predict_test))
    get_wrong_labled_points(val_pd, y_predict_val)
    return clf


def final_function(magic_value, trainingdata_pd,
                   data_filename_all, validationdata_pd, cutOff,
                   trainingdata_pd_eng):
    # Inv model =1, normal model = 0
    inv_mod = 1
    print('Swiss#####################3')

    data_raw_pd_all = pd.read_pickle(folder_path + '/' + data_filename_all)
    print('=================')
    print(data_raw_pd_all.shape)

    if magic_value == PreprocessingOption.drop_row:
        (np_data, pd_data) = drop_nan_strings(trainingdata_pd)
        (np_data_all, pd_data_all) = drop_nan_strings(data_raw_pd_all)
    elif magic_value == PreprocessingOption.drop_feature:
        (pd_data_all, pd_data) = drop_feature(trainingdata_pd, data_raw_pd_all)
        print(pd_data_all)
    elif magic_value == PreprocessingOption.convert_to_mean:
        (pd_data_all, pd_data) = NaN_to_mean(trainingdata_pd, data_raw_pd_all)
    elif magic_value == PreprocessingOption.drop_bad_features:
        (pd_data_all, pd_data,
         pd_data_all_column,
         pd_data_validation) = drop_nan_column_in_learning_set(
            trainingdata_pd, data_raw_pd_all, validationdata_pd)

    if not check_column(pd_data.drop(columns='Truth'), pd_data_all):
        raise ValueError('Columns do not match')

    clf = train_model(pd_data, pd_data_validation, cutOff, inv_mod, 0)

    np_data_all = pd_data_all[['Coverage', 'Alt%']].values
    if inv_mod == 1:
        np_data_all = np.power(np_data_all, [-1, -1])

    y_data_all = clf.predict(np_data_all)
    print("Percentage of true Calls")
    print(sum(y_data_all)/y_data_all.shape[0])

    # plot_svm(np_data_all, y_data_all, clf, inv_mod, 3, 100000, 0)

    # return the data with all the columns
    end_data = pd_data_all_column.assign(prediction=y_data_all)
    return end_data


def final_function_eng(trainingdata_pd, data_raw_pd_all, cutOff):
    # Inv model =1, normal model = 0
    inv_mod = 1
    print('English#######################')

    pd_data_all = keep_Alt_Coverage(data_raw_pd_all, 0)

    pd_data = keep_Alt_Coverage(trainingdata_pd, 1)

    clf = train_model(pd_data, pd_data, cutOff, inv_mod, 1)

    np_data_all = pd_data_all[['Coverage', 'Alt%']].values
    if inv_mod == 1:
        np_data_all = np.power(np_data_all, [-1, -1])

    y_data_all = clf.predict(np_data_all)
    print("Percentage of true Calls")
    print(sum(y_data_all)/y_data_all.shape[0])
    end_result = pd_data_all.assign(prediction=y_data_all)
    print(end_result)

    return end_result



final_matrix = final_function(preprocessingType,
                              train_test_Pandas, real_data,
                              validation_Pandas, cutOff, train_test_Pandas_eng)

final_matrix_eng = final_function_eng(
    train_test_Pandas_eng, all_data_eng_pd, cutOff)


true_calls = final_matrix.loc[final_matrix['prediction'] == 1]
true_calls_eng = final_matrix_eng.loc[final_matrix_eng['prediction'] == 1]

false_calls = final_matrix.loc[final_matrix['prediction'] == 0]
false_calls_eng = final_matrix_eng.loc[final_matrix_eng['prediction'] == 0]
print('False Calls')
print(false_calls.shape[0])
print('True Calls')
print(true_calls.shape[0])


# ################################################
# only save, if changing something with the clf!

save_path = "/home/selene/Programm/Diss" + "/pred_true_calls.pkl"
# true_calls.to_pickle(save_path)

save_path_eng = "/home/selene/Programm/Diss" + "/pred_true_calls_eng.pkl"
# true_calls_eng.to_pickle(save_path_eng)

save_path_false = "/home/selene/Programm/Diss/spreadsheet_excluded" + "/pred_false_calls.csv"
# false_calls.to_csv(save_path_false, sep='\t')

save_path_false_eng = "/home/selene/Programm/Diss/spreadsheet_excluded" + "/pred_false_calls_eng.csv"
# false_calls_eng.to_csv(save_path_false_eng, sep='\t')
