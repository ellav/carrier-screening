import os



def find_substring(start, end, s):
    if start not in s or end not in s:
        return ''
    result = s[s.find(start)+len(start):s.rfind(end)]
    print(result)
    return result

def open_logfile_return_coverage(filname):
    f = open(os.path.join(filname), "r")
    start = 'Percent of ROI with > 20x coverage\t'
    end = '%'
    # print(f.read())
    for line in f:
        res = find_substring(start, end, line)
    return float(res)


def get_array_of_coverage():
    folder_path = '/home/selene/Programm/Diss/plot/PostProcessLog/'
    result_coverage = []

    for file in os.listdir(folder_path):
        file_path = folder_path + "/" + file
        coverage = open_logfile_return_coverage(file_path)
        result_coverage.append(coverage)
    print(result_coverage)
    return



if __name__=="__main__":
    # location = '/home/selene/Programm/Diss/plot/PostProcessLog/'
    # file = location + 'PostProcessingTables.log'
    # open_logfile(file)
    # start = 'Percent of ROI with > 20x coverage'
    # end = 'Number of Bases in ROI'
    get_array_of_coverage()