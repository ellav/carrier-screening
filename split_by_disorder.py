import pandas as pd
import numpy as np
from sys import exit
import os
from class_def import Parameter

# pickel_filename = 'true_calls.pkl'
# folder_path = '/home/selene/Programm/Diss'
# true_calls = pd.read_pickle(folder_path + '/' + pickel_filename)


def gene_to_csv(dataframe):
    path = '/home/selene/Programm/Diss/' + 'Gene_name.csv'
    col = dataframe['Gene'].unique()
    pd.DataFrame(col).to_csv(path)
    return

def csv_to_pd(file_csv):
    file = pd.read_csv(file_csv, sep=',')
    file.columns = (file.columns.str.strip()
                             .str.replace(' ', '_')
                             .str.replace(':', '')
                             .str.replace('/', '_'))
    return file


def get_disorder_list():
    csv_file_with_disorder = 'Gene_with_disorder.csv'
    gene_disorder_pd = csv_to_pd(csv_file_with_disorder)
    gene_disorder_pd = gene_disorder_pd.drop(['Genes', 'Unnamed_1',
                                              'A_in_right_gene',
                                              'B_in_right_gene'], axis=1)
    # Drop first row with contains total, and drop FAM161A
    # which was a duplicated row
    gene_disorder_pd = gene_disorder_pd.drop([0, 74], axis=0)
    disorder = list(gene_disorder_pd)
    return (disorder[1:], gene_disorder_pd)


def affected_gene_per_disorder(gene_disorder_pd, disorder):
    # returns the affected gene for a disease
    gene_disorder_pd = gene_disorder_pd.dropna(subset=[disorder], axis=0)
    affected_gene = gene_disorder_pd['gene_used']
    return affected_gene


def df_for_disorder(dataframe, affected_gene):
    # returns DataFrame which only contains the affected gene
    result = pd.DataFrame()
    for gene in affected_gene:
        contain_gene = dataframe.loc[dataframe['Gene'] == gene]
        result = result.append(contain_gene)
    return result


def make_tabel_entry(res, param):
    tabel = pd.DataFrame(columns=['Carrier_rate', 'AF', 'CADD', 'n', 'disease'])
    df = pd.Series({'Carrier_rate':res, 'AF':param.f, 'CADD':param.cadd, 'n':param.n, 'disease':param.dis})
    tabel = tabel.append(df, ignore_index=True)
    return tabel


def find_carrier_rate(dataframe, param):
    dataframe = dataframe.loc[dataframe['AF'] == param.f]
    dataframe = dataframe.loc[dataframe['CADD'] == param.cadd]
    dataframe = dataframe.loc[dataframe['n'] == param.n]
    print(dataframe)
    return dataframe



#   ,  'Marden-Walker', ,
# ', ',  '',
# ]

# Short polydaktyply, Jeune, Joubert, Mekel, OFD, Hydrolethalis, Nephrotisis, Senior_leuken, Leber Congenital Amunirois,
# Barde Biedl-Sydnrom, McKusik-Kaufmann, Alstrom, Retinitis Pigmentosa, Con/Rod Stuff (all retinal disase),
# Elis Syndrom, HVL, Polycistic Kidney Disease, Polycystic Lever Disase


def order_gene():
    disorder_new_order = ['Short_rib_Thoracic_Dysplasie_+_-_polydactylyl', 'Jeune', 'Joubert', 'Meckel_syndrome', 'orofaciodigital',
                          'Nephronophthisis', 'Senior-Loken', 'leber_congenital_amaurosis', 'Bardet-Biedl_syndrome',
                          'McKusick–Kaufman', 'Alstrom', 'Retinits_pigmentosa', 'Cone_Rod_Dystrophie', 'Ellis–van_Creveld_syndrome',
                          'Von_Hippel-Lindau', 'Polycystic_kidney_disease', 'Polycystic_liver_disease']

    (disorder, gene_disorder_pd) = get_disorder_list()
    affected_gene_total = pd.DataFrame()
    for dis in disorder_new_order:
        affected_gene = affected_gene_per_disorder(
                            gene_disorder_pd, dis)
        affected_gene = affected_gene.to_frame()
        affected_gene_total = pd.concat([affected_gene_total, affected_gene])


    affected_gene_total = affected_gene_total.drop_duplicates(keep='first')

    return affected_gene_total

