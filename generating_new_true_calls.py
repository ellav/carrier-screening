import numpy as np
import pandas as pd
import random
import split_by_disorder as split
import string
# from os import sys


pickel_filename = 'true_calls.pkl'
folder_path = '/home/selene/Programm/Diss'
true_calls = pd.read_pickle(folder_path + '/' + pickel_filename)

sample_size = 1000

Cilia_filter = ("DYNC2H1; IFT80; TTC21B; WDR19; AHI1; ALMS1; ANKS6; "
                "ARL13B; ARL3; ARMC9; B9D1; B9D2; BBIP1; BBS1; BBS10; "
                "TRIM32; BBS12; BBS2; ARL6; BBS4; BBS5; MKKS; BBS7; TTC8; "
                "BBS9; C2CD3; C2ORF71; C8orf37; CC2D2A; CDH23; CEP104; "
                "CEP120; CEP164; TSGA14; CEP78; CCDC41; CIB2; C5ORF42; "
                "CSPP1; DCDC2; DDX59; DFNB31; WDPCP; DYNC2LI1; EVC; EVC2; "
                "EXOC8; FAM161A; GLIS2; GPR98; HYLS1; IFT122; IFT140; "
                "IFT172; IFT27; IFT43; IFT52; IFT57; IFT74; IFT81; INPP5E; "
                "INTU; KIAA0556; KIAA0586; KIAA0753; KIF14; KIF7; LCA5; "
                "LZTFL1; MAK; MKS1; TMEM67; MYO7A; NEK1; NPHP1; SDCCAG8; "
                "INVS; NPHP3; NPHP4; IQCB1; CEP290; GLIS2; RPGRIP1L; NEK8; "
                "OFD1; PCDH15; PDE6D; PDZD7; PIBF1; PKHD1; POC1B; POMGNT1; "
                "RAB28; RP1; RP2; RPGR; RPGRIP1; SCLT1; SEPT7; SPATA7; "
                "SUFU; C6orf170; TCTEX1D2; TCTN1; TCTN2; TCTN3; TMEM107; "
                "TMEM138; TMEM216; TMEM231; TMEM237; TOPORS; TRAF3IP1; "
                "TTLL5; TULP1; USH1C; USH1G; USH2A; WDPCP; WDR34; WDR35; "
                "WDR60")

with open('colab_filter.txt', 'r') as file:
    data = file.read().replace('\n', '; ')
list_colab = data.split('; ')


list_function = list(true_calls['Function'].unique())
list_gene = list(true_calls['Gene'].unique())
# list_gene = Cilia_filter.split('; ')
list_zygosity = list(true_calls['Zygosity'].unique())
list_clinvar = list(true_calls['Clinvar_Significance'].unique())

(disorder, gene_disorder_pd) = split.get_disorder_list()
for dis in disorder:
    affected_gene = split.affected_gene_per_disorder(
                            gene_disorder_pd, dis)
    print(dis)
    print(affected_gene)
list_gene_2 = list(gene_disorder_pd['gene_used'].unique())

# print(set(list_gene_2).symmetric_difference(set(list_gene)))
# print(set(list_gene) - set(list_gene_2))
# print(set(list_gene3) - set(list_gene))
# print('ppp')
# print(set(list_colab) - set(list_gene))
# exit(0)


def make_num(a, b, size):
    res = pd.Series(np.random.randint(a, b, size=(size, 1))[:, 0])
    return res


def make_float(a, b, size):
    res = pd.Series(np.random.uniform(a, b, size=(size,)))
    return res


def randomStringDigits(element, stringLength):
    """Generate a random string of letters and digits """
    lettersAndDigits = string.ascii_letters + string.digits
    res = ''.join(random.choice(lettersAndDigits) for i in range(stringLength))
    return res


def random_list(element, list_to_choose):
    num = np.random.randint(0, len(list_to_choose))
    return list_to_choose[num]


def generate_scors(element, name, prob):
    res_bool = random.randrange(100) < prob
    if res_bool == 1:
        return name
    return 0


calls = pd.DataFrame()
calls['Pos'] = make_num(1, 10000000, sample_size)
calls['Coverage'] = make_num(1, 100, sample_size)
calls['Mutation_Call_HGVS_Coding'] = calls.apply(randomStringDigits, stringLength=20, axis=1)
calls['Function'] = calls.apply(random_list, list_to_choose=list_function, axis=1)
calls['Zygosity'] = calls.apply(random_list, list_to_choose=list_zygosity, axis=1)
calls['Gene'] = calls.apply(random_list, list_to_choose=list_gene, axis=1)
calls['SNP_db_xref'] = calls.apply(randomStringDigits, stringLength=15, axis=1)
calls['Amino_Acid_Change'] = calls.apply(randomStringDigits, stringLength=10, axis=1)
calls['INFO_AF'] = make_float(0, 0.01, sample_size)
calls['CADD_phred'] = make_num(0, 50, sample_size)
calls['Polyphen2_HVAR_pred'] = calls.apply(generate_scors, name='D', prob=90, axis=1)
calls['LRT_pred'] = calls.apply(generate_scors, name='D', prob=80, axis=1)
calls['MutationTaster_pred'] = calls.apply(generate_scors, name='A', prob=80, axis=1)
calls['MutationAssessor_pred'] = calls.apply(generate_scors, name='H', prob=90, axis=1)
calls['SIFT_pred'] = calls.apply(generate_scors, name='D', prob=80, axis=1)
calls['FATHMM_pred'] = calls.apply(generate_scors, name='D', prob=90, axis=1)
calls['M-CAP_score'] = make_float(0, 0.8, sample_size)
calls['M-CAP_pred'] = calls.apply(generate_scors, name='D', prob=95, axis=1)
calls['PROVEAN_pred'] = calls.apply(generate_scors, name='D', prob=90, axis=1)
calls['Clinvar_Significance'] = calls.apply(random_list, list_to_choose=list_clinvar, axis=1)
calls['patient_id'] = make_num(1, np.max([int(sample_size/20), 5]), sample_size)
calls['prediction'] = pd.Series(np.ones(sample_size))


list_title_num = ['', '', 'Alt%', 'Allele_Score',
                  'Overall_Score', '', '',
                  '', '', '', '',
                  'SIFT_score', '', 'Polyphen2_HVAR_score', '',
                  'LRT_score', '', 'MutationTaster_score', '',
                  'MutationAssessor_score', '', 'FATHMM_score', '',
                  'PROVEAN_score', 'MetaSVM_pred', 'MetaLR_pred',
                  'M-CAP_score', 'INFO_AC', '',
                  'INFO_HOM', 'INFO_HEMI', 'INFO_CLNDN', '',
                  'Imported_dbSNP_ID', '', '']


print(list(calls))

save_path = "/home/selene/Programm/Diss/" + "calls_generated.csv"
calls.to_csv(save_path)

print('done')
