import pandas as pd
from variant_filter import damaging_variant
from class_def import Parameter

def open_file():
    csv_fam = 'Kohorte_Ciliopathie_Ruxandra.csv'

    df_fam = pd.read_csv(csv_fam, sep='\t')
    df_fam.columns = (df_fam.columns.str.strip()
                         .str.replace(' ', '_')
                         .str.replace(':', ''))
    print(df_fam)
    return df_fam


def find_consanguinty(dataframe):
    total_indivual = dataframe.shape[0]
    # substring to be searched 
    sub ='‡'   
    # creating and passsing series to new column 
    dataframe["Consanguity"]= dataframe["FAM_ID"].str.find(sub)
    num_consanguin = dataframe[dataframe['Consanguity'] > 0].shape[0]
    print('There are %i persons how are consnguin (%0.3f percentag)' % (num_consanguin, num_consanguin*100/total_indivual))
    cons = dataframe[dataframe['Consanguity'] > 0]
    cons = make_fam_id_int(cons)
    cons['FAM_ID'] = pd.to_numeric(cons['FAM_ID'], errors='coerce')

    return cons


def men_women(dataframe):
    total_indivual = dataframe.shape[0]
    men = dataframe[dataframe['Sex'] == 'm'].shape[0]
    print('There are %i men (%0.3f percentag)' % (men, men*100/total_indivual))
    women = dataframe[dataframe['Sex'] == 'f'].shape[0]
    print('There are %i women (%0.3f percentag)' % (women, women*100/total_indivual))
    return


def make_fam_id_int(dataframe):

    def remove_asterix(element):
        element = element.replace('‡', '')
        element = element.replace('?', '')
        return element

    dataframe['FAM_ID'] = dataframe['FAM_ID'].apply(remove_asterix)
    return dataframe


def find_num_couples(dataframe):
    total_indivual = dataframe.shape[0]

    dataframe = make_fam_id_int(dataframe)
    fam_id = pd.to_numeric(dataframe['FAM_ID'], errors='coerce')

    singles = fam_id.drop_duplicates(keep=False)
    print('There are %i single parents (%0.3f percentag)' % (singles.shape[0], singles.shape[0]*100/total_indivual))

    couples = fam_id.drop_duplicates(keep='first')
    couples = couples.append(singles)
    couples = couples.drop_duplicates(keep=False)
    print('There are %i couples as parents (%0.3f percentag)' % (couples.shape[0], couples.shape[0]*100/total_indivual))
    return


def count_nr(element):
    return element.shape[0]

def origin(dataframe):
    total_indivual = dataframe.shape[0]

    def remove_other_strings(element):
        element = element.replace('?', '')
        return element

    def count_by_country(element):
        return element.groupby('labelled').apply(count_nr)

    dataframe['CAUC/NON-CAUC'] = dataframe['CAUC/NON-CAUC'].apply(remove_other_strings)

    cauc = dataframe[dataframe['CAUC/NON-CAUC'] == 'CAUC'].shape[0]
    print('There are %i CAUC (%0.3f percentag)' % (cauc, cauc*100/total_indivual))
    ncauc = dataframe[dataframe['CAUC/NON-CAUC'] == 'NON-CAUC'].shape[0]
    print('There are %i NON-CAUC (%0.3f percentag)' % (ncauc, ncauc*100/total_indivual))

    origins = dataframe.groupby('CAUC/NON-CAUC').apply(count_by_country)
    print(origins)
    return


def add_family_id(calls, fam):
    fam = fam.rename(columns={'M-Nr.' : 'patient_id'})

    fam = fam.set_index('patient_id')
    calls = calls.set_index('patient_id')

    result = calls.join(fam[['FAM_ID', 'Consanguity']], on='patient_id', how='left')
    result = make_fam_id_int(result)
    result.reset_index(inplace=True)
    return result


def parents_with_variants_in_same_gene(damging, cons_original):
    cons = cons_original
    damging['FAM_ID'] = pd.to_numeric(damging['FAM_ID'], errors='coerce')

    def check_individuals(element):
        return len(element['patient_id'].unique())

    def split_by_fam(element):
        return element.groupby('FAM_ID').apply(check_individuals)

    distribution = damging.groupby('Gene').apply(split_by_fam)

    distribution = distribution.to_frame()
    distribution.reset_index(inplace=True)

    distribution.rename(columns={0:'occurence'}, inplace=True)
    multiple_var = distribution[distribution['occurence'] > 1]
    multiple_var.set_index('FAM_ID', inplace=True)
    cons = cons.set_index('FAM_ID')
    result = multiple_var.join(cons['Consanguity'], on='FAM_ID', how='left')
    result.reset_index(inplace=True)
    print(result)
    print(result.shape)
    return


if __name__ == '__main__':
    family = open_file()
    cons = find_consanguinty(family)
    men_women(family)
    find_num_couples(family)
    origin(family)



    pickel_filename = 'true_calls.pkl'
    folder_path = '/home/selene/Programm/Diss'
    true_calls = pd.read_pickle(folder_path + '/' + pickel_filename)
    true_calls_with_fam = add_family_id(true_calls, family)

    ch_eng = 0
    for i in range(2):
        acmg_only = i
        param = Parameter(0.004, 500, 500, dis=0, ch_eng = ch_eng, acmg_only=acmg_only)
        damaging = damaging_variant(true_calls_with_fam, param)
        parents_with_variants_in_same_gene(damaging, cons)