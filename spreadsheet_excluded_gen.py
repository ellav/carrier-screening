import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
from class_def import Parameter
from damaging_filter import (clear_pathogenic, CADD,
                             at_least_n_pathogenic, MCAP_CADD_sep,
                             adapt_spreadsheet, ClinVar_separation,
                             HGMD_separation, af_homo_cohort)

def common_variants(dataframe, param):
    uncommon = af_homo_cohort(dataframe, param.f, param.ch_eng)
    list_original_columns = list(dataframe.columns)
    list_original_columns.remove('INFO_HOM')
    list_original_columns.remove('INFO_HEMI')
    list_original_columns.remove('INFO_AF')
    common = pd.concat([uncommon, dataframe])
    common = common.drop_duplicates(subset=list_original_columns, keep=False)
    if param.ch_eng == 0:
        nat = 'ch'
    elif param.ch_eng == 1:
        nat = 'eng'
    path = 'spreadsheet_excluded/common_variants_' + nat + '.csv'
    common.to_csv(path, sep='\t')
    return

def not_variant(dataframe, damaging, param, file_name):
    columns_to_match = ['patient_id', 'Index', 'Chrom', 'Pos', 'Coverage', 'Ref#(F;R)', 'Alt#(F;R)', 'Alt#', 'Alt%', 'Allele_Score',
                        'Overall_Score', 'Mutation_Call_HGVS_Coding', 'Function', 'Zygosity', 'Gene', 'Strand', 'CDS',
                        'Ambiguous_Gain_Penalty', 'Ambiguous_Loss_Penalty', 'Read_Balance(Counts)', 'Read_Balance(Percentage)',
                        'SNP_db_xref', 'Amino_Acid_Change', 'Comments', 'GERP++_NR', 'SIFT_score', 'SIFT_pred', 'Polyphen2_HVAR_score',
                        'Polyphen2_HVAR_pred', 'LRT_score', 'LRT_pred', 'MutationTaster_score', 'MutationTaster_pred',
                        'MutationAssessor_score', 'MutationAssessor_pred', 'FATHMM_score', 'FATHMM_pred', 'PROVEAN_score',
                        'PROVEAN_pred', 'MetaSVM_pred', 'MetaLR_pred', 'M-CAP_score', 'M-CAP_pred', 'CADD_phred', 'INFO_CAF',
                        'INFO_MAF', 'INFO_AC', 'INFO_AF', 'INFO_HOM', 'INFO_HEMI','INFO_CLNDN', 'AA_or_SNP__Pos']
    not_variants = pd.concat([dataframe, damaging], sort=False).drop_duplicates(subset=columns_to_match, keep=False)
    
    # save the spreadsheet
    if param.ch_eng == 0:
        nat = '_ch'
    elif param.ch_eng == 1:
        nat = '_eng'

    path_name = 'spreadsheet_excluded/' + file_name + nat + '.csv'
    not_variants.to_csv(path_name, sep='\t')
    return


def excluded_variant(dataframe, param):

    param.print_value()
    if dataframe.shape[0] == 0:
        return dataframe

    dataframe = af_homo_cohort(dataframe, param.f, param.ch_eng)

    cadd = param.cadd
    n = param.n
    (df_ClinVar_patho, df_ClinVar_benign, df_ClinVar_rest) = ClinVar_separation(dataframe)
    df_ClinVar_rest = df_ClinVar_rest.append(df_ClinVar_benign)
    (HGMD_patho, HGMD_rest) = HGMD_separation(df_ClinVar_rest, param.acmg_only)
    (result_pathogenic, dataframe_rest) = clear_pathogenic(HGMD_rest)

    result_end = result_pathogenic.append(df_ClinVar_patho, sort=False)
    result_end = result_end.append(HGMD_patho, sort=False)
    if param.acmg_only == 1:
        file_name = 'acmg_insufficient'
        not_variant(dataframe, result_end, param, file_name)
        return result_end

    if cadd == n and n in (100, 200, 300, 400, 500, 600):
        result_cadd_n = MCAP_CADD_sep(dataframe_rest, param)
    elif cadd != 0 and n != 0:
        result_prediction_software = at_least_n_pathogenic(
            dataframe_rest, n)
        result_cadd_n = CADD(result_prediction_software, cadd)
    elif cadd != 0 and n == 0:
        result_cadd_n = CADD(dataframe_rest, cadd)
    elif cadd == 0 and n != 0:
        result_cadd_n = at_least_n_pathogenic(
            dataframe_rest, n)
    elif cadd == 0 and n == 0:
        result_cadd_n = dataframe_rest

    print(list(result_cadd_n))
    exit()
    result_end = result_end.append(result_cadd_n, sort=False)
    file_name = 'our_model_insufficient'
    not_variant(dataframe_rest, result_cadd_n, param, file_name)
    return result_end



if __name__ == '__main__':
    folder_path = '/home/selene/Programm/Diss/'
    param = Parameter(0.004, 500, 500)

    for i in range(2):
        print(i)
        param.ch_eng = i
        if param.ch_eng == 0:
            pickel_filename = 'true_calls.pkl'
        elif param.ch_eng == 1:
            pickel_filename = 'true_calls_eng.pkl'
        true_calls = pd.read_pickle(folder_path + pickel_filename)
        common_variants(true_calls, param)

        # columns_to_match = ['patient_id', 'Index', 'Chrom', 'Pos', 'Coverage', 'Ref#(F;R)', 'Alt#(F;R)', 'Alt#', 'Alt%', 'Allele_Score',
        #                     'Overall_Score', 'Mutation_Call_HGVS_Coding', 'Function', 'Zygosity', 'Gene', 'Strand', 'CDS', 'AA_or_SNP__Pos']

        for j in range(2):
            param.acmg_only = j
            if j == 0:
                damaging0 = excluded_variant(true_calls, param)
            elif j == 1:
                damaging1 = excluded_variant(true_calls, param)
        # print(pd.concat([damaging0, damaging1]).drop_duplicates(subset=columns_to_match, keep=False))