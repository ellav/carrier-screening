import matplotlib.pyplot as plt
import pandas as pd
import pylab
import scipy.stats
import numpy as np


def splice_no_splice(dataframe):
    df_coding = dataframe.loc[
        dataframe['Function'] != 'Noncoding']
    df_noncoding = dataframe.loc[
        dataframe['Function'] == 'Noncoding']
    name = 'Splice'
    splice = df_noncoding.loc[df_noncoding['Amino_Acid_Change'] == name]
    dataframe_rest = df_noncoding.loc[
        df_noncoding['Amino_Acid_Change'] != name]
    dataframe_rest = df_coding.append(dataframe_rest)
    splice.loc[:, 'Function'] = (
        splice.loc[:, 'Function'].replace('Noncoding', 'Splice'))
    dataframe_end = splice.append(dataframe_rest)
    return dataframe_end


def AF_string_to_float(dataframe):
    AF_name = 'INFO_AF'
    dataframe.loc[:, AF_name] = dataframe[AF_name].fillna(value=0)
    float_col = pd.to_numeric(dataframe[AF_name], errors='coerce')
    res = dataframe[float_col.isna()]
    rest = dataframe[float_col.notna()]

    if res.shape[0] == 0:
        return rest

    file_name = 'string_as_INFO_AF.csv'
    res.to_csv(file_name, sep='\t')
    print(res)
    print(res[AF_name])
    res.loc[:, AF_name] = res.loc[:, AF_name].str.split(',')
    res.loc[:, AF_name] = res[AF_name].apply(max)

    return res.append(rest)


def replace_splice(row):
    SNP = row['SNP_db_xref']
    AA = row['Amino_Acid_Change']
    if AA == 'Splice':
        return SNP
    elif SNP == 0:
        return AA
    else:
        return AA


def make_unique_col(dataframe):
    column_list = ['Amino_Acid_Change', 'SNP_db_xref']

    aa_snp = dataframe[column_list].apply(replace_splice, axis=1)
    aa_pos_added = aa_snp.astype(str) + '__' + dataframe['Pos'].astype(str)
    dataframe.loc[:, 'AA_or_SNP__Pos'] = aa_pos_added
    return dataframe


def keep_wrong_gene(dataframe, ch_eng):
    # Genes that were not included in our filter
    list_gene = ['KAAG1', 'PUS3', 'TMEM204', 'LRRC19', 'C10ORF105', 'POC1B-GALNT4', 'C10ORF54', 'STX19']
    dataframe_failes_gene = pd.DataFrame()
    for gene in list_gene:
        temp = dataframe[dataframe.Gene == gene]
        dataframe_failes_gene = dataframe_failes_gene.append(temp, sort=False)
    if ch_eng == 0:
        nat = '_ch'
    elif ch_eng == 1:
        nat = '_eng'
    path = 'spreadsheet_excluded/failing_manual_cleaning' + nat + '.csv'
    dataframe_failes_gene.to_csv(path, sep='\t')
    return


def ClinVar_cols(dataframe):

    def clinvar(elements, criteria):
        if elements in criteria:
            return 1
        return 0

    benign_crit = [
        'Benign', 'Likely_benign', 'Benign/Likely_benign']
    unknown_crit = [
        'Conflicting_interpretations_of_pathogenicity',
        'Uncertain_significance']
    patho_crit = [
        'Pathogenic', 'Likely_pathogenic',
        'Pathogenic/Likely_pathogenic']
    conflict_crit = [
        'Conflicting_interpretations_of_pathogenicity',
        'Benign,Conflicting_interpretations_of_pathogenicity',
        'Conflicting_interpretations_of_pathogenicity,Likely_benign,Uncertain_significance',
        'Benign,Benign/Likely_benign,Conflicting_interpretations_of_pathogenicity']
    uncertain_crit = [
        'Uncertain_significance']

    dataframe.loc[:, 'ClinVar_red'] = dataframe[
        'Clinvar_Significance'].apply(
            clinvar, criteria=benign_crit)
    
    dataframe.loc[:, 'ClinVar_italic'] = dataframe[
        'Clinvar_Significance'].apply(
            clinvar, criteria=patho_crit)

    dataframe.loc[:, 'ClinVar_bold'] = dataframe[
        'Clinvar_Significance'].apply(
            clinvar, criteria=unknown_crit)

    dataframe.loc[:, 'ClinVar_conflict'] = dataframe[
        'Clinvar_Significance'].apply(
            clinvar, criteria=conflict_crit)

    dataframe.loc[:, 'ClinVar_uncertain'] = dataframe[
        'Clinvar_Significance'].apply(
            clinvar, criteria=uncertain_crit)

    return dataframe


def hgmd_check_manual(dataframe, ch_eng):
    # variants wich have not an unique chr:pos value
    # -> the variants is not DM for all patientes

    if ch_eng == 0:
        return dataframe
    critical_values = ['chr2:73800135']
    dataframe_critical = pd.DataFrame()
    for crit in critical_values:
        tmp = dataframe[dataframe['hg19/hg38_coordinate'] == crit]
        dataframe_critical = dataframe_critical.append(tmp)
    print('The critical variants with the HGMD:')
    print(dataframe_critical)
    return 1

def count_hgmd_valeus(dataframe):
    counters = {}
    for element in dataframe['HGMD']:
        counters.setdefault(element, 0)
        counters[element] = counters[element] + 1
    print(counters)
    return


def add_hgmd(dataframe, ch_eng):

    def hgmd_code(elements):
        chrom = str(int(elements[0]))
        pos = str(int(elements[1]))
        result_str = 'chr' + chrom + ':' + pos
        return result_str

    def merge_two_hgmd_column(elements):
        if str(elements[0]) == 'nan':
            return elements[1]
        return elements[0]

    dataframe['hg19/hg38_coordinate'] = dataframe[['Chrom', 'Pos']].apply(hgmd_code, axis=1)

    # add the HGMD column
    if ch_eng == 0:
        xls = pd.ExcelFile('CH_patients_variants_ with HGMD classification.xlsx')
        df = pd.read_excel(xls, 'CH_variants_in_HGMD')
        dataframe['AA_or_SNP__Pos'] = dataframe['AA_or_SNP__Pos'].astype(str)
        df['AA_or_SNP__Pos'] = df['AA_or_SNP__Pos'].astype(str).drop_duplicates()
        result = dataframe.merge(df[['HGMD', 'AA_or_SNP__Pos']], on='AA_or_SNP__Pos', how='left')
    elif ch_eng == 1:
        # variants which have already been classified in Swiss data
        xls_ch = pd.ExcelFile('CH_patients_variants_ with HGMD classification.xlsx')
        df_ch = pd.read_excel(xls_ch, 'CH_variants_in_HGMD')
        dataframe['AA_or_SNP__Pos'] = dataframe['AA_or_SNP__Pos'].astype(str)
        df_ch['AA_or_SNP__Pos'] = df_ch['AA_or_SNP__Pos'].astype(str).drop_duplicates()
        result_ch = dataframe.merge(df_ch[['HGMD', 'AA_or_SNP__Pos']], on='AA_or_SNP__Pos', how='left')
        result_ch.rename(columns={'HGMD': 'HGMD_ch'}, inplace=True)

        # variants which have an chr:pos entry
        xls = pd.ExcelFile('Eng_trueCalls_HGMD_batch_ruxandra.xlsx')
        df = pd.read_excel(xls, 'variants with HGMD entry')
        result_ch['hg19/hg38_coordinate'] = result_ch['hg19/hg38_coordinate'].astype(str)
        df.rename(columns={'Search terms': 'hg19/hg38_coordinate', 'Variant class': 'HGMD'}, inplace=True)
        df['hg19/hg38_coordinate'] = df['hg19/hg38_coordinate'].astype(str).drop_duplicates()
        result_1_HGMD = result_ch.merge(df[['HGMD', 'hg19/hg38_coordinate']], on='hg19/hg38_coordinate', how='left')

        # variants which have an RS entry
        xls_rs = pd.ExcelFile('Eng_trueCalls_HGMD_batch_rs_ruxandra.xlsx')
        df_rs = pd.read_excel(xls_rs, 'variants with HGMD entry')
        result_1_HGMD['SNP_db_xref'] = result_1_HGMD['SNP_db_xref'].astype(str)
        df_rs.rename(columns={'Search terms': 'SNP_db_xref', 'Variant class': 'HGMD'}, inplace=True)
        df_rs['SNP_db_xref'] = df_rs['SNP_db_xref'].astype(str).drop_duplicates()
        result_2_HGMD = result_1_HGMD.merge(df_rs[['HGMD', 'SNP_db_xref']], on='SNP_db_xref', how='left')

        # merge the three HGMD columns to one column
        result_2_HGMD['HGMD_z'] = result_2_HGMD[['HGMD_x', 'HGMD_y']].apply(merge_two_hgmd_column, axis=1)
        result = result_2_HGMD.drop(columns=['HGMD_x', 'HGMD_y'])
        result['HGMD'] = result[['HGMD_z', 'HGMD_ch']].apply(merge_two_hgmd_column, axis=1)
        result = result.drop(columns=['HGMD_z', 'HGMD_ch'])

    hgmd_check_manual(result, ch_eng)
    count_hgmd_valeus(result)
    return result


def plot_num_of_variant(dataframe, ch_eng, fast):
    # count the number of varients for each patient
    num_of_var = []
    pat_list = dataframe['patient_id'].unique()
    for pat in pat_list:
        variants_pat = dataframe.loc[dataframe['patient_id'] == pat]
        size = variants_pat.shape[0]
        num_of_var.append(size)

    print('Mean of Mutation Report size: %f' % (np.mean(num_of_var)))
    total = sum(num_of_var)
    # print(total)
    # plot the number of variants as histo-plot
    hist = {}
    if fast == 1:
        # step = 1
        step = float(1/total)
    elif fast == 0:
        step = 1

    for i in num_of_var:
        hist[i] = float(hist.get(i, 0)) + step

    if fast == 1:
        return hist

    plt.rcParams.update({'font.size': 18})

    plt.bar(x=list(hist.keys()), height=list(hist.values()))
    plt.xlabel('Number of variants in mutation report')
    plt.ylabel('Patients')

    # save the figure
    path = '/home/selene/Programm/Diss/'
    path_plot = path + 'distribution_of_variants'
    if ch_eng == 1:
        path_plot = path_plot + '_eng'
    plt.savefig(path_plot, bbox_inches='tight')

    # delete the plot to prevent open empty figures
    plt.clf()
    plt.close()
    return hist


def plot_ch_eng_num_of_variant(path, path_eng):
    calls = pd.read_pickle(path)
    calls_eng = pd.read_pickle(path_eng)

    plt.rcParams.update({'font.size': 15})

    hist = plot_num_of_variant(calls, 0, 1)
    hist_eng = plot_num_of_variant(calls_eng, 0, 1)
    plt.bar(x=list(hist.keys()), height=list(hist.values()),
            alpha=0.5, label='Swiss')
    plt.bar(x=list(hist_eng.keys()), height=list(hist_eng.values()),
            alpha=0.5, label='English')
    pylab.legend(loc='upper left')

    plt.xlabel('Number of variants in mutation report')
    # plt.ylabel('Patients')
    plt.ylabel('Patients, normalized with number of variants')
    plt.title('Distribution of Number of Variants per Patient')

    # save the figure
    path = '/home/selene/Programm/Diss/'
    path_plot = path + 'difference_ch_eng.png'
    plt.savefig(path_plot, bbox_inches='tight')

    # plt.show()
    plt.clf()
    plt.close()
    return


def hist_coverage(dataframe, name):
    cov = dataframe[name]
    step = 1
    hist = {}
    for i in cov:
        hist[i] = float(hist.get(i, 0)) + step
    return hist


def plot_ch_eng_coverage(path, path_eng):
    calls = pd.read_pickle(path)
    calls_eng = pd.read_pickle(path_eng)

    cov = calls['Coverage']
    cov_eng = calls_eng['Coverage']
    print(scipy.stats.ttest_ind(cov, cov_eng, equal_var=False))

    hist = hist_coverage(calls, 'Coverage')
    hist_eng = hist_coverage(calls_eng, 'Coverage')

    plt.rcParams.update({'font.size': 15})

    plt.bar(x=list(hist.keys()), height=list(hist.values()),
            alpha=0.5, label='Swiss')
    plt.bar(x=list(hist_eng.keys()), height=list(hist_eng.values()),
            alpha=0.5, label='English')
    pylab.legend(loc='upper left')

    plt.title('Distribution of Variant Coverage')
    plt.xlabel('Coverage')
    plt.ylabel('Variant')

    # save the figure
    path = '/home/selene/Programm/Diss/'
    path_plot = path + 'difference_coverage_ch_eng.png'
    plt.savefig(path_plot, bbox_inches='tight')

    # plt.show()
    plt.clf()
    plt.close()

    return


def plot_ch_eng_overall_score(path, path_eng):
    calls = pd.read_pickle(path)
    calls_eng = pd.read_pickle(path_eng)

    cov = calls['Overall_Score']
    cov_eng = calls_eng['Overall_Score']
    print(scipy.stats.ttest_ind(cov, cov_eng, equal_var=False))

    hist = hist_coverage(calls, 'Overall_Score')
    hist_eng = hist_coverage(calls_eng, 'Overall_Score')

    plt.rcParams.update({'font.size': 15})

    plt.bar(x=list(hist.keys()), height=list(hist.values()), width=0.1,
            alpha=0.5, label='Swiss')
    plt.bar(x=list(hist_eng.keys()), height=list(hist_eng.values()), width=0.1,
            alpha=0.5, label='English')
    pylab.legend(loc='upper left')

    plt.title('Distribution of Variant Overall Score')
    plt.xlabel('Overall Score')
    plt.ylabel('Variant')

    # save the figure
    path = '/home/selene/Programm/Diss/'
    path_plot = path + 'difference_overall_score_ch_eng.png'
    plt.savefig(path_plot, bbox_inches='tight')

    # plt.show()
    plt.clf()
    plt.close()

    return

def distribution_of_unique_variants(dataframe, ch_eng):
    if dataframe.shape[0] == 0:
        return
    m_plot = 1
    counters = {}
    for element in dataframe['AA_or_SNP__Pos']:
        counters.setdefault(element, 0)
        counters[element] = counters[element] + 1
    counters_filtered = {k: v for k, v in counters.items() if v >= m_plot}

    counters_filtered_print = {k: v for k, v in counters.items() if v >= 20}
    print('\n'.join(counters_filtered_print))

    hist = {}
    for i in counters_filtered.values():
        hist[i] = hist.get(i, 0) + 1
    plt.rcParams.update({'font.size': 18})
    plt.bar(x=list(hist.keys()), height=list(hist.values()))
    plt.xlabel('Repetition of each Variant')
    plt.ylabel('Number of Variants')

    titel = 'Distribution of all Unique Variant'
    if ch_eng == 0:
        titel = titel + ' - Swiss Data'
        path_end = '_ch'
    elif ch_eng == 1:
        titel = titel + ' - English Data'
        path_end = '_eng'


    path = '/home/selene/Programm/Diss/'
    path_plot = path + 'distribution_of_unique_variants' + path_end + '.png'
    plt.title(titel)
    plt.savefig(path_plot, bbox_inches='tight')
    plt.clf()
    plt.close()
    return


def preprocess(path_name, ch_eng):
    calls = pd.read_pickle(path_name)
    print(calls.shape)

    calls = AF_string_to_float(calls)
    calls = splice_no_splice(calls)
    calls = make_unique_col(calls)
    calls = ClinVar_cols(calls)
    calls = add_hgmd(calls, ch_eng)
    keep_wrong_gene(calls, ch_eng)
    print(calls.shape)
    print('=========')

    # plot_num_of_variant(calls, ch_eng, 0)
    # distribution_of_unique_variants(calls, ch_eng)
    if ch_eng == 0:
        print('Swiss data')
        save_new_path = "/home/selene/Programm/Diss/true_calls.pkl"
    elif ch_eng == 1:
        print('English data')
        save_new_path = "/home/selene/Programm/Diss/true_calls_eng.pkl"

    # calls.to_pickle(save_new_path)

    print(len(calls['patient_id'].unique()))
    print('Coverage')
    print(np.mean(calls['Coverage']))
    return


if __name__ == '__main__':
    save_path = "/home/selene/Programm/Diss" + "/pred_true_calls.pkl"
    save_path_eng = "/home/selene/Programm/Diss" + "/pred_true_calls_eng.pkl"

    save_path_new = "/home/selene/Programm/Diss/true_calls.pkl"
    save_path_eng_new = "/home/selene/Programm/Diss/true_calls_eng.pkl"

    plot_ch_eng_num_of_variant(save_path_new, save_path_eng_new)
    plot_ch_eng_coverage(save_path_new, save_path_eng_new)
    plot_ch_eng_overall_score(save_path_new, save_path_eng_new)


    preprocess(save_path, 0)
    preprocess(save_path_eng, 1)
