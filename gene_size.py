import pandas as pd
from variant_filter import damaging_variant
from class_def import Parameter
import matplotlib.pyplot as plt
import numpy as np
from scipy.stats import (linregress, spearmanr, normaltest)


def open_adapt_file():
    csv_gene_size = 'correlation_gene_size.csv'

    df_gene_size = pd.read_csv(csv_gene_size, sep='\t')
    df_gene_size.columns = (df_gene_size.columns.str.strip()
                         .str.replace(' ', '_')
                         .str.replace(':', ''))
    df_gene_size = df_gene_size.rename(columns={'Gene_name': 'Gene'})
    return df_gene_size


def compare_size_to_var(damaging, df_size):

    def count_variants(element):
        return element.shape[0]

    damaging = damaging.drop_duplicates(subset=['AA_or_SNP__Pos'])

    variants_per_gene = damaging.groupby('Gene').apply(count_variants)
    variants_per_gene = variants_per_gene.rename('number_of_variants')

    variants_per_gene = variants_per_gene.to_frame()
    df_size = df_size.set_index('Gene')
    df_gene_size_variant = variants_per_gene.join(df_size[['cDNA_nucl', 'Prot_aa']], on='Gene', how='left')

    return df_gene_size_variant


def plot_nr_variants_size(dataframe, ch_eng, acmg_only):
    acmg = 'ACMG'
    acmg_txt = acmg
    if acmg_only == 0:
        acmg = ''
        acmg_txt = 'including novel ms variants'
    nat = 'ch'
    if ch_eng == 1:
        nat = 'eng'
    to_compare = 'cDNA_nucl'
    dataframe = dataframe.dropna(subset=[to_compare])
    dataframe = dataframe.reset_index()
    x = dataframe['number_of_variants']
    y = dataframe[to_compare]
    n = dataframe['Gene']

    fig, ax = plt.subplots()
    ax.scatter(x, y)


    coef = np.polyfit(x,y,1)
    poly1d_fn = np.poly1d(coef)
    x_line = np.linspace(x.min()-3, x.max()+3, 20)
    ax.plot(x_line, poly1d_fn(x_line), color='green', linestyle='dashed')
    ax.set_title('Correlation of cDNA and number of unique variants - ' + acmg_txt)
    ax.set_xlabel('Number of unique variants')
    ax.set_ylabel('Size of cDNA')

    fig_path = 'plot/correlation_size_gene_'+ nat + '_' + acmg + '.png'
    fig.savefig(fig_path, bbox_inches='tight')

    print('--------\n' + nat +' ' + acmg + ': ' + str(spearmanr(x, y)))

    for i, txt in enumerate(n):

        ax.annotate(txt, (x[i], y[i]))
    return


if __name__ == '__main__':
    df_gene_size = open_adapt_file()

    for j in range(2):
        acmg_only = j

        for i in range(2):
            ch_eng = i
            param = Parameter(0.004, 500, 500, ch_eng=ch_eng, dis=0, acmg_only=acmg_only)
            if ch_eng == 0:
                pickel_filename = 'true_calls.pkl'
            elif ch_eng == 1:
                pickel_filename = 'true_calls_eng.pkl'
            folder_path = '/home/selene/Programm/Diss'

            true_calls = pd.read_pickle(folder_path + '/' + pickel_filename)

            damaging = damaging_variant(true_calls, param)
            gene_size_variants = compare_size_to_var(damaging, df_gene_size)
            plot_nr_variants_size(gene_size_variants, ch_eng, acmg_only)